一、dchdmaster1备份机制   
1、 每天凌晨3点进行一次全量备份。不能增量备份原因，mysql version 5.1.7 与还原增量与percona-xtrabackup-2.0.1存些兼容性报错问题！
2、 脚本存放路径：``/db_scripts/call_innobk.sh``

二、全量还原演练    
1、 安装mysql且版本要一致    
``
[root@dchadoop3 ~]# yum install mysql-server mysql
``

``[root@dchadoop3 ~]# rpm -qa mysql
mysql-5.1.73-7.el6.x86_64
``
2、安装percona-xtrabackup-2.0.1-446.rhel6.x86_64.rpm   

``
[root@dchadoop3 ~]# yum install percona-xtrabackup-2.0.1-446.rhel6.x86_64.rpm
``
 3、删除/var/lib/mysql   
``
[root@dchadoop3 ~]# rm /var/lib/mysql/* -rf
``
4、还原   

``
[root@dchadoop3 ~]# innobackupex --copy-back /backup/base_20170120
``

```
InnoDB Backup Utility v1.5.1-xtrabackup; Copyright 2003, 2009 Innobase Oy
and Percona Inc 2009-2012.  All Rights Reserved.

This software is published under
the GNU GENERAL PUBLIC LICENSE Version 2, June 1991.

IMPORTANT: Please check that the copy-back run completes successfully.
           At the end of a successful copy-back run innobackupex
           prints "completed OK!".

innobackupex: Starting to copy files in '/backup/base_20170120'
innobackupex: back to original data directory '/var/lib/mysql'
innobackupex: Creating directory '/var/lib/mysql/mysql'
innobackupex: Copying '/backup/base_20170120/mysql/help_relation.MYD' to '/var/lib/mysql/mysql/help_relation.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_transition.MYI' to '/var/lib/mysql/mysql/time_zone_transition.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/help_relation.frm' to '/var/lib/mysql/mysql/help_relation.frm'
innobackupex: Copying '/backup/base_20170120/mysql/general_log.CSV' to '/var/lib/mysql/mysql/general_log.CSV'
innobackupex: Copying '/backup/base_20170120/mysql/tables_priv.MYD' to '/var/lib/mysql/mysql/tables_priv.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/ndb_binlog_index.frm' to '/var/lib/mysql/mysql/ndb_binlog_index.frm'
innobackupex: Copying '/backup/base_20170120/mysql/user.MYI' to '/var/lib/mysql/mysql/user.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_name.frm' to '/var/lib/mysql/mysql/time_zone_name.frm'
innobackupex: Copying '/backup/base_20170120/mysql/event.MYI' to '/var/lib/mysql/mysql/event.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/func.frm' to '/var/lib/mysql/mysql/func.frm'
innobackupex: Copying '/backup/base_20170120/mysql/ndb_binlog_index.MYD' to '/var/lib/mysql/mysql/ndb_binlog_index.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/help_keyword.MYI' to '/var/lib/mysql/mysql/help_keyword.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/proc.frm' to '/var/lib/mysql/mysql/proc.frm'
innobackupex: Copying '/backup/base_20170120/mysql/help_topic.MYD' to '/var/lib/mysql/mysql/help_topic.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/func.MYI' to '/var/lib/mysql/mysql/func.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_transition.MYD' to '/var/lib/mysql/mysql/time_zone_transition.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/servers.MYD' to '/var/lib/mysql/mysql/servers.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/host.MYI' to '/var/lib/mysql/mysql/host.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_leap_second.MYI' to '/var/lib/mysql/mysql/time_zone_leap_second.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/host.MYD' to '/var/lib/mysql/mysql/host.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/columns_priv.MYI' to '/var/lib/mysql/mysql/columns_priv.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/help_topic.frm' to '/var/lib/mysql/mysql/help_topic.frm'
innobackupex: Copying '/backup/base_20170120/mysql/slow_log.CSV' to '/var/lib/mysql/mysql/slow_log.CSV'
innobackupex: Copying '/backup/base_20170120/mysql/help_keyword.MYD' to '/var/lib/mysql/mysql/help_keyword.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/user.frm' to '/var/lib/mysql/mysql/user.frm'
innobackupex: Copying '/backup/base_20170120/mysql/slow_log.CSM' to '/var/lib/mysql/mysql/slow_log.CSM'
innobackupex: Copying '/backup/base_20170120/mysql/ndb_binlog_index.MYI' to '/var/lib/mysql/mysql/ndb_binlog_index.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone.MYI' to '/var/lib/mysql/mysql/time_zone.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_transition_type.frm' to '/var/lib/mysql/mysql/time_zone_transition_type.frm'
innobackupex: Copying '/backup/base_20170120/mysql/proc.MYI' to '/var/lib/mysql/mysql/proc.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/servers.MYI' to '/var/lib/mysql/mysql/servers.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_name.MYI' to '/var/lib/mysql/mysql/time_zone_name.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/servers.frm' to '/var/lib/mysql/mysql/servers.frm'
innobackupex: Copying '/backup/base_20170120/mysql/help_keyword.frm' to '/var/lib/mysql/mysql/help_keyword.frm'
innobackupex: Copying '/backup/base_20170120/mysql/columns_priv.frm' to '/var/lib/mysql/mysql/columns_priv.frm'
innobackupex: Copying '/backup/base_20170120/mysql/help_category.MYI' to '/var/lib/mysql/mysql/help_category.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone.frm' to '/var/lib/mysql/mysql/time_zone.frm'
innobackupex: Copying '/backup/base_20170120/mysql/db.MYD' to '/var/lib/mysql/mysql/db.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/db.MYI' to '/var/lib/mysql/mysql/db.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_transition_type.MYD' to '/var/lib/mysql/mysql/time_zone_transition_type.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/help_category.MYD' to '/var/lib/mysql/mysql/help_category.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone.MYD' to '/var/lib/mysql/mysql/time_zone.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/func.MYD' to '/var/lib/mysql/mysql/func.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/procs_priv.MYD' to '/var/lib/mysql/mysql/procs_priv.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_name.MYD' to '/var/lib/mysql/mysql/time_zone_name.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/plugin.MYD' to '/var/lib/mysql/mysql/plugin.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/columns_priv.MYD' to '/var/lib/mysql/mysql/columns_priv.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/help_relation.MYI' to '/var/lib/mysql/mysql/help_relation.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_transition.frm' to '/var/lib/mysql/mysql/time_zone_transition.frm'
innobackupex: Copying '/backup/base_20170120/mysql/db.frm' to '/var/lib/mysql/mysql/db.frm'
innobackupex: Copying '/backup/base_20170120/mysql/event.MYD' to '/var/lib/mysql/mysql/event.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_leap_second.frm' to '/var/lib/mysql/mysql/time_zone_leap_second.frm'
innobackupex: Copying '/backup/base_20170120/mysql/plugin.frm' to '/var/lib/mysql/mysql/plugin.frm'
innobackupex: Copying '/backup/base_20170120/mysql/tables_priv.frm' to '/var/lib/mysql/mysql/tables_priv.frm'
innobackupex: Copying '/backup/base_20170120/mysql/user.MYD' to '/var/lib/mysql/mysql/user.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/plugin.MYI' to '/var/lib/mysql/mysql/plugin.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/host.frm' to '/var/lib/mysql/mysql/host.frm'
innobackupex: Copying '/backup/base_20170120/mysql/event.frm' to '/var/lib/mysql/mysql/event.frm'
innobackupex: Copying '/backup/base_20170120/mysql/slow_log.frm' to '/var/lib/mysql/mysql/slow_log.frm'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_leap_second.MYD' to '/var/lib/mysql/mysql/time_zone_leap_second.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/general_log.CSM' to '/var/lib/mysql/mysql/general_log.CSM'
innobackupex: Copying '/backup/base_20170120/mysql/help_category.frm' to '/var/lib/mysql/mysql/help_category.frm'
innobackupex: Copying '/backup/base_20170120/mysql/proc.MYD' to '/var/lib/mysql/mysql/proc.MYD'
innobackupex: Copying '/backup/base_20170120/mysql/help_topic.MYI' to '/var/lib/mysql/mysql/help_topic.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/time_zone_transition_type.MYI' to '/var/lib/mysql/mysql/time_zone_transition_type.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/tables_priv.MYI' to '/var/lib/mysql/mysql/tables_priv.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/general_log.frm' to '/var/lib/mysql/mysql/general_log.frm'
innobackupex: Copying '/backup/base_20170120/mysql/procs_priv.MYI' to '/var/lib/mysql/mysql/procs_priv.MYI'
innobackupex: Copying '/backup/base_20170120/mysql/procs_priv.frm' to '/var/lib/mysql/mysql/procs_priv.frm'
innobackupex: Creating directory '/var/lib/mysql/dc_scheduleserver_hadoop2'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/convert_tables.MYI' to '/var/lib/mysql/dc_scheduleserver_hadoop2/convert_tables.MYI'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/process_log.frm' to '/var/lib/mysql/dc_scheduleserver_hadoop2/process_log.frm'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_template.MYD' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_template.MYD'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_execute_log.MYD' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_execute_log.MYD'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_template.frm' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_template.frm'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_execute_log.frm' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_execute_log.frm'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/orderid_jobinstid.MYD' to '/var/lib/mysql/dc_scheduleserver_hadoop2/orderid_jobinstid.MYD'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/convert_queue.frm' to '/var/lib/mysql/dc_scheduleserver_hadoop2/convert_queue.frm'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_instance.MYD' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_instance.MYD'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_execute_log.MYI' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_execute_log.MYI'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/convert_tables.frm' to '/var/lib/mysql/dc_scheduleserver_hadoop2/convert_tables.frm'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_instance.MYI' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_instance.MYI'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/convert_tables.MYD' to '/var/lib/mysql/dc_scheduleserver_hadoop2/convert_tables.MYD'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/process_log.MYD' to '/var/lib/mysql/dc_scheduleserver_hadoop2/process_log.MYD'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/orderid_jobinstid.MYI' to '/var/lib/mysql/dc_scheduleserver_hadoop2/orderid_jobinstid.MYI'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/convert_queue.MYD' to '/var/lib/mysql/dc_scheduleserver_hadoop2/convert_queue.MYD'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/convert_queue.MYI' to '/var/lib/mysql/dc_scheduleserver_hadoop2/convert_queue.MYI'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_template.MYI' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_template.MYI'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/job_instance.frm' to '/var/lib/mysql/dc_scheduleserver_hadoop2/job_instance.frm'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/orderid_jobinstid.frm' to '/var/lib/mysql/dc_scheduleserver_hadoop2/orderid_jobinstid.frm'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/db.opt' to '/var/lib/mysql/dc_scheduleserver_hadoop2/db.opt'
innobackupex: Copying '/backup/base_20170120/dc_scheduleserver_hadoop2/process_log.MYI' to '/var/lib/mysql/dc_scheduleserver_hadoop2/process_log.MYI'
innobackupex: Creating directory '/var/lib/mysql/company'
innobackupex: Copying '/backup/base_20170120/company/employees_info.MYD' to '/var/lib/mysql/company/employees_info.MYD'
innobackupex: Copying '/backup/base_20170120/company/employees_info.frm' to '/var/lib/mysql/company/employees_info.frm'
innobackupex: Copying '/backup/base_20170120/company/employees_info.MYI' to '/var/lib/mysql/company/employees_info.MYI'
innobackupex: Copying '/backup/base_20170120/company/db.opt' to '/var/lib/mysql/company/db.opt'
innobackupex: Creating directory '/var/lib/mysql/huangchengbo'
innobackupex: Copying '/backup/base_20170120/huangchengbo/base_brand_mapped_list.frm' to '/var/lib/mysql/huangchengbo/base_brand_mapped_list.frm'
innobackupex: Copying '/backup/base_20170120/huangchengbo/base_brand_mapped_list.MYI' to '/var/lib/mysql/huangchengbo/base_brand_mapped_list.MYI'
innobackupex: Copying '/backup/base_20170120/huangchengbo/db.opt' to '/var/lib/mysql/huangchengbo/db.opt'
innobackupex: Copying '/backup/base_20170120/huangchengbo/base_brand_mapped_list.MYD' to '/var/lib/mysql/huangchengbo/base_brand_mapped_list.MYD'
innobackupex: Creating directory '/var/lib/mysql/mytest_001'
innobackupex: Copying '/backup/base_20170120/mytest_001/db.opt' to '/var/lib/mysql/mytest_001/db.opt'
innobackupex: Creating directory '/var/lib/mysql/dc_business_user'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_shorturl.MYD' to '/var/lib/mysql/dc_business_user/dc_channel_shorturl.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm.frm' to '/var/lib/mysql/dc_business_user/dc_alarm.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_custom_id_global.MYD' to '/var/lib/mysql/dc_business_user/dc_custom_id_global.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_sign.MYD' to '/var/lib/mysql/dc_business_user/dc_channel_sign.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_multi_config.MYD' to '/var/lib/mysql/dc_business_user/dc_multi_config.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_month_report.frm' to '/var/lib/mysql/dc_business_user/dc_month_report.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_datacenter_cache.frm' to '/var/lib/mysql/dc_business_user/dc_games_datacenter_cache.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_child_account_h5.MYD' to '/var/lib/mysql/dc_business_user/dc_child_account_h5.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_fail_info.MYI' to '/var/lib/mysql/dc_business_user/dc_push_fail_info.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_config.frm' to '/var/lib/mysql/dc_business_user/dc_game_config.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_message_config.MYD' to '/var/lib/mysql/dc_business_user/dc_push_message_config.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_auth_coop.MYD' to '/var/lib/mysql/dc_business_user/dc_auth_coop.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_company_for_coolpad.frm' to '/var/lib/mysql/dc_business_user/dc_company_for_coolpad.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_url.MYI' to '/var/lib/mysql/dc_business_user/dc_game_url.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_config.MYD' to '/var/lib/mysql/dc_business_user/dc_game_config.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_subscription.frm' to '/var/lib/mysql/dc_business_user/dc_subscription.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sso_authinfo.MYI' to '/var/lib/mysql/dc_business_user/dc_sso_authinfo.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_page.frm' to '/var/lib/mysql/dc_business_user/dc_users_page.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_plugins.frm' to '/var/lib/mysql/dc_business_user/dc_game_plugins.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_open_auth.frm' to '/var/lib/mysql/dc_business_user/dc_open_auth.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_exp.MYD' to '/var/lib/mysql/dc_business_user/dc_menu_exp.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_child_account_h5.frm' to '/var/lib/mysql/dc_business_user/dc_child_account_h5.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_message_detail.MYD' to '/var/lib/mysql/dc_business_user/dc_push_message_detail.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm.MYI' to '/var/lib/mysql/dc_business_user/dc_alarm.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sub_data.frm' to '/var/lib/mysql/dc_business_user/dc_sub_data.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm_detail.frm' to '/var/lib/mysql/dc_business_user/dc_alarm_detail.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_uniq_config.MYI' to '/var/lib/mysql/dc_business_user/dc_uniq_config.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_user_message.frm' to '/var/lib/mysql/dc_business_user/dc_user_message.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_exchange_rate.MYD' to '/var/lib/mysql/dc_business_user/dc_exchange_rate.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_g2.MYD' to '/var/lib/mysql/dc_business_user/dc_menu_g2.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_sign.frm' to '/var/lib/mysql/dc_business_user/dc_channel_sign.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_company_coop.frm' to '/var/lib/mysql/dc_business_user/dc_company_coop.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_message_detail.MYI' to '/var/lib/mysql/dc_business_user/dc_push_message_detail.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm_day_cfg.frm' to '/var/lib/mysql/dc_business_user/dc_alarm_day_cfg.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sdk_version_stable.frm' to '/var/lib/mysql/dc_business_user/dc_sdk_version_stable.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_for_coolpad.MYD' to '/var/lib/mysql/dc_business_user/dc_games_for_coolpad.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sdk_version.MYD' to '/var/lib/mysql/dc_business_user/dc_sdk_version.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_type.frm' to '/var/lib/mysql/dc_business_user/dc_game_type.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_inner_alarm_cfg.frm' to '/var/lib/mysql/dc_business_user/dc_inner_alarm_cfg.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_g2.frm' to '/var/lib/mysql/dc_business_user/dc_menu_g2.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_message.frm' to '/var/lib/mysql/dc_business_user/dc_message.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_active_callbackurl.MYI' to '/var/lib/mysql/dc_business_user/dc_active_callbackurl.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games.frm' to '/var/lib/mysql/dc_business_user/dc_games.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sdk_version_stable.MYI' to '/var/lib/mysql/dc_business_user/dc_sdk_version_stable.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_datacenter_cache.MYD' to '/var/lib/mysql/dc_business_user/dc_games_datacenter_cache.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_shorturl.MYI' to '/var/lib/mysql/dc_business_user/dc_channel_shorturl.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_open_auth.MYI' to '/var/lib/mysql/dc_business_user/dc_open_auth.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_dataclean.MYI' to '/var/lib/mysql/dc_business_user/dc_dataclean.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sub_data.MYI' to '/var/lib/mysql/dc_business_user/dc_sub_data.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_dataclean.MYD' to '/var/lib/mysql/dc_business_user/dc_dataclean.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_message_detail.frm' to '/var/lib/mysql/dc_business_user/dc_push_message_detail.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_subscription_cfg.frm' to '/var/lib/mysql/dc_business_user/dc_subscription_cfg.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_selector.MYD' to '/var/lib/mysql/dc_business_user/dc_users_selector.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_test_equip.MYD' to '/var/lib/mysql/dc_business_user/dc_push_test_equip.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm_exclude.MYI' to '/var/lib/mysql/dc_business_user/dc_alarm_exclude.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/monitorData.frm' to '/var/lib/mysql/dc_business_user/monitorData.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_clusteranalysis.frm' to '/var/lib/mysql/dc_business_user/dc_clusteranalysis.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_config2.MYI' to '/var/lib/mysql/dc_business_user/dc_game_config2.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_url.MYD' to '/var/lib/mysql/dc_business_user/dc_game_url.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sdk_version_stable.MYD' to '/var/lib/mysql/dc_business_user/dc_sdk_version_stable.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm_hour_cfg.frm' to '/var/lib/mysql/dc_business_user/dc_alarm_hour_cfg.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_test_equip.frm' to '/var/lib/mysql/dc_business_user/dc_push_test_equip.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_track_info.frm' to '/var/lib/mysql/dc_business_user/dc_channel_track_info.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_rt_cfg.frm' to '/var/lib/mysql/dc_business_user/dc_rt_cfg.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_currency_type.MYI' to '/var/lib/mysql/dc_business_user/dc_currency_type.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_coop.MYD' to '/var/lib/mysql/dc_business_user/dc_games_coop.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_month_report.MYD' to '/var/lib/mysql/dc_business_user/dc_month_report.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_currency_type.MYD' to '/var/lib/mysql/dc_business_user/dc_currency_type.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sdk_version.MYI' to '/var/lib/mysql/dc_business_user/dc_sdk_version.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_player_group.MYD' to '/var/lib/mysql/dc_business_user/dc_push_player_group.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_h5.frm' to '/var/lib/mysql/dc_business_user/dc_menu_h5.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_g.MYI' to '/var/lib/mysql/dc_business_user/dc_menu_g.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_fileinfo.MYI' to '/var/lib/mysql/dc_business_user/dc_fileinfo.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_uid.frm' to '/var/lib/mysql/dc_business_user/dc_cocos_uid.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_shorturl.frm' to '/var/lib/mysql/dc_business_user/dc_channel_shorturl.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sso_authinfo.MYD' to '/var/lib/mysql/dc_business_user/dc_sso_authinfo.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_company_coop.MYI' to '/var/lib/mysql/dc_business_user/dc_company_coop.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_operator_type.MYI' to '/var/lib/mysql/dc_business_user/dc_operator_type.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_week_report.frm' to '/var/lib/mysql/dc_business_user/dc_week_report.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_test_equip.MYI' to '/var/lib/mysql/dc_business_user/dc_push_test_equip.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_gamedeletelog.frm' to '/var/lib/mysql/dc_business_user/dc_gamedeletelog.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_gamedatacenter_cache.MYD' to '/var/lib/mysql/dc_business_user/dc_gamedatacenter_cache.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_child_account_exp.MYD' to '/var/lib/mysql/dc_business_user/dc_child_account_exp.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_plugins.MYD' to '/var/lib/mysql/dc_business_user/dc_game_plugins.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_app.MYI' to '/var/lib/mysql/dc_business_user/dc_cocos_app.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_coop.frm' to '/var/lib/mysql/dc_business_user/dc_games_coop.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_verifycode.frm' to '/var/lib/mysql/dc_business_user/dc_verifycode.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_user_menu_h5.MYD' to '/var/lib/mysql/dc_business_user/dc_user_menu_h5.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm_exclude.MYD' to '/var/lib/mysql/dc_business_user/dc_alarm_exclude.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_uid.MYI' to '/var/lib/mysql/dc_business_user/dc_cocos_uid.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_type.MYI' to '/var/lib/mysql/dc_business_user/dc_game_type.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_type_bak.MYD' to '/var/lib/mysql/dc_business_user/dc_game_type_bak.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_auth_coop.MYI' to '/var/lib/mysql/dc_business_user/dc_auth_coop.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_type_bak.MYI' to '/var/lib/mysql/dc_business_user/dc_game_type_bak.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_g.MYD' to '/var/lib/mysql/dc_business_user/dc_menu_g.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/AuthInfoProtocol.MYI' to '/var/lib/mysql/dc_business_user/AuthInfoProtocol.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_type_bak.frm' to '/var/lib/mysql/dc_business_user/dc_game_type_bak.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_bak.frm' to '/var/lib/mysql/dc_business_user/dc_users_bak.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_day_alarm_item.frm' to '/var/lib/mysql/dc_business_user/dc_day_alarm_item.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sub_data_channel.MYD' to '/var/lib/mysql/dc_business_user/dc_sub_data_channel.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_fail_info.MYD' to '/var/lib/mysql/dc_business_user/dc_push_fail_info.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_player_group.MYI' to '/var/lib/mysql/dc_business_user/dc_push_player_group.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_dateye_service.frm' to '/var/lib/mysql/dc_business_user/dc_cocos_dateye_service.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_uniq_config.frm' to '/var/lib/mysql/dc_business_user/dc_uniq_config.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_custom_id_global.MYI' to '/var/lib/mysql/dc_business_user/dc_custom_id_global.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_user_menu_exp.MYD' to '/var/lib/mysql/dc_business_user/dc_user_menu_exp.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/monitorData.MYI' to '/var/lib/mysql/dc_business_user/monitorData.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_config2.MYD' to '/var/lib/mysql/dc_business_user/dc_game_config2.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_operator_type.MYD' to '/var/lib/mysql/dc_business_user/dc_operator_type.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_gamedatacenter_cache.frm' to '/var/lib/mysql/dc_business_user/dc_gamedatacenter_cache.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_day_report.frm' to '/var/lib/mysql/dc_business_user/dc_day_report.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_bind.MYD' to '/var/lib/mysql/dc_business_user/dc_push_bind.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/AuthInfoProtocol.frm' to '/var/lib/mysql/dc_business_user/AuthInfoProtocol.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm.MYD' to '/var/lib/mysql/dc_business_user/dc_alarm.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_week_report.MYI' to '/var/lib/mysql/dc_business_user/dc_week_report.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_custom_module_cfg.frm' to '/var/lib/mysql/dc_business_user/dc_custom_module_cfg.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sso_authinfo.frm' to '/var/lib/mysql/dc_business_user/dc_sso_authinfo.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_module_mr.frm' to '/var/lib/mysql/dc_business_user/dc_module_mr.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_track_info.MYD' to '/var/lib/mysql/dc_business_user/dc_channel_track_info.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_testflag.frm' to '/var/lib/mysql/dc_business_user/dc_game_testflag.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_url.frm' to '/var/lib/mysql/dc_business_user/dc_game_url.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_inner_alarm_cfg.MYD' to '/var/lib/mysql/dc_business_user/dc_inner_alarm_cfg.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_keyword_baidu.MYI' to '/var/lib/mysql/dc_business_user/dc_keyword_baidu.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_operator_type.frm' to '/var/lib/mysql/dc_business_user/dc_operator_type.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_dateye_service.MYI' to '/var/lib/mysql/dc_business_user/dc_cocos_dateye_service.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_bind.frm' to '/var/lib/mysql/dc_business_user/dc_push_bind.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_inner_alarm_cfg.MYI' to '/var/lib/mysql/dc_business_user/dc_inner_alarm_cfg.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_config.MYD' to '/var/lib/mysql/dc_business_user/dc_config.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_type.MYD' to '/var/lib/mysql/dc_business_user/dc_game_type.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_g.frm' to '/var/lib/mysql/dc_business_user/dc_menu_g.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_selector.MYI' to '/var/lib/mysql/dc_business_user/dc_users_selector.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_fileinfo.MYD' to '/var/lib/mysql/dc_business_user/dc_fileinfo.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_error_detail.MYD' to '/var/lib/mysql/dc_business_user/dc_error_detail.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_plugins.MYI' to '/var/lib/mysql/dc_business_user/dc_game_plugins.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_app_detail.frm' to '/var/lib/mysql/dc_business_user/dc_app_detail.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_clusteranalysis.MYD' to '/var/lib/mysql/dc_business_user/dc_clusteranalysis.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_plugins2.frm' to '/var/lib/mysql/dc_business_user/dc_plugins2.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_company_coop.MYD' to '/var/lib/mysql/dc_business_user/dc_company_coop.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_keyword_baidu.MYD' to '/var/lib/mysql/dc_business_user/dc_keyword_baidu.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sub_data.MYD' to '/var/lib/mysql/dc_business_user/dc_sub_data.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_active_callbackurl.MYD' to '/var/lib/mysql/dc_business_user/dc_active_callbackurl.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_user_menu_exp.MYI' to '/var/lib/mysql/dc_business_user/dc_user_menu_exp.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_month_report.MYI' to '/var/lib/mysql/dc_business_user/dc_month_report.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_del_user_logs.frm' to '/var/lib/mysql/dc_business_user/dc_del_user_logs.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_ios_device.frm' to '/var/lib/mysql/dc_business_user/dc_ios_device.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/AuthInfo.MYI' to '/var/lib/mysql/dc_business_user/AuthInfo.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_company.frm' to '/var/lib/mysql/dc_business_user/dc_company.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_child_account_exp.frm' to '/var/lib/mysql/dc_business_user/dc_child_account_exp.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_week_report.MYD' to '/var/lib/mysql/dc_business_user/dc_week_report.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_clusteranalysis.MYI' to '/var/lib/mysql/dc_business_user/dc_clusteranalysis.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm_exclude.frm' to '/var/lib/mysql/dc_business_user/dc_alarm_exclude.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_child_account_h5.MYI' to '/var/lib/mysql/dc_business_user/dc_child_account_h5.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_testflag.MYD' to '/var/lib/mysql/dc_business_user/dc_game_testflag.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users.frm' to '/var/lib/mysql/dc_business_user/dc_users.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_ios_device.MYI' to '/var/lib/mysql/dc_business_user/dc_ios_device.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_exp.frm' to '/var/lib/mysql/dc_business_user/dc_menu_exp.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_config.MYI' to '/var/lib/mysql/dc_business_user/dc_config.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_config.MYI' to '/var/lib/mysql/dc_business_user/dc_game_config.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_feedback.frm' to '/var/lib/mysql/dc_business_user/dc_feedback.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_h5.MYD' to '/var/lib/mysql/dc_business_user/dc_menu_h5.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_app.MYD' to '/var/lib/mysql/dc_business_user/dc_cocos_app.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_multi_config.MYI' to '/var/lib/mysql/dc_business_user/dc_multi_config.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_plugins2.MYI' to '/var/lib/mysql/dc_business_user/dc_plugins2.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_active_callbackurl.frm' to '/var/lib/mysql/dc_business_user/dc_active_callbackurl.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_dataclean.frm' to '/var/lib/mysql/dc_business_user/dc_dataclean.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_datacenter_cache.MYI' to '/var/lib/mysql/dc_business_user/dc_games_datacenter_cache.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_alarm_hour_item.frm' to '/var/lib/mysql/dc_business_user/dc_alarm_hour_item.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_exchange_rate.MYI' to '/var/lib/mysql/dc_business_user/dc_exchange_rate.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_error_detail.frm' to '/var/lib/mysql/dc_business_user/dc_error_detail.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/AuthInfo.frm' to '/var/lib/mysql/dc_business_user/AuthInfo.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/AuthInfo.MYD' to '/var/lib/mysql/dc_business_user/AuthInfo.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_coop.MYI' to '/var/lib/mysql/dc_business_user/dc_games_coop.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_track_info_bak.frm' to '/var/lib/mysql/dc_business_user/dc_channel_track_info_bak.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_fileinfo.frm' to '/var/lib/mysql/dc_business_user/dc_fileinfo.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_custom_id_global.frm' to '/var/lib/mysql/dc_business_user/dc_custom_id_global.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_selector.frm' to '/var/lib/mysql/dc_business_user/dc_users_selector.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_multi_config.frm' to '/var/lib/mysql/dc_business_user/dc_multi_config.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_day_report.MYI' to '/var/lib/mysql/dc_business_user/dc_day_report.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_open_auth.MYD' to '/var/lib/mysql/dc_business_user/dc_open_auth.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_uniq_config.MYD' to '/var/lib/mysql/dc_business_user/dc_uniq_config.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_app.frm' to '/var/lib/mysql/dc_business_user/dc_cocos_app.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_currency_type.frm' to '/var/lib/mysql/dc_business_user/dc_currency_type.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_ios_device.MYD' to '/var/lib/mysql/dc_business_user/dc_ios_device.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_user_menu_h5.MYI' to '/var/lib/mysql/dc_business_user/dc_user_menu_h5.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_company_2.frm' to '/var/lib/mysql/dc_business_user/dc_company_2.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_track_info_bak.MYD' to '/var/lib/mysql/dc_business_user/dc_channel_track_info_bak.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sub_data_channel.MYI' to '/var/lib/mysql/dc_business_user/dc_sub_data_channel.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_fail_info.frm' to '/var/lib/mysql/dc_business_user/dc_push_fail_info.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_gamedatacenter_cache.MYI' to '/var/lib/mysql/dc_business_user/dc_gamedatacenter_cache.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_certificate.frm' to '/var/lib/mysql/dc_business_user/dc_push_certificate.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sdk_version.frm' to '/var/lib/mysql/dc_business_user/dc_sdk_version.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_rt_cfg.MYD' to '/var/lib/mysql/dc_business_user/dc_rt_cfg.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_exchange_rate.frm' to '/var/lib/mysql/dc_business_user/dc_exchange_rate.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_interface_page.frm' to '/var/lib/mysql/dc_business_user/dc_interface_page.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_company_investor.frm' to '/var/lib/mysql/dc_business_user/dc_company_investor.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_del_user_logs.MYI' to '/var/lib/mysql/dc_business_user/dc_del_user_logs.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_player_group.frm' to '/var/lib/mysql/dc_business_user/dc_push_player_group.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_for_coolpad.frm' to '/var/lib/mysql/dc_business_user/dc_games_for_coolpad.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_config.frm' to '/var/lib/mysql/dc_business_user/dc_config.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_message_config.frm' to '/var/lib/mysql/dc_business_user/dc_push_message_config.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_bind.MYI' to '/var/lib/mysql/dc_business_user/dc_push_bind.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_config2.frm' to '/var/lib/mysql/dc_business_user/dc_game_config2.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_del_user_logs.MYD' to '/var/lib/mysql/dc_business_user/dc_del_user_logs.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_keyword_baidu.frm' to '/var/lib/mysql/dc_business_user/dc_keyword_baidu.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_track_info_bak.MYI' to '/var/lib/mysql/dc_business_user/dc_channel_track_info_bak.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_h5.MYI' to '/var/lib/mysql/dc_business_user/dc_menu_h5.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_sub_data_channel.frm' to '/var/lib/mysql/dc_business_user/dc_sub_data_channel.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_page2.frm' to '/var/lib/mysql/dc_business_user/dc_users_page2.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/db.opt' to '/var/lib/mysql/dc_business_user/db.opt'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_user_menu_h5.frm' to '/var/lib/mysql/dc_business_user/dc_user_menu_h5.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_uid.MYD' to '/var/lib/mysql/dc_business_user/dc_cocos_uid.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_games_for_coolpad.MYI' to '/var/lib/mysql/dc_business_user/dc_games_for_coolpad.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_fileinfo_bak.frm' to '/var/lib/mysql/dc_business_user/dc_fileinfo_bak.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_game_testflag.MYI' to '/var/lib/mysql/dc_business_user/dc_game_testflag.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/monitorData.MYD' to '/var/lib/mysql/dc_business_user/monitorData.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_day_report.MYD' to '/var/lib/mysql/dc_business_user/dc_day_report.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_plugins2.MYD' to '/var/lib/mysql/dc_business_user/dc_plugins2.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_reg.frm' to '/var/lib/mysql/dc_business_user/dc_push_reg.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/AuthInfoProtocol.MYD' to '/var/lib/mysql/dc_business_user/AuthInfoProtocol.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_user_menu_exp.frm' to '/var/lib/mysql/dc_business_user/dc_user_menu_exp.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_track_info.MYI' to '/var/lib/mysql/dc_business_user/dc_channel_track_info.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_push_message_config.MYI' to '/var/lib/mysql/dc_business_user/dc_push_message_config.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_auth_coop.frm' to '/var/lib/mysql/dc_business_user/dc_auth_coop.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_askdemo.frm' to '/var/lib/mysql/dc_business_user/dc_users_askdemo.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_users_games.frm' to '/var/lib/mysql/dc_business_user/dc_users_games.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu.frm' to '/var/lib/mysql/dc_business_user/dc_menu.frm'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_error_detail.MYI' to '/var/lib/mysql/dc_business_user/dc_error_detail.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_g2.MYI' to '/var/lib/mysql/dc_business_user/dc_menu_g2.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_cocos_dateye_service.MYD' to '/var/lib/mysql/dc_business_user/dc_cocos_dateye_service.MYD'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_channel_sign.MYI' to '/var/lib/mysql/dc_business_user/dc_channel_sign.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_rt_cfg.MYI' to '/var/lib/mysql/dc_business_user/dc_rt_cfg.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_child_account_exp.MYI' to '/var/lib/mysql/dc_business_user/dc_child_account_exp.MYI'
innobackupex: Copying '/backup/base_20170120/dc_business_user/dc_menu_exp.MYI' to '/var/lib/mysql/dc_business_user/dc_menu_exp.MYI'
innobackupex: Creating directory '/var/lib/mysql/ga2_metastore'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/drop_me.MYI' to '/var/lib/mysql/ga2_metastore/drop_me.MYI'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/DATABASE_PARAMS.frm' to '/var/lib/mysql/ga2_metastore/DATABASE_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/COLUMNS_V2.frm' to '/var/lib/mysql/ga2_metastore/COLUMNS_V2.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/ROLES.frm' to '/var/lib/mysql/ga2_metastore/ROLES.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/PARTITION_KEY_VALS.frm' to '/var/lib/mysql/ga2_metastore/PARTITION_KEY_VALS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/CDS.frm' to '/var/lib/mysql/ga2_metastore/CDS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SD_PARAMS.frm' to '/var/lib/mysql/ga2_metastore/SD_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/drop_me.MYD' to '/var/lib/mysql/ga2_metastore/drop_me.MYD'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/PART_PRIVS.frm' to '/var/lib/mysql/ga2_metastore/PART_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SEQUENCE_TABLE.frm' to '/var/lib/mysql/ga2_metastore/SEQUENCE_TABLE.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/TABLE_PARAMS.frm' to '/var/lib/mysql/ga2_metastore/TABLE_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/TBL_PRIVS.frm' to '/var/lib/mysql/ga2_metastore/TBL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/PART_COL_STATS.frm' to '/var/lib/mysql/ga2_metastore/PART_COL_STATS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/drop_me.frm' to '/var/lib/mysql/ga2_metastore/drop_me.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SORT_COLS.frm' to '/var/lib/mysql/ga2_metastore/SORT_COLS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/GLOBAL_PRIVS.frm' to '/var/lib/mysql/ga2_metastore/GLOBAL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SKEWED_STRING_LIST.frm' to '/var/lib/mysql/ga2_metastore/SKEWED_STRING_LIST.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/PARTITION_PARAMS.frm' to '/var/lib/mysql/ga2_metastore/PARTITION_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/VERSION.frm' to '/var/lib/mysql/ga2_metastore/VERSION.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SERDES.frm' to '/var/lib/mysql/ga2_metastore/SERDES.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/BUCKETING_COLS.frm' to '/var/lib/mysql/ga2_metastore/BUCKETING_COLS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SKEWED_COL_VALUE_LOC_MAP.frm' to '/var/lib/mysql/ga2_metastore/SKEWED_COL_VALUE_LOC_MAP.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/PARTITION_KEYS.frm' to '/var/lib/mysql/ga2_metastore/PARTITION_KEYS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SKEWED_COL_NAMES.frm' to '/var/lib/mysql/ga2_metastore/SKEWED_COL_NAMES.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/DBS.frm' to '/var/lib/mysql/ga2_metastore/DBS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/IDXS.frm' to '/var/lib/mysql/ga2_metastore/IDXS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/PARTITIONS.frm' to '/var/lib/mysql/ga2_metastore/PARTITIONS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/TBLS.frm' to '/var/lib/mysql/ga2_metastore/TBLS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/TAB_COL_STATS.frm' to '/var/lib/mysql/ga2_metastore/TAB_COL_STATS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/FUNC_RU.frm' to '/var/lib/mysql/ga2_metastore/FUNC_RU.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/DB_PRIVS.frm' to '/var/lib/mysql/ga2_metastore/DB_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SKEWED_STRING_LIST_VALUES.frm' to '/var/lib/mysql/ga2_metastore/SKEWED_STRING_LIST_VALUES.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/PART_COL_PRIVS.frm' to '/var/lib/mysql/ga2_metastore/PART_COL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/INDEX_PARAMS.frm' to '/var/lib/mysql/ga2_metastore/INDEX_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/db.opt' to '/var/lib/mysql/ga2_metastore/db.opt'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/FUNCS.frm' to '/var/lib/mysql/ga2_metastore/FUNCS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SDS.frm' to '/var/lib/mysql/ga2_metastore/SDS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/TBL_COL_PRIVS.frm' to '/var/lib/mysql/ga2_metastore/TBL_COL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SKEWED_VALUES.frm' to '/var/lib/mysql/ga2_metastore/SKEWED_VALUES.frm'
innobackupex: Copying '/backup/base_20170120/ga2_metastore/SERDE_PARAMS.frm' to '/var/lib/mysql/ga2_metastore/SERDE_PARAMS.frm'
innobackupex: Creating directory '/var/lib/mysql/warehouse'
innobackupex: Copying '/backup/base_20170120/warehouse/tag.frm' to '/var/lib/mysql/warehouse/tag.frm'
innobackupex: Copying '/backup/base_20170120/warehouse/app_tag.frm' to '/var/lib/mysql/warehouse/app_tag.frm'
innobackupex: Copying '/backup/base_20170120/warehouse/db.opt' to '/var/lib/mysql/warehouse/db.opt'
innobackupex: Copying '/backup/base_20170120/warehouse/city.frm' to '/var/lib/mysql/warehouse/city.frm'
innobackupex: Creating directory '/var/lib/mysql/metastore_db'
innobackupex: Copying '/backup/base_20170120/metastore_db/TXNS.frm' to '/var/lib/mysql/metastore_db/TXNS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DATABASE_PARAMS.frm' to '/var/lib/mysql/metastore_db/DATABASE_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1453117797396.frm' to '/var/lib/mysql/metastore_db/DELETEME1453117797396.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/ROLE_MAP.frm' to '/var/lib/mysql/metastore_db/ROLE_MAP.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/COLUMNS_V2.frm' to '/var/lib/mysql/metastore_db/COLUMNS_V2.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/NOTIFICATION_LOG.frm' to '/var/lib/mysql/metastore_db/NOTIFICATION_LOG.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/ROLES.frm' to '/var/lib/mysql/metastore_db/ROLES.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1448009357534.frm' to '/var/lib/mysql/metastore_db/DELETEME1448009357534.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PARTITION_KEY_VALS.frm' to '/var/lib/mysql/metastore_db/PARTITION_KEY_VALS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/CDS.frm' to '/var/lib/mysql/metastore_db/CDS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/HIVE_LOCKS.frm' to '/var/lib/mysql/metastore_db/HIVE_LOCKS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/NEXT_LOCK_ID.frm' to '/var/lib/mysql/metastore_db/NEXT_LOCK_ID.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SD_PARAMS.frm' to '/var/lib/mysql/metastore_db/SD_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PART_PRIVS.frm' to '/var/lib/mysql/metastore_db/PART_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1448087047058.frm' to '/var/lib/mysql/metastore_db/DELETEME1448087047058.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/NUCLEUS_TABLES.frm' to '/var/lib/mysql/metastore_db/NUCLEUS_TABLES.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1448009576342.frm' to '/var/lib/mysql/metastore_db/DELETEME1448009576342.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SEQUENCE_TABLE.frm' to '/var/lib/mysql/metastore_db/SEQUENCE_TABLE.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TYPES.frm' to '/var/lib/mysql/metastore_db/TYPES.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TABLE_PARAMS.frm' to '/var/lib/mysql/metastore_db/TABLE_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TBL_PRIVS.frm' to '/var/lib/mysql/metastore_db/TBL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TYPE_FIELDS.frm' to '/var/lib/mysql/metastore_db/TYPE_FIELDS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TXN_COMPONENTS.frm' to '/var/lib/mysql/metastore_db/TXN_COMPONENTS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PART_COL_STATS.frm' to '/var/lib/mysql/metastore_db/PART_COL_STATS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SORT_COLS.frm' to '/var/lib/mysql/metastore_db/SORT_COLS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/GLOBAL_PRIVS.frm' to '/var/lib/mysql/metastore_db/GLOBAL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1453285029251.frm' to '/var/lib/mysql/metastore_db/DELETEME1453285029251.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELEGATION_TOKENS.frm' to '/var/lib/mysql/metastore_db/DELEGATION_TOKENS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/NEXT_COMPACTION_QUEUE_ID.frm' to '/var/lib/mysql/metastore_db/NEXT_COMPACTION_QUEUE_ID.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SKEWED_STRING_LIST.frm' to '/var/lib/mysql/metastore_db/SKEWED_STRING_LIST.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PARTITION_PARAMS.frm' to '/var/lib/mysql/metastore_db/PARTITION_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PARTITION_EVENTS.frm' to '/var/lib/mysql/metastore_db/PARTITION_EVENTS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/VERSION.frm' to '/var/lib/mysql/metastore_db/VERSION.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SERDES.frm' to '/var/lib/mysql/metastore_db/SERDES.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1448251271327.frm' to '/var/lib/mysql/metastore_db/DELETEME1448251271327.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/BUCKETING_COLS.frm' to '/var/lib/mysql/metastore_db/BUCKETING_COLS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SKEWED_COL_VALUE_LOC_MAP.frm' to '/var/lib/mysql/metastore_db/SKEWED_COL_VALUE_LOC_MAP.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/COMPACTION_QUEUE.frm' to '/var/lib/mysql/metastore_db/COMPACTION_QUEUE.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PARTITION_KEYS.frm' to '/var/lib/mysql/metastore_db/PARTITION_KEYS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SKEWED_COL_NAMES.frm' to '/var/lib/mysql/metastore_db/SKEWED_COL_NAMES.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DBS.frm' to '/var/lib/mysql/metastore_db/DBS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/IDXS.frm' to '/var/lib/mysql/metastore_db/IDXS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PARTITIONS.frm' to '/var/lib/mysql/metastore_db/PARTITIONS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TBLS.frm' to '/var/lib/mysql/metastore_db/TBLS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TAB_COL_STATS.frm' to '/var/lib/mysql/metastore_db/TAB_COL_STATS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1450784412225.frm' to '/var/lib/mysql/metastore_db/DELETEME1450784412225.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/FUNC_RU.frm' to '/var/lib/mysql/metastore_db/FUNC_RU.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DB_PRIVS.frm' to '/var/lib/mysql/metastore_db/DB_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/MASTER_KEYS.frm' to '/var/lib/mysql/metastore_db/MASTER_KEYS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SKEWED_STRING_LIST_VALUES.frm' to '/var/lib/mysql/metastore_db/SKEWED_STRING_LIST_VALUES.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1450506600477.frm' to '/var/lib/mysql/metastore_db/DELETEME1450506600477.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/COMPLETED_TXN_COMPONENTS.frm' to '/var/lib/mysql/metastore_db/COMPLETED_TXN_COMPONENTS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/PART_COL_PRIVS.frm' to '/var/lib/mysql/metastore_db/PART_COL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/INDEX_PARAMS.frm' to '/var/lib/mysql/metastore_db/INDEX_PARAMS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/db.opt' to '/var/lib/mysql/metastore_db/db.opt'
innobackupex: Copying '/backup/base_20170120/metastore_db/NEXT_TXN_ID.frm' to '/var/lib/mysql/metastore_db/NEXT_TXN_ID.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1448268744092.frm' to '/var/lib/mysql/metastore_db/DELETEME1448268744092.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/FUNCS.frm' to '/var/lib/mysql/metastore_db/FUNCS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SDS.frm' to '/var/lib/mysql/metastore_db/SDS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/TBL_COL_PRIVS.frm' to '/var/lib/mysql/metastore_db/TBL_COL_PRIVS.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SKEWED_VALUES.frm' to '/var/lib/mysql/metastore_db/SKEWED_VALUES.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/NOTIFICATION_SEQUENCE.frm' to '/var/lib/mysql/metastore_db/NOTIFICATION_SEQUENCE.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/DELETEME1448009419602.frm' to '/var/lib/mysql/metastore_db/DELETEME1448009419602.frm'
innobackupex: Copying '/backup/base_20170120/metastore_db/SERDE_PARAMS.frm' to '/var/lib/mysql/metastore_db/SERDE_PARAMS.frm'
innobackupex: Creating directory '/var/lib/mysql/test'
innobackupex: Copying '/backup/base_20170120/test/city.MYI' to '/var/lib/mysql/test/city.MYI'
innobackupex: Copying '/backup/base_20170120/test/db.opt' to '/var/lib/mysql/test/db.opt'
innobackupex: Copying '/backup/base_20170120/test/city.MYD' to '/var/lib/mysql/test/city.MYD'
innobackupex: Copying '/backup/base_20170120/test/city.frm' to '/var/lib/mysql/test/city.frm'
innobackupex: Creating directory '/var/lib/mysql/summarydata'
innobackupex: Copying '/backup/base_20170120/summarydata/province_sumdata.MYD' to '/var/lib/mysql/summarydata/province_sumdata.MYD'
innobackupex: Copying '/backup/base_20170120/summarydata/province_sumdata.MYI' to '/var/lib/mysql/summarydata/province_sumdata.MYI'
innobackupex: Copying '/backup/base_20170120/summarydata/db.opt' to '/var/lib/mysql/summarydata/db.opt'
innobackupex: Copying '/backup/base_20170120/summarydata/province_sumdata.frm' to '/var/lib/mysql/summarydata/province_sumdata.frm'

innobackupex: Starting to copy InnoDB system tablespace
innobackupex: in '/backup/base_20170120'
innobackupex: back to original InnoDB data directory '/var/lib/mysql'
innobackupex: Copying file '/backup/base_20170120/ibdata1'

innobackupex: Starting to copy InnoDB log files
innobackupex: in '/backup/base_20170120'
innobackupex: back to original InnoDB log directory '/var/lib/mysql'
innobackupex: Finished copying back files.

170120 16:07:03  innobackupex: completed OK!
```

5、修改属主属组   
``
[root@dchadoop3 ~]# ll /var/lib/mysql
``

```
total 15386684
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 company
drwxr-xr-x. 2 root root       16384 Jan 20 16:05 dc_business_user
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 dc_scheduleserver_hadoop2
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 ga2_metastore
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 huangchengbo
-rw-r-----. 1 root root 15755902976 Jan 20 15:41 ibdata1
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 metastore_db
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 mysql
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 mytest_001
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 summarydata
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 test
drwxr-xr-x. 2 root root        4096 Jan 20 16:05 warehouse
```

``
[root@dchadoop3 ~]# chown mysql:mysql -R /var/lib/mysql
``
``
[root@dchadoop3 ~]# ll /var/lib/mysql/
``
```
total 15386684
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 company
drwxr-xr-x. 2 mysql mysql       16384 Jan 20 16:05 dc_business_user
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 dc_scheduleserver_hadoop2
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 ga2_metastore
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 huangchengbo
-rw-r-----. 1 mysql mysql 15755902976 Jan 20 15:41 ibdata1
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 metastore_db
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 mysql
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 mytest_001
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 summarydata
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 test
drwxr-xr-x. 2 mysql mysql        4096 Jan 20 16:05 warehouse
```

5、验证   

```

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 1
Server version: 5.1.73 Source distribution

Copyright (c) 2000, 2013, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
ERROR 2006 (HY000): MySQL server has gone away
No connection. Trying to reconnect...
Connection id:    1
Current database: *** NONE ***

+---------------------------+
| Database                  |
+---------------------------+
| information_schema        |
| company                   |
| dc_business_user          |
| dc_scheduleserver_hadoop2 |
| ga2_metastore             |
| huangchengbo              |
| metastore_db              |
| mysql                     |
| mytest_001                |
| summarydata               |
| test                      |
| warehouse                 |
+---------------------------+
12 rows in set (0.00 sec)

mysql>

```