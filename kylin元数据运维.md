背景
===================

元数据保存了kylin 实例的状态信息，在kylin集群的运维中至关重要，假如元数据丢失了，kylin集群将无法正常工作。



1.元数据的存储方式
-------------

kylin提供了一个抽象类来定义元数据的存储：

```
     abstract public class ResourceStore
```

当前kylin 有两种元数据存储方式：

> - 本地文件系统存储，即实现ResourceStore的**FileResourceStore** 
> - hbase存储，即实现ResourceStore的**HBaseResourceStore** 

可通过kylin的配置文件**kylin.properties**中的如下配置项，配置kylin的元数据信息:

```
kylin.metadata.url
```
默认值为：
```
kylin.metadata.url=kylin_metadata@hbase
```
即 元数据保存在hbase中的kylin_metadata表中,假如要将元数据保存在本地文件系统，需将**kylin.metadata.url** 指向本地文件系统的一个**绝对路径**,如：

```
kylin.metadata.url=/home/${username}/${kylin_home}/kylin_metada
```
注：相对路径会出错。

2.选择hbase存储元数据时，元数据表中单条记录的最大值
-------------
 当选择元数据存储在hbase中时，并非所有的数据都在hbase中，当待存储的数据大于一个最大值**kvSizeLimit**时，数据将被保存在HDFS中的 ${kylin.hdfs.working.dir}/kylin_metadata/ 目录下，默认值为：/kylin/kylin_metadata/ ，代码如下：

```
private Put buildPut(String resPath, long ts, byte[] row, byte[] content, HTableInterface table) throws IOException {
        int kvSizeLimit = this.kylinConfig.getHBaseKeyValueSize();
        if (content.length > kvSizeLimit) {
            writeLargeCellToHdfs(resPath, content, table);
            content = BytesUtil.EMPTY_BYTE_ARRAY;
        }

        Put put = new Put(row);
        put.add(B_FAMILY, B_COLUMN, content);
        put.add(B_FAMILY, B_COLUMN_TS, Bytes.toBytes(ts));

        return put;
    }
```
kvSizeLimit 获取代码如下：

```
public int getHBaseKeyValueSize() {
        return Integer.parseInt(this.getOptional("kylin.hbase.client.keyvalue.maxsize", "10485760"));
    }
```
默认值为10M，可在kylin的配置文件**kylin.properties**中配置 **kylin.hbase.client.keyvalue.maxsize**的大小，调整该值。

**注意**: **kylin.hbase.client.keyvalue.maxsize**的值很重要，因为kylin为了提高性能，将元数据表kylin_metadata缓存在hbase进程的内存中，如下图：
![image](/uploads/4712f493b549758ee613812c404fe575/image.png)



随着每天 cube的增量build，该表会越来越大。假如不及时清理历史数据，将会使hbase的进程发生 OutOfMemoryError错误！


3.元数据的备份
-------------
当前kylin的元数据只能采取冷备份的方式。

可利用crontab 在${KYLIN_HOME}下，每天执行./bin/metastore.sh backup命令，kylin会将元数据信息保存如下目录：

```
${KYLIN_HOME}/meta_backups/meta_year_month_day_hour_minute_second
```

当kylin元数据损坏或不一致，可采用如下命令恢复：

**step1:**  
    
        cd  ${KYLIN_HOME}
        sh  ./bin/metastore.sh reset

**step2:**  

       sh  ./bin/metastore.sh restore $KYLIN_HOME/meta_backups/meta_xxxx_xx_xx_xx_xx_xx


