###  问题
fabric设计为在交互环境下面执行默认需要stdin,如果在nohup中直接运行fabric任务，可能会报下面的错误
```bash
Traceback (most recent call last):
  File "/usr/lib/python2.6/site-packages/fabric/main.py", line 756, in main
    *args, **kwargs
  File "/usr/lib/python2.6/site-packages/fabric/tasks.py", line 387, in execute
    multiprocessing
  File "/usr/lib/python2.6/site-packages/fabric/tasks.py", line 277, in _execute
    return task.run(*args, **kwargs)
  File "/usr/lib/python2.6/site-packages/fabric/tasks.py", line 174, in run
    return self.wrapped(*args, **kwargs)
  File "/home/hadoop2/hadoop-robot.py", line 167, in execute_remote_cmd
    run(cmd)
  File "/usr/lib/python2.6/site-packages/fabric/network.py", line 677, in host_prompting_wrapper
    return func(*args, **kwargs)
  File "/usr/lib/python2.6/site-packages/fabric/operations.py", line 1088, in run
    shell_escape=shell_escape, capture_buffer_size=capture_buffer_size,
  File "/usr/lib/python2.6/site-packages/fabric/operations.py", line 931, in _run_command
    capture_buffer_size=capture_buffer_size)
  File "/usr/lib/python2.6/site-packages/fabric/operations.py", line 813, in _execute
    worker.raise_if_needed()
  File "/usr/lib/python2.6/site-packages/fabric/thread_handling.py", line 12, in wrapper
    callable(*args, **kwargs)
  File "/usr/lib/python2.6/site-packages/fabric/io.py", line 231, in input_loop
    byte = msvcrt.getch() if win32 else sys.stdin.read(1)
IOError: [Errno 9] Bad file descriptor
```

### 方案
将/dev/null定向为fabric任务的输入,像下面这样
```bash
nohup fab -f fab-file execute_cmd < /dev/null >> /tmp/fab.log 2>&1 &
```
