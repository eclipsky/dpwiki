### hiveserver2和metastore进程参数错误修正   
#### 背景   
春节期间现网环境dchdmaster1节点上的hiveserver2发生一次OOM导致hiveserver2进程死掉，同时metastore进程死掉，进程死掉后并没有被crontab重新拉起。
#### 问题分析 
查看hiveserver2进程日志发现如下错误
```
java.lang.OutOfMemoryError: PermGen space
Dumping heap to /tmp/hadoop2/hive/hive-hao.hprof ...
Heap dump file created [602264729 bytes in 6.155 secs]
#
# java.lang.OutOfMemoryError: PermGen space
# -XX:OnOutOfMemoryError="/home/hadoop2/hive/bin/stop-metastore.sh"
#   Executing /bin/sh -c "/home/hadoop2/hive/bin/stop-metastore.sh"...
```
可以看到hiveserver2进程在发生OOM后又执行了脚本stop-metastore.sh，很显然该脚本是关闭了metastore进程，这说明hiveserver2进程挂掉，也杀死了metastore进程（脚本metastore.sh实际是kill操作）。    

执行命令```ps -aux | egrep -i --color "hiveserver2|metastore"```查看进程启动参数
![image](/uploads/72b9ff9c9bbe827c7d422bbe7c8d46ee/image.png)   
可以看到hiveserver2进程配置的参数`-XX:OnOutOfMemoryError=/home/hadoop2/hive/bin/stop-metastore.sh`，该参数导致了metastore进程被杀死。   
进一步查看${HIVE_HOME}/conf/hive-env.sh脚本可以看到其中的配置
```
export GC_OPTIONS="-XX:+UseG1GC -XX:MaxGCPauseMillis=300 -XX:InitiatingHeapOccupancyPercent=35 -XX:PermSize=198M -XX:MaxPermSize=256M -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCDetails -XX:+Print
GCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=20M "
export HADOOP_OPTS="-Xmx4096m -Xss512k -XX:HeapDumpPath=/tmp/hadoop2/hive/hive-hao.hprof -Xloggc:/tmp/hadoop2/hive/hive-hao.gc.log $GC_OPTIONS -XX:OnOutOfMemoryError=/home/hadoop2/hive/bin/stop-me
tastore.sh "
```
查看${HIVE_HOME}/bin/hive源码，发现每次执行start-hiveserver2.sh和start-metastore.sh脚本（这两个为自定义脚本）都会加载上述配置，这就导致了hiveserver2进程和metastore进程的启动参数都相同，所以发生了上述hiveserver2进程在挂掉后也导致了metastore进程挂掉   

### 解决
现网环境${HIVE_HOME}/bin/start-hiveserver2.sh和start-metastore.sh脚本是为了将hiveserver2进程和metastore进程放置到后台而自己实现的，在cdh-hive发布版并不存在，考虑到进程的启动方式可能是`${HIVE_HOME}/bin/bin/hive --service hiveserver2/metastore`，那么，如果只是修改了start-hiveserver2.sh或者start-metastore.sh，这种方式将无法加载到修改后的配置，故直接修改了${HIVE_HOME}/bin/hive脚本，在该脚本中加入如下逻辑
```
###### BEGIN: added by Fonder on 20170206
if [ "$SERVICE" = "hiveserver2" ]; then
   export HADOOP_OPTS=" $HADOOP_OPTS -XX:HeapDumpPath=/tmp/hadoop2/hive/hiveserver2.hprof -Xloggc:/tmp/hadoop2/hive/hiveserver2.gc.log -XX:OnOutOfMemoryError=/usr/local/hive/bin/stop-hiveserver2.sh "
elif [ "$SERVICE" = "metastore" ]; then
   export HADOOP_OPTS=" $HADOOP_OPTS -XX:HeapDumpPath=/tmp/hadoop2/hive/metastore.hprof -Xloggc:/tmp/hadoop2/hive/metastore.gc.log -XX:OnOutOfMemoryError=/usr/local/hive/bin/stop-metastore.sh "
fi
######## END ##########
```
同时${HIVE_HOME}/conf/hive-env.sh文件中的HADOOP_OPTS变量，修改为如下内容
```
export HADOOP_OPTS="-Xmx512m -Xss512k $GC_OPTIONS"
```   
通过测试，问题解决
![image](/uploads/7d60be112fcb4bc7da1b55a54483e741/image.png)   
   
---
   
*另外hiveserver2进程在挂掉后，并没有被crontab拉起，原因请参考[crontab读取环境变量的问题(metastore自动拉起失败)](http://10.1.2.222/dp-wiki/dpwiki/wikis/crontab-environment-variables)*