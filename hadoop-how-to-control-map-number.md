！！！***这些参数设置都是针对的text文件格式，其他数据格式，可能参数略有不同，或完全不同。需要自己参照具体格式确定。***    
<br>

##### 1）文件很大，造成执行任务的map数目太多   
这一般都是应为hdfs的blocksize设置的比较小造成的，比如目前集群是128MB，在处理一些大文件时，128MB显得有些小。   

```   
//设置每个map处理的数据量至少为512MB，~~需要注意改大小不能超过Integer.MAX，否则会触发hadoop的bug（集群的当前版本有该问题)~~
conf.setInt("mapreduce.input.fileinputformat.split.minsize",536870912);

设置该参数来加大每个map处理的数据量，从而控制任务的map数目，提高运行效率
```
##### 2） 文件都是小文件，造成执行的map任务数目太多
默认的情况下，这样的每个小文件都由一个map任务处理，如果大量的这样的小文件，map-reduce的处理效率就很低了,会有大量的任务运行，但每个任务只处理很少的数据

``` 
conf.setInt("mapreduce.input.fileinputformat.split.maxsize",256000000);
//设置InputFormat，针对text文件
job.setInputFormatClass(CombineTextInputFormat.class);

Comline的InputFormat会尽可能多的将小文件集中到一个map中处理，每个map中处理的数据量以 mapreduce.input.fileinputformat.split.maxsize     
为上限，   默认是没有设置，所以会把所有的文件集中到一个map中处理（这样也不合理）

还有其他参数控制每个node和rack的split的size，感兴趣的可以查阅源码
```

##### 3）如果既有大文件，又有很多小文件呢？

```
处理方式同第二种情况相同，可以根据需要调整mapreduce.input.fileinputformat.split.maxsize的大小，控制每个map处理的最大数据量
```

大家可以补充其他的方案，或者hive如何做类似的控制
<br>
<br>
<br>
<br>
<br>