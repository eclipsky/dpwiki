### 背景和方案   
目前namenode使用了ha的部署模式，但系统会经常出现ha的自动切换（namenode节点其实正常）。经过调研发现可能的原因如下：
 <br />   
1）HealthMonitor check本地namenode的rpc端口时超时，导致HealthMonitor认为namenode挂掉。  
2）zk上的session timeout，导致丢掉当前持有的active锁（temp节点），引起自动切换。  
<br />
 
下面的优化将针对1）和2）调整相应的超时参数，看是否起效。修改core-site.xml   
```xml
    <!-- HealthMonitor check namenode 的超时设置，默认50000ms，改为5mins -->
    <property>
        <name>ha.health-monitor.rpc-timeout.ms</name>
        <value>300000</value>
    </property>
   <!-- zk failover的session 超时设置，默认5000ms，改为3mins -->
    <property>
        <name>ha.zookeeper.session-timeout.ms</name>
        <value>180000</value>
    </property>
```
### 操作影响
无，不需要重启hadoop，不会引起namenode切换   
### 操作步骤   
下面操作假设master1（active），master2（standby），**只有按照该顺序操作，才能实现namenode不切换**   
<br/>
1）登陆master2（standby），**注释掉crontab中的restart-zkfc.sh任务**， 执行 sbin/hadoop-daemon.sh stop zkfc   
2）登陆master1（active），**注释掉crotab中的restart-zkfc.sh任务**，执行sbin/hadoop-daemon.sh stop zkfc   
3）备份master1和master2的core-site.xml文件，将上述配置项加到master1和master2的core-site.xml配置文件中   
4）登陆master1（active），执行sbin/hadoop-daemon.sh start zkfc   
5）登陆master2（standby）,执行sbin/hadoop-daemon.sh start zkfc   
6）check hadoop集群状态     
7）重要!!! ***登录master1和master2，恢复crontab中的restart-zkfc.sh任务***   
8）完成
### 注意事项
1）需要先注释掉机器上的crontab任务restart-zkfc.sh，否则该程序会自动拉起zkfc进程。导致namenode active产生切换   
2）任务完成后要恢复crontab中注释掉的restart-zkfc.sh任务   
<br/>
<br/>