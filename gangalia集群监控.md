``本次是虚拟机下测试，区别本地测试环境，但操作步聚是一致``   
一、ganglia 工作原理   
![image](/uploads/0c432c797432cbcb7fb485be64e1db37/image.png)
Ganglia 包括如下几个程序，他们之间通过XDL(xml的压缩格式)或者XML格式传递监控数据，达到监控效果。集群内的节点，通过运行gmond收集发布节点状 态信息，然后gmetad周期性的轮询gmond收集到的信息，然后存入rrd数据库，通过web服务器可以对其进行查询展示。   

Gmetad 这个程序负责周期性的到各个datasource收集各个cluster的数据，并更新到rrd数据库中。 可以把它理解为服务端。   

Gmond 收集本机的监控数据，发送到其他机器上，收集其他机器的监控数据，gmond之间通过udp通信，传递文件格式为xdl。收集的数据供Gmetad读取，默认监听端口8649 ，监听到gmetad请求后发送xml格式的文件。可以把它理解为客户端。   

web front-end 一个基于web的监控界面，通常和Gmetad安装在同一个节点上(还需确认是否可以不在一个节点上，因为php的配置文件中ms可配置gmetad的地址及端口)，它从Gmetad取数据，并且读取rrd数据库，生成图片，显示出来。   

 如上图所示，gmetad周期性的去gmond节点或者gmetad节点poll数据。一个gmetad可以设置多个datasource，每个datasource可以有多个备份，一个失败还可以去其他host取数据。   

 如 果是muticast模式的话，gmond之间还会通过多播来相互传递数据。Gmond本身具有udp send和recv通道，还有一个tcp recv通道。其中udp通道用于向其他gmond节点发送或接受数据，tcp则用来export xml文件，主要接受来自gmetad的请求。Gmetad只有tcp通道，一方面他向datasource发送请求，另一方面会使用一个tcp端口，发 布自身收集的xml文件，默认使用8651端口。所以gmetad即可以从gmond也可以从其他的gmetad得到xml数据。   


二、规划   
1、使用一台ganglia机器监控两个集群   
ganglia-server（gmetad）：10.64.8.10   centos6.5   
ganglia-agent（gmond）：xdhadoop、sjselk   

集群xdhadoop：   
master1、master2、slave1、slave2、slave3   
 集群sjselk：   
es-master1、es-master2、es-master3、es-master4、es-master5   
三：准备工作   
（1）修改主机名，并将监控机名写入hosts   
```
#hostname ganglia && echo ganglia >/etc/hostname
#cat >> /etc/hosts << EOF
10.10.1.1 master1
10.10.1.2 master2
10.10.1.3 slave1
10.10.1.4 slave2
10.10.1.5 slave3
10.10.0.1 es-master1
10.10.0.2 es-master2
10.10.0.3 es-master3
10.10.0.4 es-master4
10.10.0.5 es-master5
10.10.1.10 ganglia
EOF
```
（2）关闭selinux和防火墙   
`#setenforce 0&& service iptables stop &&chkconfig iptables off`   
四：ganglia-server环境安装   
           ganglia   
（1）web环境   
安装apache   
```
#yum install httpd
#vim /etc/httpd/conf/httpd.conf 
User nobody
Group nobody
```
安装php，并编辑测试页面。   
```
#yum install php  
#vim /var/www/html/info.php 
<?php
phpinfo();
?>
```
启动httpd   
```

#service httpd restart  


```
打开浏览器访问10.64.8.10/info.php测试   

（2）安装依赖   
```

 yum install -y apr-devel apr-util check-devel cairo-devel pango-devel libxml2-devel rpm-build glib2-devel dbus-devel freetype-devel fontconfig-devel gcc-c++ expat-devel python-devel libXrender-devel   



```
（3）安装libconfuse   
```

#wget http://download.savannah.gnu.org/releases/confuse/confuse-2.7.tar.gz
#tar confuse-2.7.tar.gz
#cd confuse-2.7
# ./configure CFLAGS=-fPIC --disable-nls
# make&&make install   


```
（4）安装pcre   

```
#tar xf pcre-8.12.tar.bz2
#cd pcre-8.12
#./configure && make && make install
#echo "/usr/local/lib" >> /etc/ld.so.conf
# ldconfig   

```
（5）安装rrdTool   
1：下载tar包，编译安装rrdTool   
```

#cd /opt
#wget  http://oss.oetiker.ch/rrdtool/pub/rrdtool-1.3.1.tar.gz
#tar xf rrdtool-1.3.1.tar.gz
#cd rrdtool-1.3.1
#./configure --prefix=/usr/local
#make && make install   


```
rrd安装后：lib库文件 /usr/local/lib  bin可执行文件 /usr/local/bin/rrdtool   
2：将rrdtool 所以拷到/usr/bin下（方便后面ganglia调用），并加入库文件。
```

#cp /usr/local/bin/rrdtool  /usr/bin/rrdtool
#echo "/usr/local/lib" >> /etc/ld.so.conf
# ldconfig   



```

3：验证rrdtool是否安装成功，利用examples下的示例，渲染一个示例图
```

#rrdtool -V
#cd /usr/local/share/rrdtool/examples/
#./stripes.pl
#cp stripes.png /var/www/html/


```
在浏览器访问http://ganglia/stripes.png，，如下图说明rrd安装正常。
![image](/uploads/8b21a72c2e7b7fcd73fb466b2f08efa2/image.png)
三：ganglia-server（gmetad）安装    
          ganglia   
（1）编译安装ganglia   
```

#wget http://120.52.73.45/jaist.dl.sourceforge.net/project/ganglia/ganglia%20monitoring%20core/3.7.2/ganglia-3.7.2.tar.gz
#tar xf  ganglia-3.7.2.tar.gz
#cd  ganglia-3.7.2
# ./configure --prefix=/usr/local/ganglia --with-gmetad --with-librrd=/usr/local/lib --sysconfdir=/etc/ganglia --with-libpcre=no
#make && make install   



```
（2）cpoy启动脚本   
```

# cp gmond/gmond.init /etc/rc.d/init.d/gmond
# cp gmetad/gmetad.init /etc/rc.d/init.d/gmetad
# chkconfig --add gmond && chkconfig gmond on
# chkconfig --add gmetad && chkconfig gmetad on
#vim /etc/init.d/gmetad
改
GMETAD=/usr/sbin/gmetad
为
GMETAD=/usr/local/ganglia/sbin/gmetad
#vim /etc/init.d/gmond
改
GMETAD=/usr/sbin/gmond
为
GMETAD=/usr/local/ganglia/sbin/gmond   


```
 复制python_modules   
```

#mkdir /usr/local/ganglia/lib64/ganglia/python_modules
#cp ./gmond/python_modules/*/*.py  /usr/local/ganglia/lib64/ganglia/python_modules   


```
（3）安装ganglia前端   
```

#wget http://120.52.73.47/jaist.dl.sourceforge.net/project/ganglia/ganglia-web/3.7.1/ganglia-web-3.7.1.tar.gz
#tar xf ganglia-web-3.7.1.tar.gz
#cd ganglia-web-3.7.1
#vim Makefile
#改成实际的目录
##########################################################
# User configurables:
##########################################################
# Location where gweb should be installed to (excluding conf, dwoo dirs).
#ganglia的web发布目录
GDESTDIR = /var/www/html/ganglia    
 
# Location where default apache configuration should be installed to.
#ganglia-web的配置文件目录
GCONFDIR = /etc/ganglia-web         
 
# Gweb statedir (where conf dir and Dwoo templates dir are stored)
GWEB_STATEDIR = /var/lib/ganglia-web
 
# Gmetad rootdir (parent location of rrd folder)
GMETAD_ROOTDIR = /usr/local/ganglia
#httpd的用户
APACHE_USER = nobody
 
#make install   


```
（4）启动server端gmetad   
```

# service gmetad start                   
Starting GANGLIA gmetad:                                   [  OK  ]   


```
浏览器访问：http://ganglia/ganglia 如下图，gmetad正常，但是还没有加入机器，所以是空白的。
![image](/uploads/3fc1ddaa637a60d8bafe49ede0dec3d0/image.png)
五：ganglia-agent（gmond）安装   
xdhadoop所有机器、sjselk所有机器   
（1）安装依赖   
```

# yum install -y apr-devel apr-util check-devel cairo-devel pango-devel libxml2-devel rpm-build glib2-devel dbus-devel freetype-devel fontconfig-devel gcc-c++ expat-devel python-devel libXrender-devel

```
（2）安装libconfuse   
```

#wget http://download.savannah.gnu.org/releases/confuse/confuse-2.7.tar.gz
#tar confuse-2.7.tar.gz
#cd confuse-2.7
# ./configure CFLAGS=-fPIC --disable-nls
# make&&make install  


```
（3）安装pcre   
```

#tar xf pcre-8.12.tar.bz2
#cd pcre-8.12
#./configure && make && make install
#echo "/usr/local/lib" >> /etc/ld.so.conf
# ldconfig   


```
（4）安装   
```

#tar xf  ganglia-3.7.2.tar.gz
#cd  ganglia-3.7.2
# ./configure --prefix=/usr/local/ganglia --sysconfdir=/etc/ganglia
#make && make install  


```
启动脚本   
```

#cp gmond/gmond.init /etc/rc.d/init.d/gmond
#chkconfig --add gmond && chkconfig gmond on
#vim /etc/init.d/gmond
改
GMETAD=/usr/sbin/gmond
为
GMETAD=/usr/local/ganglia/sbin/gmond


``` 
复制python_modules   
```

#mkdir /usr/local/ganglia/lib64/ganglia/python_modules
#cp ./gmond/python_modules/*/*.py  /usr/local/ganglia/lib64/ganglia/python_modules


```
安装完毕，其实agent安装和server安装几乎一样，只是server端我们用的是gmetad（也会安装gmond），agent端，我们只用gmond就行了。   
``注意``
``如果编译安装出现``
```

checking for cfg_parse in -lconfuse... no
Trying harder including gettext
checking for cfg_parse in -lconfuse... no
Trying harder including iconv
checking for cfg_parse in -lconfuse... no
libconfuse not found   


```
``解决办法   ``
`yum -y install libconfuse libconfuse-devel.x86_64`   
六：配置gmetad和gmond   
（1）配置gmond.conf（sjselk集群，组播）   
**sjselk**   
```

#vim  /etc/ganglia/gmond.conf
cluster {
  name = "sjselk"               #集群名
  owner = "nobody"              #运行gmond用户名
  latlong = "unspecified"
  url = "unspecified"
}
 
host {
  location = "unspecified"
}
 
udp_send_channel {
  mcast_join = 239.2.11.71      #默认组播地址
  port = 8649                   #gmond端口
  ttl = 1
}
 
udp_recv_channel {
  mcast_join = 239.2.11.71
  port = 8649
  bind = 239.2.11.71
  retry_bind = true
}
 
tcp_accept_channel {
  port = 8649
  gzip_output = no
}


```
添加路由到组播地址   
`ip route add 239.2.11.71 dev eth0`   
（2）配置gmond.conf（xdhadoop集群，单播）   

```
#vim  /etc/ganglia/gmond.conf
cluster {
  name = "xdhadoop"      #集群名
  owner = "nobody"       #运行gmond的用户
  latlong = "unspecified"
  url = "unspecified"
}
 
host {
  location = "unspecified"
}
 
udp_send_channel {
#  mcast_join = 239.2.11.71   #使用单播，注释组播地址      
  host = 10.10.1.10           #使用单播，写gmond的IP   
  port = 8653                 #设置端口
  ttl = 1
}
 
udp_recv_channel {
#  mcast_join = 239.2.11.71    #使用单播，注释组播地址 
  port = 8653                  #设置端口
#  bind = 239.2.11.71          #使用单播，注释组播地址 
bind = 10.10.0.1               #写自己的IP
  retry_bind = true
}
 
tcp_accept_channel {
  port = 8653
  gzip_output = no
}   


```
（3）配置gmetad.conf   
**ganglia**   
```

#vim /etc/gmetad.conf
 #组播只写一个ip即可，可以写两个，防止一台机器挂了后，收不到数据。单播需要写上所有机器。
data_source "sjselk" es-master1:8649  es-master2:8649 
data_source "xdhadoop" master1:8653 master2:8653 slave1:8653  slave2:8653 slave3:8653   


```
七：启动gmetad和gmond   
**ganglia**   
```

[root@ganglia ~]# service gmetad start
Starting GANGLIA gmetad:                


```
**sjselk、xdhadoop**   
```

[root@ganglia ~]# service gmond start
Starting GANGLIA gmond:                                     [  OK  ]    


```
