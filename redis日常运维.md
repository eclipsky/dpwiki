redis日常运维   
----------------------------------------
### 1、组件部署情况      

| 机器        | IP   |  组件  |
| --------   | :-----:  | :----:  |
| dcredis207|192.168.1.107 |   redis-server|
| dcredis208|192.168.1.108 |   redis-server|
| dcredis209|192.168.1.109 |   redis-server|

### 2、数据文件目录   
/data0/redis-cluster (每个以7000+命名的文件夹对应的是各自实例的端口号)   

### 3、动态修改参数   
redis启动后，如果要修改参数，是不需要重新启动的，redis支持动态修改参数，如：   
```
redis-cli -p 7000 config get cluster-node-timeout //获取参数值
redis-cli -p 7000 config set save "900 1"    //rdb 文件保存频率
redis-cli -p 7000 config set save ""   //rdb文件关闭自动保存
redis-cli -p 7000 bgsave  // 手动保存rdb文件
redis-cli -p 7000 config set cluster-node-timeout 30000
redis-cli -p 7000 config set cluster-require-full-coverage no
```
### 4、常用运维命令   
```
redis-cli -c -p 7000 cluster info  查看集群状态
redis-cli -c -p 7000 cluster nodes 查看节点的主备关系
redis-cli -c -p 7000 --stat  查看tps数量
redis-trib.rb check redis201:7000  查看个节点是否可用
```
### 5、redis集群知识   
参考作业部落的文章：https://www.zybuluo.com/phper/note/195558 