JStorm日常运维   
----------------------------------------
### 1、组件部署情况      

| 机器        | IP   |  组件  |
| --------   | :-----:  | :----:  |
| dckafka1   |192.168.1.150 |   NimbusServer、Supervisor、Bootstrap  |
| dckafka2   |192.168.1.151 |   Supervisor  |
| dckafka3   |192.168.1.152 |   Supervisor  |
| dchbase1  |192.168.1.173 |   Supervisor  |
| dchbase2  |192.168.1.172 |   Supervisor  |
| dchbase3  |192.168.1.181 |   Supervisor  |

### 2、客户端操作的机器和目录   
(1)打开跳板机，进入dctest1机器
```
   ssh jstorm2@dctest1 
   密码：jstorm2@2016
```
(2)进入目录：/home/jstorm2/jstorm-2.1.0/dc_topology   
该目录是实时jar包汇总目录，线上所有实时业务的jar包都是上传到这里才提交到集群中   
### 3、JStorm界面管理及日志查看   
(1)界面UI：http://jstorm2.dataeye.com/cluster?name=jstorm2_cluster   
> 用户名@密码  : dataeye@digitcube   

(2)查看topology运行状态的步骤：

-  点击对应的topology
-  在页面上查看 SendTps 和 RecvTps 两个字段的值   
``` 一般经验是：   
　　高峰时段，dcRTExtTopology的TPS为6K~8K为合理，dcRTTopology为1W以上为合理   
　　低谷时段，dcRTExtTopology的TPS为1K~2K为合理，dcRTTopology为6K以上为合理   
```
-  打开 Task Stats 标签页，在Host中可以看到对应的日志所在的机器，如 192.168.1.150   
-  找日志的步骤：
   在跳板机上执行  ssh hadoop@192.168.1.150  dchd@2013   
   进入目录：/data0/jstorm2/logs 找到对应topology的文件夹
   查看：xxxx-worker-7809.log 文件即可   

### 4、如果对应的 topology 消费能力跟不上，需要调大并行度的步骤   
以 /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar 为例   
(1)进入/home/jstorm2/jstorm-2.1.0/dc_topology/ga1文件夹，解压jar文件：jar -xf dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar   
(2)找到stormkafka.properties文件，修改配置参数，如下所示，适当调整参数值:   
   vim stormkafka.properties   
   例：   
```
	rt.redis.topic=dcRTTopic
	rt.redis.group=dcRTRedisGroup	
	rt.redis.spout.num=3  // 读取kafka消息的线程数量，3个已经足够
	rt.redis.bolt.num=36 // 业务处理的线程数量，适当加大，可为spout的整数倍
	rt.redis.worker.num=12 // 工作进程数，包含spout和bolt的数量，一般为机器数量的整数倍，我们集群有6台机器，所以为6、12、18、24
	rt.redis.thread.num=32  //bolt内部插入数据的线程数，适当加大可加快执行速度
```
(3)删除 dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar 文件   
(4)打包： jar -cf dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar ./*   
(5)打包完成后，会再次生成dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar文件，按照不同业务，使用以下对应的命令关闭重启即可   
### 5、各个实时任务的操作命令   
以下为各个实时任务的操作命令，说明如下：   
第一行：注释是哪个topology   
第二行：deactivate(冻结)该topology   
第三行：active(激活)该topology   
第四行：关闭该topology   
第五行：启动该topology   
第六行：查看该topology的消费延时了多少offset，直接拷贝到 dckafka1 机器上运行即可(ssh hadoop@dckafka1 密码： dchd@2013)   
```
# dcRTExtTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcRTExtTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcRTExtTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcRTExtTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.ext.RTExtTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcRTExtTopic dcRTExtGroup 6 dckafka1,dckafka2,dckafka3


# dcRTTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcRTTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcRTTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcRTTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.online.RTTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcRTTopic dcRTGroup 6 dckafka1,dckafka2,dckafka3


# jsyLevelTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate jsyLevelTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate jsyLevelTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill jsyLevelTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.jsy.JSYTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar JinShanYunLevel jsyLevelGroup 6 dckafka1,dckafka2,dckafka3


# dcDebugLogStorm(不再使用)
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcDebugLogStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcDebugLogStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcDebugLogStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.ActiveLogTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar debugLogTopic debugLogGroup 6 dckafka1,dckafka2,dckafka3


# dcDebugLogStormNew(切表专用)
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcDebugLogStormNew
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcDebugLogStormNew
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcDebugLogStormNew
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.ActiveLogTopologyNew
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar debugLogTopic debugLogGroupNew 6 dckafka1,dckafka2,dckafka3


# dcClickStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcClickStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcClickStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcClickStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.ClickTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcClickTopic dcClickGroup 6 dckafka1,dckafka2,dckafka3


# dcItemPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcItemPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcItemPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcItemPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.ItemPluginTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcItemPluginTopic dcItemPluginGroup 6 dckafka1,dckafka2,dckafka3


# dcCoinPluginStormES
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcCoinPluginStormES
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcCoinPluginStormES
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcCoinPluginStormES
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1es/dc-storm2-ga1-es-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.jstorm.topology.CoinPluginTopologyES
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcCoinPluginTopic dcCoinPluginStormES 6 dckafka1,dckafka2,dckafka3


# dcCoinPluginStorm(不再使用)
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcCoinPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcCoinPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcCoinPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.CoinPluginTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcCoinPluginTopic dcCoinPluginGroup 6 dckafka1,dckafka2,dckafka3



# dcCoinPluginStormNew(切表专用)
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcCoinPluginStormNew
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcCoinPluginStormNew
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcCoinPluginStormNew
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.CoinPluginTopologyNew
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcCoinPluginTopic dcCoinPluginGroupNew 6 dckafka1,dckafka2,dckafka3



# dcPaymentPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcPaymentPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate dcPaymentPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcPaymentPluginStorm
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.PaymentPluginTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcPaymentPluginTopic dcPaymentPluginGroup 6 dckafka1,dckafka2,dckafka3


# adTrackerEventStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate adTrackerEventStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate adTrackerEventStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill adTrackerEventStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/tracker/dc-storm2-tracker-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.tracker.storm.rt.AdTrackerEventTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar adTrackerEventMsgTopic adTrackerEventMsgGroup 6 dckafka1,dckafka2,dckafka3


# H5RtLogStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate H5RtLogStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate H5RtLogStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill H5RtLogStrom
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/h5/dc-storm-h5-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.h5.topology.rtlog.RtLogTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcH5RTLogTopic dcH5RTLogGroup 6 dckafka1,dckafka2,dckafka3


# H5OnlineAsyncTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate H5OnlineAsyncTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate H5OnlineAsyncTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill H5OnlineAsyncTopology
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/h5/dc-storm-h5-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.h5.topology.online.OnlineTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcH5RTTopic dcH5RTGroup 6 dckafka1,dckafka2,dckafka3



#appActAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate appActAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate appActAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill appActAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/app_bak/dc-storm-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.app.topology.act.AppActTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcAppStoreActTopic dcAppStoreActGroup20 6 dckafka1,dckafka2,dckafka3


#appOnlineAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate appOnlineAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate appOnlineAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill appOnlineAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/app_bak/dc-storm-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.app.topology.online.AppOnlineTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcAppStoreOnlineTopic dcAppStoreOnlineGroup20 6 dckafka1,dckafka2,dckafka3


#appRLClickAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate appRLClickAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate appRLClickAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill appRLClickAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/app_bak/dc-storm-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.app.topology.rlclick.AppRLClickTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcAppStoreClickTopic dcAppStoreClickGroup20 6 dckafka1,dckafka2,dckafka3


#appRLShowAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate appRLShowAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate appRLShowAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill appRLShowAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/app_bak/dc-storm-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.app.topology.rlshow.AppRLShowTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcAppStoreShowTopic dcAppStoreShowGroup20 6 dckafka1,dckafka2,dckafka3


#appResDownloadAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate appResDownloadAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate appResDownloadAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill appResDownloadAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/app_bak/dc-storm-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.app.topology.resdownload.AppResDownloadTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcAppStoreDownloadTopic dcAppStoreDownloadGroup20 6 dckafka1,dckafka2,dckafka3


#appDebugLogAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate appDebugLogAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate appDebugLogAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill appDebugLogAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/app_bak/dc-storm-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.app.topology.debuglog.AppDebugLogTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcAppStoreDebugLogTopic dcAppStoreDebugLogGroup20 6 dckafka1,dckafka2,dckafka3


#appVideoPlayAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate appVideoPlayAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm activate appVideoPlayAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill appVideoPlayAsyncTopology20
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/app_bak/dc-storm-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.app.topology.videoplay.AppVideoPlayTopology
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcAppVideoPlayTopic dcAppVideoPlayGroup20 6 dckafka1,dckafka2,dckafka3

```
