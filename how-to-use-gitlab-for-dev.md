### 主要涉及的内容包括如何利用gitlab进行协同开发，包括:    
1）多分支开发   
2）pull request提交代码合并请求   
3）code review   
4）代码合并到主干    

### gitlab/github大体的开发流程是:    
1）在自己本地fork上进行代码的开发     
2）提交代码到自己远程的repo（仓库）   
3）向目标项目提交pull request请求代码合并   
4）对请求的pull request进行code review    
5）修改代码，重复2-5    
6）code review完成，整合（rebase）之前所有的提交    
7）merge pull request的代码到主干代码    
8）完成    

### 一个例子
1）fork 目标项目    
<p>
![image](/uploads/3cc252f191f64eb8f0545ccc4fbee7b6/image.png)
 <p>     
2）clone刚才check的项目到本地
<p>      
![image](/uploads/a2dd2b28fabd8f83f8e5561a0a71b3d8/image.png)
可以看到这是刚才fork的项目，check该fork项目到本地目录，在本地命令行窗口执行以下命令:
```
git clone git@devhadoop222:root/de-azkaban.git
```

3）本地进行开发，并提交push到gitlab    
**本地的代码可能需要和上游的源项目保持一致（比如合并别人提交的代码），详情查看这里**https://help.github.com/articles/syncing-a-fork/

git commit 进行提交    
git push   进行push   

4）创建pull request，提交代码merge请求。首先登陆gitlab上fork的项目主页
![image](/uploads/220176cbed9992b0e2648f03398fa007/image.png)
 接着
![image](/uploads/6e6b2c2d1ac1aec40c74c06d7912fa4c/image.png)    
<br>
![image](/uploads/4dd4a379694b5cb3da3c1b9f57b57f47/image.png)



<br><br><br><br><br><br>