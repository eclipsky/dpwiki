### 现象

线上crontab中hive metastore 服务自动拉起失败，但shell脚本手动启动可以

### 分析过程

```bash
* * * * * /home/hadoop2/restart-metastore.sh >>/dev/null 2>&1
```

**坑1）:** crontab任务启动过程中的标准输出、标准出错都被丢掉了！！！   

改为

```bash
* * * * * /home/hadoop2/restart-metastore.sh >>/tmp/restart-metastore.log 2>&1
```

发现/tmp/restart-metastore.log中有如下出错信息:

```bash
Cannot find hadoop installation: $HADOOP_HOME or $HADOOP_PREFIX must be set or hadoop must be in the path
```
很明显是环境变量的问题，但是环境变量在.bashrc中都已经设置，且手动执行脚本服务是可以启动的。经过调查，有如下结论。

### 结论

crontab默认不会读取user的环境变量信息（比如.bashrc和.bash_profile），这在一些情况下回出现问题，比如在crontab中去拉起hive的metastore服务。

### 解决方案

在启动脚本中判断如果HADOOP_HOME变量没有加载load用户的环境变量配置(例如.bashrc)
