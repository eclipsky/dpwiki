### 一、问题现象
1、启动hregionserver后，日志一直报错，如下：
```
2016-07-08 17:25:05,631 WARN org.apache.hadoop.hdfs.DFSClient: DFS Read: java.io.IOException: Could not obtain block: blk_3725927050443192376_382043281 file=/hbase/dc_rt_click_detail/dfba4ddd0514e9f761550b93bc7f89b8/info/f6107061c4b44f3f8fea2f4becffa61d
	at org.apache.hadoop.hdfs.DFSClient$DFSInputStream.chooseDataNode(DFSClient.java:2269)
	at org.apache.hadoop.hdfs.DFSClient$DFSInputStream.blockSeekTo(DFSClient.java:2063)
	at org.apache.hadoop.hdfs.DFSClient$DFSInputStream.read(DFSClient.java:2224)
	at java.io.DataInputStream.readFully(DataInputStream.java:195)
	at org.apache.hadoop.hbase.io.hfile.FixedFileTrailer.readFromStream(FixedFileTrailer.java:312)
	at org.apache.hadoop.hbase.io.hfile.HFile.pickReaderVersion(HFile.java:551)
	at org.apache.hadoop.hbase.io.hfile.HFile.createReaderWithEncoding(HFile.java:615)
	at org.apache.hadoop.hbase.regionserver.StoreFile$Reader.<init>(StoreFile.java:1368)
	at org.apache.hadoop.hbase.regionserver.StoreFile.open(StoreFile.java:557)
	at org.apache.hadoop.hbase.regionserver.StoreFile.createReader(StoreFile.java:666)
	at org.apache.hadoop.hbase.regionserver.Store$1.call(Store.java:458)
	at org.apache.hadoop.hbase.regionserver.Store$1.call(Store.java:453)
	at java.util.concurrent.FutureTask$Sync.innerRun(FutureTask.java:334)
	at java.util.concurrent.FutureTask.run(FutureTask.java:166)
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:471)
	at java.util.concurrent.FutureTask$Sync.innerRun(FutureTask.java:334)
	at java.util.concurrent.FutureTask.run(FutureTask.java:166)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
	at java.lang.Thread.run(Thread.java:724)
```
2、打开hbase的web界面，找到日志里描述的表 dc_rt_click_detail ，发现该表没有region信息，界面上只有两个Action操作：  
![image](/uploads/49b6e65ec92e25bd2ea8d8c07d64cee2/image.png)


3、实时与这个表对应的任务的tps几乎为零，无法插入数据

### 二、导致问题的原因
1、查看该问题出现的时候的日志，hbase刚好在做split操作。而这个时候hadoop的namenode出现宕机，导致hmaster不可用，hregionserver全部出现异常，实时全线崩溃。   
2、重启hregionserver后，问题依然存在，后来重启了hmaster后，部分实时任务恢复正常执行了，但是dcDebugLog任务的tps一直提不上去。   
3、查看hregionserver日志，后台在不断报出以上错误日志，推断是该表有block损坏了（hadoop fsck发现hbase的文件确实有损坏）。   

### 三、问题解决
1、尝试使用官网提供的repair命令进行修复，如下命令：   
       ./hbase hbck -repair dc_debug_log   
      但是最后都报错异常：   
      failed to move out of transition within timeout 120000ms   
2、尝试了各种与reapir相关的命令，如fixMeta等，都报了同样的错误   
3、最后使用了以下命令：    
       ./hbase hbck -sidelineCorruptHFiles dc_debug_log   
       没有报错，而已查看hbase web界面，region信息又回来了^v^    
4、最后执行repair命令：   
       ./hbase hbck -repair dc_debug_log   
      也没有报错(这个命令运行时间比较长，一定要等到执行结束)          
5、最后查看实时任务，tps马上飙到3K多，恢复正常了               

<br>
<br>