### 平台升级和优化
[namenode ha切换优化](namenode-ha-session-timeout)   
[resource manager因为CapacityScheduler的NPE异常退出，引起failover切换](resourcemanger-exit-by-npe-of-capacityscheduler)   
[nodemanager jvm参数优化(Xss)](nodemanage-jvm-tuning)   
[could't create native thread的问题跟踪](could-not-create-native-thread)   
[application master 持续org.apache.hadoop.ipc.Client: Retrying connect to server](application-master-retrying-connect-to-server)     
[修复hbase corrupt的文件](hbase-could-not-obtain-block)  
[hadoop迁移journal node](hadoop-add-journal-node)   
[standby resourcemanager的zk session超时问题](standby-zk-session-timeout)

[tasktracker重启长时间不能被分配任务：MapTask/ReduceTask](restart tasktracker 后，需要很长时间，该tasktracker才能重新接收maptask 或者 reducetask)

[kylin slow query sql 的监控改造方案](kylin-slow-sql-监控和运维)    
[如何控制map-reduce任务的map数目](hadoop-how-to-control-map-number)    
[spark2.0的部署](spark2-deploy)    
[如何本地运行和debug mapreduce任务](how-to-make-mapreduce-local-running)    
[hive源码编译与hive-on-spark环境搭建](hive-on-spark编译)  
[Mapreduce框架的shuffle reduce端流程梳理](Mapreduce框架的shuffle reduce端流程梳理)  
[Yarn & mapreduce主要配置参数概述](Yarn & mapreduce主要配置参数概述)  
[Datanode节点磁盘容错测试与配置](Datanode节点磁盘容错调研与配置)

### 平台技术
[平台组件核心metric的监控](hadoop-component-monitor-metrics)    
[ganglia的metric显示的问题](ganglia-metric-draw-problem)    
[使用gitlab进行协同开发](how-to-use-gitlab-for-dev)   
[使用supervisor来monitor hadoop集群](supervisor-hadoop)    
[centos6添加开机服务(supervisor)](centos6-add-startup-service)    
[crontab读取环境变量的问题(metastore自动拉起失败)](crontab-environment-variables)    
[maven精粹](how-to-use-maven)   
[部署deepwiki系统](deploy-deeepwiki)   
[hive的mysql备份和恢复](mysql5.1.73全量恢复)   
[ganglia集群监控](gangalia集群监控)

### hive
[hive权限与通用优化配置文件说明](hive权限与通用优化配置文件说明)   
[现网环境metastore schema升级2.2操作步骤概述](现网环境升级hive2.0大概步骤)   
[Hiveserver2和metastore进程参数错误修正](Hiveserver2和metastore进程参数错误修正)

### 其他问题
[ssh无密码登录仍然需要输密码问题的排查](配置ssh公钥登录显示还是输入)   
[ambari的编译](ambari-build)   
[后台或者crontab中运行fabric任务](launch-fabric-tasks-without-stdin-or-nohup)    
[每日工作回顾](work-notes)    
[gitlab-服务端的安装](gitlab-服务端的安装)   
[Hbase0.94出现responsetooslow的解决方式](Hbase0.94出现responsetooslow的解决方式)   

### 日常运维
[hadoop2.0日常运维](hadoop2.0-devops)   
[kafka日常运维](kafka日常运维)   
[redis日常运维](redis日常运维)   
[JStorm日常运维](JStorm日常运维)   
[Hbase日常运维](Hbase日常运维)   