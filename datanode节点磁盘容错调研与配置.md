### Datanode节点磁盘容错测试与配置   
考虑到集群节点磁盘出问题的情况比较多，目前现网每个datanode节点都配置了十块硬盘，在集群实际运行过程中已经证明了这一点，只要有磁盘错误导致整个datanode就会挂掉，所以需要配置datanode对磁盘错误容忍数目。   
#### 在测试环境测试可行性   
###### 环境准备  
由于测试环境datanode节点只有一块硬盘，这里采用折中的方式，方法是：在配置文件中为其指定一块不存在的硬盘，这样当datanode进程启动的时候自然会检测到该硬盘不存在或者认为读写该硬盘发生错误，进而可以验证配置的容错是否生效。   
在测试环境testhadoop236节点上执行命令`df -lh`可以看到该节点只有一块磁盘   
```
[hadoop2@testhadoop236 ~]$ df -lh
Filesystem      Size  Used Avail Use% Mounted on
/dev/vda2        48G  7.7G   40G  17% /
tmpfs           7.8G     0  7.8G   0% /dev/shm
/dev/vda1       190M   41M  140M  23% /boot
/dev/vda5       118G   89G   23G  80% /data0
```
配置$HADOOP_HOME/etc/hadoop/hdfs-site.xml如下   
```
<property>
     <name>dfs.datanode.data.dir</name>
     <value>file:///data0/hadoop2/hdfs/data,file:///data1/hadoop2/hdfs/data</value>
</property>
```
其中`file:///data0/hadoop2/hdfs/data`对应上面的/dev/vda5，而`file:///data1/hadoop2/hdfs/data`并不存在    
hdfs-site.xml增加如下配置
```
<property>
        <name>dfs.datanode.failed.volumes.tolerated</name>
        <value>0</value>
        <description>The number of volumes that are allowed to
        fail before a datanode stops offering service. By default
        any volume failure will cause a datanode to shutdown.
        </description>
</property>
```
注意上述配置的值为0，也既是该datanode节点可容忍的磁盘错误数目为零个    
###### 测试实验    
执行命令`$HADOOP_HOME/sbin/hadoop-daemon.sh start datanode`(如果已经启动，则需要先关闭)启动datanode节点   
等几秒后，再执行`jps`查看datanode并不存在，查看datanode日志/data0/hadoop2/log/hadoop-hadoop2-datanode-testhadoop236.log发现如下错误
```
2017-02-08 10:21:03,148 FATAL org.apache.hadoop.hdfs.server.datanode.DataNode: Initialization failed for Block pool <registering> (Datanode Uuid unassigned) service to devhadoop223/10.1.2.223:9000. Exiting. 
org.apache.hadoop.util.DiskChecker$DiskErrorException: Too many failed volumes - current valid volumes: 1, volumes configured: 2, volumes failed: 1, volume failures tolerated: 0
        at org.apache.hadoop.hdfs.server.datanode.fsdataset.impl.FsDatasetImpl.<init>(FsDatasetImpl.java:284)
        at org.apache.hadoop.hdfs.server.datanode.fsdataset.impl.FsDatasetFactory.newInstance(FsDatasetFactory.java:34)
        at org.apache.hadoop.hdfs.server.datanode.fsdataset.impl.FsDatasetFactory.newInstance(FsDatasetFactory.java:30)
        at org.apache.hadoop.hdfs.server.datanode.DataNode.initStorage(DataNode.java:1408)
        at org.apache.hadoop.hdfs.server.datanode.DataNode.initBlockPool(DataNode.java:1360)
        at org.apache.hadoop.hdfs.server.datanode.BPOfferService.verifyAndSetNamespaceInfo(BPOfferService.java:317)
        at org.apache.hadoop.hdfs.server.datanode.BPServiceActor.connectToNNAndHandshake(BPServiceActor.java:224)
        at org.apache.hadoop.hdfs.server.datanode.BPServiceActor.run(BPServiceActor.java:825)
        at java.lang.Thread.run(Thread.java:724)
2017-02-08 10:21:03,149 WARN org.apache.hadoop.hdfs.server.datanode.DataNode: Ending block pool service for: Block pool <registering> (Datanode Uuid unassigned) service to devhadoop222/10.1.2.222:9000
2017-02-08 10:21:03,149 WARN org.apache.hadoop.hdfs.server.datanode.DataNode: Ending block pool service for: Block pool <registering> (Datanode Uuid unassigned) service to devhadoop223/10.1.2.223:9000
2017-02-08 10:21:03,250 INFO org.apache.hadoop.hdfs.server.datanode.DataNode: Removed Block pool <registering> (Datanode Uuid unassigned)
2017-02-08 10:21:05,251 WARN org.apache.hadoop.hdfs.server.datanode.DataNode: Exiting Datanode
2017-02-08 10:21:05,253 INFO org.apache.hadoop.util.ExitUtil: Exiting with status 0
2017-02-08 10:21:05,254 INFO org.apache.hadoop.hdfs.server.datanode.DataNode: SHUTDOWN_MSG: 
/************************************************************
SHUTDOWN_MSG: Shutting down DataNode at devhadoop230/10.1.2.230
************************************************************/
```
可以看到错误信息非常清晰，datanode进程最终关闭，其中这段日志` Too many failed volumes - current valid volumes: 1, volumes configured: 2, volumes failed: 1, volume failures tolerated: 0`分别表示当前不可用的卷为1，配置的卷为2，错误卷为1，卷错误容忍为0。   
###### 进一步实验
为了启动datanode进程，进行如下配置，将数目由0改为1
```
<property>
        <name>dfs.datanode.failed.volumes.tolerated</name>
        <value>1</value>
        <description>The number of volumes that are allowed to
        fail before a datanode stops offering service. By default
        any volume failure will cause a datanode to shutdown.
        </description>
</property>
```
执行命令`$HADOOP_HOME/sbin/hadoop-daemon.sh start datanode`
```
[hadoop2@testhadoop236 logs]$ jps
3472 NodeManager
28452 Jps
[hadoop2@testhadoop236 logs]$ /usr/local/hadoop/sbin/hadoop-daemon.sh start datanode
starting datanode, logging to /data0/hadoop2/logs/hadoop-hadoop2-datanode-testhadoop236.out
[hadoop2@testhadoop236 logs]$ jps
3472 NodeManager
28600 Jps
28482 DataNode
```
可以看到datanode进程启动正常   

#### 同步到现网datanode节点   
将上述配置dfs.datanode.failed.volumes.tolerated，同步到现网所有datanode，这样可以保证一块磁盘出现错误后并不会导致该datanode节点挂掉。    

*具体细节不在陈述*




