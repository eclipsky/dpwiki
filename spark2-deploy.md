在yarn上部署spark相对来说比较简单，因为spark只是通过yarn获取资源，然后自己运行，所以spark的部署主要作为client端程序，不需要在所有节点上部署spark程序（当然如果出于某些效率因素考虑，也可以在所有节点有一份spark的安装包）。对于spark在yarn上运行部署和升级的过程类似，下面以线上spark1.6升级

###  升级原因

*  spark2.0在api上更统一（SparkSession为统一入口），DataFrame/Dataset统一化

*  spark2.0性能有较大提升，从RDD全面转向Dataset，由于DataSet是格式化存储，使得spark有了更多优化手段

### 升级过程

1.  打包spark2.0的安装包，主要要配置conf/spark-env.sh和conf/spark-default.conf这两个文件

2. 将打包好的安装包，推送到需要更新的客户机上，本次是warehouse的机器

3. 解压安装包，对spark重新建立软链，指向spark2.0的解压目录，即可

4. 这样warehouse的spark环境升级完毕，其他环境升级类似      

##### 1) java.lang.NoClassDefFoundError: com/sun/jersey/api/client/config/ClientConfig
spark上也有该问题的jira https://issues.apache.org/jira/browse/SPARK-15343，  应该是spark的jersey的版本和yarn的不一致只引起的。目前
采取禁用set hadoop.yarn.timeline-service.enabled=false 来绕过该问题，不过不同的场景参数名称有点区别

对于直接通过spark提交任务，可在spark-default.conf添加如下内容：
```
spark.hadoop.yarn.timeline-service.enabled    false
```

对于hive on spark，可在命令行或者hive-site.xml中设置如下内容：
```
yarn.timeline-service.enabled    false
```

##### 2)部署spark job history server   
http://www.cnblogs.com/luogankun/p/3981645.html

### 说明
可见在yarn上使用和升级spark还是比较简单，主要是spark任务只是作为yarn中的普通程序，所以spark也就只是一种客户端应用，所以升级比较简单。

### 可能的改进
目前当spark程序运行时，spark程序所需的jar会自动从本地copy到所以运行任务的节点上，这可能会消耗一定时间，鉴于目前的集群规模，这种时间花费还可接受。后续优化时，可将spark部署到集群所有节点