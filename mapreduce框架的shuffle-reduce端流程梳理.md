**Mapreduce框架的shuffle reduce端流程梳理**   

1. 从多个mapper端fetch数据（copy）  
fetch指从mapper端拉取mapper的输出数据。在fetch阶段reduce task会启动多个fetch线程从所有的mapper端取这个reducer的数据，同时还有一个fetchEvent线程负责获取mapper完成的event通知Fetch线程。  
注意：某个fetch线程读取完对应的mapper输出后是不是销毁了？当有新的mapper完成后，fetchevent线程会通知到fetch线程去取数据，这个过程是不是创建了新的fetch线程？  该问题有待查看源码确定
 
2. reduce不会等到所有mapper完成才执行fetch，而是可以配置mapper的完成比例  
   `mapreduce.job.reduce.slowstart.completedmaps`：默认值0.05  也即是5%  
  `mapreduce.reduce.shuffle.parallelcopies` ：默认值5，同时创建的fetch线程个数   

3. fetch操作涉及到网络，既然涉及到网络就会有如下问题：网络连接超时时长，读取数据超时时长，网络连接最大失败次数。该过程有关的三项配置如下  
`mapreduce.reduce.shuffle.read.timeout`：fetch读数据的timeout时间，默认为3分钟  
`mapreduce.reduce.shuffle.maxfetchfailures`：fetch最大的失败次数，默认为10次  
`mapreduce.reduce.shuffle.connect.timeout`：fetch建立连接的timeout时间，默认也是3分钟  

4. fetch拉取的数据最终目的地是buff和磁盘   
fetch过来的数据放到buff的条件是数据量是否大于buff，大于buff内存则写入磁盘，可以参考mapreduce源码类名称`MergeManagerImpl.java`  
注意这里和map阶段的环形缓冲区buffer没有关系，环形缓冲区是mapper spill数据时用到   

5.  内存merge和磁盘merge（merge & sort）  
fetch负责取数据，merge负责存数据，merge的输出就是reduce的输入。reduce的merge分为内存merge，磁盘merge和finalmerge等阶段将一步步将零散的mapper输出整合成一个**整体有序**的reduce输入。这个阶段的配置有  
`mapreduce.reduce.shuffle.input.buffer.percent`：copy阶段内存使用的最大值，默认为0.70  
`mapreduce.task.io.sort.factor`：每次merge文件个数，关系到需要merge的次数，默认为100          
`mapreduce.reduce.shuffle.memory.limit.percent`：每个fetch取到的输出的大小能够占的内存比，默认是0.25。   因此实际每个fetcher的输出能放在内存的大小是reducer的`Java heap size*0.70*0.25`。所以，如果我们想fetch不进磁盘的话，可以适当调大这个值   
`mapreduce.reduce.merge.memtomem.enabled`：是否enable内存merge，默认是false，如果设置为true，则在finalmerge时内存的数据不会存入磁盘再merge而是直接在内存中merge。参考资料[不建议开启][1]  
 `mapreduce.reduce.shuffle.merge.percent`：默认值0.66，触发merge的内存条件，实际大小计算方式 Java heap size*0.70*0.66  
注意merge过程也会使用combiner，并且和map输出时的combine区别  
最后将内存buf中的值merge到一个输出文件中。DiskMerge的merge过程类似，不过开始merge的条件是磁盘上的零散文件超过mapreduce.task.io.sort.factor的2倍-1。
在fetch结束后会进入finalmerge。所谓finalmerge就是将各个mapper的输入整合成真正reducer的输入。在fetch阶段我们已经一边copy数据一边merge，因此到了finalmerge时，mapper的数据部分存在于内存buf中部分存在于tmp目录的文件中。  

6.  总结一下大致过程  
reducer端的merge包括了内存merge，磁盘merge和finalmerge，其中内存merge是fetch过来的一个个文件segment，每一个fetch过来的文件放在内存或者写入磁盘依据上述介绍的参数来确定，假如都是在内存中，那么随着fetch的越来越多的文件总是需要merge的，这个merge的触发条件是mapreduce.task.io.sort.factor，合并完毕后（这个合并应该是在内存了），随着合并次数增加，占用的内存越来越多，达到一定的条件后会写出到磁盘，这个条件大概是mapreduce.reduce.shuffle.input.buffer.percent，类似于mapper端的spill过程，随着写出到磁盘的文件越来越多，当磁盘上的零散文件超过mapreduce.task.io.sort.factor的2倍-1后，则进行磁盘merge。当fetch结束后进入finalmerge，finalmerge完成各个mapper的输出整合成reducer的输入，由于mapper的部分数据存在于buf和磁盘上的tmp目录中，最后的finalmerge会将二者合并，finalmerge同样通过io.factor来确定是否继续进行merge，所以最后生成的reduce输入文件可能是多个，reducer读取的时候只需要将这多个文件放在queue中依次读取即可


  [1]: https://issues.apache.org/jira/browse/MAPREDUCE-268