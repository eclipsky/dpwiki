问题描述
===================


重启hadoop集群中的某台机器的**TaskTracker**进程后 ，虽然该**TaskTracker**已经启动，但**JobTracker**无法发现该**TaskTracker**,并向其分配MapTask或ReduceTask任务。
----------


问题分析
-------------

查看该TaskTracker启动后的日志信息如下：


>STARTUP_MSG: Starting TaskTracker
>STARTUP_MSG:   host = dchadoop6/192.168.1.177
>STARTUP_MSG:   args = []
>STARTUP_MSG:   version = 1.0.4
>STARTUP_MSG:   build = https://svn.apache.org/repos/asf/hadoop/common/branches/branch-1.0 -r 1393290; compiled by 'hortonfo' on Wed Oct  3 05:13:58 UTC 2012
************************************************************/
>2016-07-08 14:39:17,142 INFO org.apache.hadoop.metrics2.impl.MetricsConfig: loaded properties >from hadoop-metrics2.properties
>2016-07-08 14:39:17,231 INFO org.apache.hadoop.metrics2.impl.MetricsSinkAdapter: Sink ganglia started
>2016-07-08 14:39:18,493 INFO org.apache.hadoop.metrics2.impl.MetricsSourceAdapter: MBean for source MetricsSystem,sub=Stats registered.
>2016-07-08 14:39:18,494 INFO org.apache.hadoop.metrics2.impl.MetricsSystemImpl: Scheduled snapshot period at 10 second(s).
>2016-07-08 14:39:18,494 INFO org.apache.hadoop.metrics2.impl.MetricsSystemImpl: TaskTracker metrics system started
>2016-07-08 14:39:19,463 INFO org.apache.hadoop.metrics2.impl.MetricsSourceAdapter: MBean for source ugi registered.
>2016-07-08 14:39:22,151 INFO org.mortbay.log: Logging to org.slf4j.impl.Log4jLoggerAdapter(org.mortbay.log) via org.mortbay.log.Slf4jLog
>2016-07-08 14:39:22,760 INFO org.apache.hadoop.http.HttpServer: Added global filtersafety (class=org.apache.hadoop.http.HttpServer$QuotingInputFilter)
>2016-07-08 14:39:22,866 INFO org.apache.hadoop.mapred.TaskLogsTruncater: Initializing logs' truncater with mapRetainSize=-1 and reduceRetainSize=-1
>2016-07-08 14:39:22,905 INFO org.apache.hadoop.mapred.TaskTracker: Starting tasktracker with owner as hadoop
>2016-07-08 14:39:22,905 INFO org.apache.hadoop.mapred.TaskTracker: Good mapred local directories are: /data0/hadoop/mapred/map_loc,/data1/hadoop/mapred/map_loc,/data2/hadoop/mapred/map_loc,/data3/hadoop/mapred/map_loc,/data4/hadoop/mapred/map_loc,/data5/hadoop/mapred/map_loc,/data6/hadoop/mapred/map_loc,/data7/hadoop/mapred/map_loc,/data8/hadoop/mapred/map_loc,/data9/hadoop/mapred/map_loc,/data10/hadoop/mapred/map_loc

发现程序一直卡顿在该位置很长时间，于是跟踪了TaskTracker的代码发现
```

    final String dirs = localStorage.getDirsString();
    fConf.setStrings(JobConf.MAPRED_LOCAL_DIR_PROPERTY, dirs);
    LOG.info("Good mapred local directories are: " + dirs);
    taskController.setConf(fConf);
    // Setup task controller so that deletion of user dirs happens properly
    taskController.setup(localDirAllocator, localStorage);
    server.setAttribute("conf", fConf);

    deleteUserDirectories(fConf);

    // NB: deleteLocalFiles uses the configured local dirs, but does not 
    // fail if a local directory has failed. 
    fConf.deleteLocalFiles(SUBDIR);
    final FsPermission ttdir = FsPermission.createImmutable((short) 0755);
    for (String s : localStorage.getDirs()) {
      localFs.mkdirs(new Path(s, SUBDIR), ttdir);
    }
    fConf.deleteLocalFiles(TT_PRIVATE_DIR);
    final FsPermission priv = FsPermission.createImmutable((short) 0700);
    for (String s : localStorage.getDirs()) {
      localFs.mkdirs(new Path(s, TT_PRIVATE_DIR), priv);
    }
    fConf.deleteLocalFiles(TT_LOG_TMP_DIR);
    final FsPermission pub = FsPermission.createImmutable((short) 0755);
    for (String s : localStorage.getDirs()) {
      localFs.mkdirs(new Path(s, TT_LOG_TMP_DIR), pub);
    }

```
----------
主程序一直尝试在删除${HADOOP_HOME}/conf/mapred-site.xml 中的配置项`mapred.local.dir`的目录 下的文件。经过分析，该目录缓存了大量job执行时所需要的文件（jar，xml等）。继续往下，发现以下代码：
```
  // Initialize DistributedCache
    this.distributedCacheManager = new TrackerDistributedCacheManager(
        this.fConf, taskController);
    this.distributedCacheManager.startCleanupThread();
```

原来组件 ` TrackerDistributedCacheManager` 就是mapreduce 管理分布式缓存的。继续进入源码，查看其工作原理。
-------------------
```
public class TrackerDistributedCacheManager {
  // cacheID to cacheStatus mapping
  private LinkedHashMap<String, CacheStatus> cachedArchives = 
    new LinkedHashMap<String, CacheStatus>();
  private Map<JobID, TaskDistributedCacheManager> jobArchives =
    Collections.synchronizedMap(
        new HashMap<JobID, TaskDistributedCacheManager>());
  private final TaskController taskController;
  private static final FsPermission PUBLIC_CACHE_OBJECT_PERM =
    FsPermission.createImmutable((short) 0755);

  // default total cache size (10GB)
  private static final long DEFAULT_CACHE_SIZE = 10737418240L;
  private static final long DEFAULT_CACHE_SUBDIR_LIMIT = 10000;
  private static final float DEFAULT_CACHE_KEEP_AROUND_PCT = 0.95f;
  private long allowedCacheSize;
```
该类有三个参数 `DEFAULT_CACHE_SIZE `,`DEFAULT_CACHE_SUBDIR_LIMIT `,`DEFAULT_CACHE_KEEP_AROUND_PCT ` 会影响Tasktracker 所在机器的缓存大小。

假如配置项`mapred.local.dir`有10个目录，则该机器的缓存可达 10*10GB*0.95f,而TaskTracker 再启动时，主线程必须删光该缓存，才会做下一步的初始化工作。这样导致 TaskTracker 初始化过程卡顿，无法上报心跳给JobTracker，也无法接收 JobTracker 分发的任务。

在mapred-site.xml 中修改下面三个配置项，则可有效减少主线程卡顿时间
```
<property>
        <name>local.cache.size</name>
        <value>2147483648L</value>
</property>

<property>
        <name>mapreduce.tasktracker.local.cache.numberdirectories</name>
        <value>3000</value>
</property>

<property>
        <name>mapreduce.tasktracker.cache.local.keep.pct</name>
        <value>0.75f</value>
</property>

```
 [1]: https://dodizzle.wordpress.com/2010/10/23/hadoop-too-much-local-cache-is-a-bad-thing/

----------


----------




























