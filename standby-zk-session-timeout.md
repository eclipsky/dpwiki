### 现象
standby 的resource manager偶尔会出现以下问题
```
2016-07-21 00:20:27,217 INFO org.apache.zookeeper.ClientCnxn: Client session timed out, have not heard from server in 26907ms for sessionid 0x25233de3625c75b, closing socket connection and attempting reconnect
2016-07-21 00:20:27,981 INFO org.apache.hadoop.ha.ActiveStandbyElector: Session disconnected. Entering neutral mode...
2016-07-21 00:20:28,014 INFO org.apache.zookeeper.ClientCnxn: Opening socket connection to server dckafka3/192.168.1.152:12181. Will not attempt to authenticate using SASL (unknown error)
2016-07-21 00:20:28,015 INFO org.apache.zookeeper.ClientCnxn: Socket connection established, initiating session, client: /192.168.1.192:41073, server: dckafka3/192.168.1.152:12181
2016-07-21 00:20:28,016 INFO org.apache.zookeeper.ClientCnxn: Unable to reconnect to ZooKeeper service, session 0x25233de3625c75b has expired, closing socket connection
2016-07-21 00:20:28,018 INFO org.apache.hadoop.ha.ActiveStandbyElector: Session expired. Entering neutral mode and rejoining...
2016-07-21 00:20:28,018 INFO org.apache.hadoop.ha.ActiveStandbyElector: Trying to re-establish ZK session
```
从日志看standby的resourcemanager由于session timeout 去 reconnect失败，导致重新进行re-establish，从而触发新的active resource manager的选举，由于是standby节点，选举后仍然是standby

### 问题调查    
去zk 集群查看日志，一台机器上确实有以下日志：
```
grep 0x25233de3625c75b zookeeper.log.1

2016-07-21 00:20:27,854 [myid:3] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:12181:ZooKeeperServer@861] - Client attempting to renew session 0x25233de3625c75b at /192.168.1.192:41073
2016-07-21 00:20:27,854 [myid:3] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:12181:Learner@108] - Revalidating client: 0x25233de3625c75b
2016-07-21 00:20:27,854 [myid:3] - INFO  [QuorumPeer[myid=3]/0:0:0:0:0:0:0:0:12181:ZooKeeperServer@610] - Invalid session 0x25233de3625c75b for client /192.168.1.192:41073, probably expired
2016-07-21 00:20:27,854 [myid:3] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:12181:NIOServerCnxn@1007] - Closed socket connection for client /192.168.1.192:41073 which had sessionid 0x25233de3625c75b
```
通过日志可知，当client去renew session的时候失败了，可能是由于设置的超时时间内server 没有收到client的心跳，故清理掉了该session，导致renew失败。
查看yarn resourcemanager的zk超时设置```yarn.resourcemanager.zk-timeout-ms```，默认是10000ms。有点太小。
### 解决方法
将```yarn.resourcemanager.zk-timeout-ms```设置大些，例如60000ms。
### 操作步骤
1. 修改standby的yarn-site.xml中的```yarn.resourcemanager.zk-timeout-ms```大小为60000
2.  重启standby resource manager，观察效果   

### 操作影响   
无，由于重启standby节点，不会触发ha切换
<br>
<br>
<br>