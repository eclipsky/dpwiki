**一、	zabbix基本架构**
![image](/uploads/003f202b1b062a9754f87edf832e8e60/image.png)
**Server1：**zabbix服务端，通过zabbix_agentd或者zabbix_proxy收集数据保存到Server3中   
**Server2：**基于apache php平台结合zabbix web gui这个组件，通过浏览器连接web gui访问zabbix管理页面   
**Server3：**数据库服务器，提供数据存储功能   
**zabbix_sender：**agentd自身出现异常时，主动发送数据给zabbix服务端   
**zabbix_get：**与agentd通信，主动拉取数据   
**zabbix_agentd：**能监控database，Device，application   
**zabbix proxy**：分布式环境中，所有被监控设备与其通信，由其收集数据，定期发送给Zabbix Server，减少Server并发量   
``注意：线上zabbix服务的环境，全部集中到一台web4机器上，运行环境：linux+php+apache+mysql5.6，zabbix源码编译``   
**二、zabbix 常用术语**   
主机（host）：要监控的网络设备，可由IP或DNS名称指定   
主机组（host group）：主机的逻辑容器，包含主机和配置模版   
监控项（item）：一个特定监控指标的相关数据，这些数据来自于被监控对象   
触发器（trigger）：一个表达式，评估监控对象item的数据是否在合理范围，即阈值；监控到其数据大于阈值时，触发器状态将从“OK”转变为“problem”，当数据量再次回归到合理范围时，其状态将从“problem”转换为“OK”   
事件（event）：即发生的一个值得关注的事情，例如触发器状态的转变，新的agent或重新上线的agent的自动注册等   
动作（action）：对特定事件事先定义的处理方法， 包含条件（何时执行操作）和操作（如发送通知）   
报警升级（escalation）：发送警报或执行远程命令的自定义方案，如每隔5分钟发送一次警报，共发送5次等   
媒介（media）：发送同志的手段或通道，如e-mail，sms，jabber等   
通知（notification）：通过选定的媒介想用户发送有关的某事件的信息   
远程命令（remote command）：预定义的命令，可在被监控主机处于某特定条件下时自动执行   
模版（template）：用于快速定义被监控主机的预设条目集合，通常包含了item、trigger、graph、screen、application以及low-level discovery rule，模版可以直接链接至单个主机   
应用（application）：一组item的集合   
web场景（web scennario）：用于检测web站点可用性的一个或多个HTTP请求   
前端（frontend）：Zabbix的web接口   

**三、zabbix逻辑架构**   
![image](/uploads/722183f566938ce108e5e21256d382f3/image.png)
Zabbix Poller：基于拉取机制的Zabbix Server，Zabbix Poller通过SNMP协议、Zabbix Agent、Internal、等方式拉取数据   
Zabbix Agent：采集被监控设备其item数据；通过监控阈值即触发器（triggers）状态，对这类事件（Events）执行（Actions）事先定义的处理方法，如发送Email或其他动作   
**四、zabbix存放路径**   
service端和agent端   
配置文件：``/etc/zabbix``   
安装路径：``/usr/local/zabbix``   
**五、线上实现监控指标**   
1、主机监控：内存，磁盘利用率和I/O，swap使用率，负载，主机存活率，磁盘健康状态   
2、网卡监控：Ping的往返时间及包成功率，网卡流量，包括流入/流出量和错误的数据包数   
3、应用程序：端口和内存使用率，CPU使用率，服务状态，请求数，并发连接数，消息队列的字节数，Client事务处理数，Service状态等   
4、数据库:监测数据库中指定表空间，数据库的游标数，Session数，事务数，死锁数，缓冲池命中率，库Cache命中率，当前连接数，进程的内存利用率等性能参数   
7、日志：错误日志匹配，特定字符串匹配   
8、硬件：温度，风扇转速，电压等   

**六、自定义key**
   需要/etc/zabbix/zabbix_agend.conf后添加内容   
```
#
UserParameter=system1.uname,/bin/uname -r

###Recv-q
UserParameter=recv-q, ss -nl|awk '{print $2}'|grep -v "Recv-Q"|awk '{if($1>0) { print 1}}'|wc -l
####memory
UserParameter=master.memtotal,echo "scale=2;`/usr/local/zabbix/script/memory.sh Memtotal` /1024" | bc
UserParameter=master.memuse,echo "scale=2;`/usr/local/zabbix/script/memory.sh Memuse` /1024" | bc
UserParameter=master.memfree,echo "scale=2;`/usr/local/zabbix/script/memory.sh Memfree` /1024" | bc
UserParameter=master.bufferuse,echo "scale=2;`/usr/local/zabbix/script/memory.sh Bufferuse` /1024" | bc
UserParameter=master.bufferfree,echo "scale=2;`/usr/local/zabbix/script/memory.sh Bufferfree` /1024" | bc

####disk
#UserParameter=dcserver4.root.total,/bin/df -lh  | sed -n 2p | awk '{print $2}' | sed s/G//g
#UserParameter=dcserver4.root.use,/bin/df -lh  | sed -n 2p | awk '{print $3}' | sed s/G//g
#UserParameter=dcserver4.root.use_per,/bin/df -lh  | sed -n 2p | awk '{print $(NF-1)}' | sed s/%//g
 
#UserParameter=dcserver4.data0.total,/bin/df -lh  | sed -n 5p | awk '{print $2}' | sed s/G//g 
#UserParameter=dcserver4.data0.use,/bin/df -lh  | sed -n 5p | awk '{print $3}' | sed s/G//g 
#UserParameter=dcserver4.data0.use_per,/bin/df -lh  | sed -n 5p | awk '{print $(NF-1)}' | sed s/%//g 

#UserParameter=dcserver4.data1.total,/bin/df -lh  | sed -n 6p | awk '{print $2}' | sed s/G//g 
#UserParameter=dcserver4.data1.use,/bin/df -lh  | sed -n 6p | awk '{print $3}' | sed s/M//g
#UserParameter=dcserver4.data1.use_per,/bin/df -lh  | sed -n 6p | awk '{print $(NF-1)}' | sed s/%//g
 

###CPU load

UserParameter=master.cpu1,/usr/bin/w  | sed -n 1p | awk -F ":"  '{print $5}' | awk -F "," '{print $1}'
UserParameter=master.cpu5,/usr/bin/w  | sed -n 1p | awk -F ":"  '{print $5}' | awk -F "," '{print $2}'
UserParameter=master.cpu15,/usr/bin/w  | sed -n 1p | awk -F ":"  '{print $5}' | awk -F "," '{print $3}'


###### uptime
UserParameter=master.uptime,/usr/bin/w | sed -n 1p | awk -F ","  '{print $1}' | awk -F  " " '{print $(NF-1)}'

####DISK IO
UserParameter=disk.pgpgin,cat /proc/vmstat | grep pgpgin | awk -F " " '{print $2}'
UserParameter=disk.pgpgout,cat /proc/vmstat | grep pgpgout| awk -F " " '{print $2}'
UserParameter=disk.pswpin,cat /proc/vmstat | grep pswpin |awk '{print $2}'
UserParameter=disk.pswpout,cat /proc/vmstat | grep pswpout |awk '{print $2}'
###TCP,UDP
UserParameter=passive.tcp.conn,cat /proc/net/snmp | grep Tcp | awk -F: '{print $2}'|awk -F " " '{print $6}'| grep -v PassiveOpens
UserParameter=tcp.conn,cat /proc/net/snmp | grep Tcp | awk -F: '{print $2}'|awk -F " " '{print $9}'| grep -v CurrEstab
UserParameter=udp.receive,cat /proc/net/snmp | grep Udp:|awk -F: '{print $2}'|awk -F " " '{print $1}'|grep -v InDatagrams
UserParameter=udp.send,cat /proc/net/snmp | grep Udp:|awk -F: '{print $2}'|awk -F " " '{print $4}'|grep -v OutDatagrams

####IO
UserParameter=custom.vfs.dev.read.ops[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$4}'
UserParameter=custom.vfs.dev.read.ms[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$7}'
UserParameter=custom.vfs.dev.write.ops[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$8}'
UserParameter=custom.vfs.dev.write.ms[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$11}'
UserParameter=custom.vfs.dev.io.active[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$12}'
UserParameter=custom.vfs.dev.io.ms[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$13}'
UserParameter=custom.vfs.dev.read.sectors[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$6}'
UserParameter=custom.vfs.dev.write.sectors[*],cat /proc/diskstats | grep $1 | head -1 | awk '{print $$10}'

####Dynamic monitoring of disk performance
UserParameter=io.avgrq-sz[*],/usr/bin/tail /tmp/iostat_output | grep $1 |tail -1|awk '{print $$8}'
UserParameter=io.avgqu-sz[*],/usr/bin/tail /tmp/iostat_output | grep $1 | tail -1 | awk '{print $$9}'
UserParameter=io.await[*],/usr/bin/tail /tmp/iostat_output | grep $1 | tail -1 | awk '{print $$10}'
UserParameter=io.svctm[*],/usr/bin/tail /tmp/iostat_output | grep $1 | tail -1 | awk '{print $$11}'
UserParameter=io.util[*],/usr/bin/tail /tmp/iostat_output |grep $1 | tail -1 | awk '{print $$12}'

###vmstat Performance status
UserParameter=io.vmstat.wa,cat /tmp/vmstat_output |tail -1 |awk -F " " '{print $16}'


#####zabbix_low_discovery
UserParameter=zabbix_low_discovery[*],/bin/bash /usr/local/zabbix/script/zabbix_low_discovery.sh $1

####check disk status##
UserParameter=check_disk_status,mount | awk '{print $NF}'|cut -c 2-3|awk '{if($1~/ro/) {print 1}}'|wc -l|awk '{if($1<=0) {print 0 } else {print 1}}'


######check servers status###
UserParameter=check_ip_exsit[*],nmap -sn $1 | grep down | wc -l
UserParameter=check_179_exsit,nmap -sn 192.168.1.179 | grep down | wc -l


####mysql
#UserParameter=zabbix_low_discovery[*],/bin/bash /usr/local/zabbix/script/zabbix_low_discovery.sh $1
#UserParameter=mysql_stats[*],mysql -h 127.0.0.1 -P $1 -uzabbix -phuidong2013 -e "show global status"|grep "\<$2\>"|cut  -f2
UserParameter=mysql_stats[*],sudo /usr/local/mysql/bin/mysql --login-path=local -P $1 -e "show global status" |grep "\<$2\>" |cut -f2
UserParameter=mysql_stats_slave[*],sudo /usr/local/mysql/bin/mysql --login-path=local -P $1 -e "show slave global status\G" |grep "\<$2\>" |awk '{if($NF=="Yes"){print 1} else {print 0}}'

#####redis
UserParameter=redis_stats[*],(echo info;sleep 1) | telnet 192.168.1.205 $1 2>&1 | grep $2|cut -d : -f2


#######follow is monitor hardware 
UserParameter=hardware_battery,omreport chassis batteries|awk '/^Status/{if($NF=="Ok") {print 1} else {print 0}}'

UserParameter=hardware_cpu_model,awk -v hardware_cpu_crontol=`sudo omreport  chassis biossetup|awk '/C State/{if($NF=="Enabled") {print 0} else {print  1}}'` -v hardware_cpu_c1=`sudo omreport chassis biossetup|awk '/C1[-|E]/{if($NF=="Enabled") {print 0} else {print 1}}'` 'BEGIN{if(hardware_cpu_crontol==0 && hardware_cpu_c1==0) {print 0} else {print 1}}'

UserParameter=hardware_fan_health,awk -v hardware_fan_number=`omreport chassis fans|grep -c "^Index"` -v hardware_fan=`omreport chassis fans|awk '/^Status/{if($NF=="Ok") count+=1}END{print count}'` 'BEGIN{if(hardware_fan_number==hardware_fan) {print 1} else {print 0}}'

UserParameter=hardware_memory_health,awk -v hardware_memory=`omreport chassis memory|awk '/^Health/{print $NF}'` 'BEGIN{if(hardware_memory=="Ok") {print 1} else {print 0}}'

UserParameter=hardware_nic_health,awk -v hardware_nic_number=`omreport chassis nics |grep -c "Interface Name"` -v hardware_nic=`omreport chassis nics |awk '/^Connection Status/{print $NF}'|wc -l` 'BEGIN{if(hardware_nic_number==hardware_nic) {print 1} else {print 0}}'

UserParameter=hardware_cpu,omreport chassis processors|awk '/^Health/{if($NF=="Ok") {print 1} else {print 0}}'

UserParameter=hardware_power_health,awk -v hardware_power_number=`omreport chassis pwrsupplies|grep -c "Index"` -v hardware_power=`omreport chassis pwrsupplies|awk '/^Status/{if($NF=="Ok") count+=1}END{print count}'` 'BEGIN{if(hardware_power_number==hardware_power) {print 1} else {print 0}}'

UserParameter=hardware_temp,omreport chassis temps|awk '/^Status/{if($NF=="Ok") {print 1} else {print 0}}'|head -n 1 

UserParameter=hardware_physics_health,awk -v hardware_physics_disk_number=`omreport storage pdisk controller=0|grep -c "^ID"` -v hardware_physics_disk=`omreport storage pdisk controller=0|awk '/^Status/{if($NF=="Ok") count+=1}END{print count}'` 'BEGIN{if(hardware_physics_disk_number==hardware_physics_disk) {print 1} else {print 0}}'

UserParameter=hardware_virtual_health,awk -v hardware_virtual_disk_number=`omreport storage vdisk controller=0|grep -c "^ID"` -v hardware_virtual_disk=`omreport storage vdisk controller=0|awk '/^Status/{if($NF=="Ok") count+=1}END{print count}'` 'BEGIN{if(hardware_virtual_disk_number==hardware_virtual_disk) {print 1} else {print 0}}' 

###Add include
Include=/etc/zabbix/zabbix_agentd.conf.d/*.conf
```



