**1. 任虎提供的原始hive sql如下**
```sql
use dmp;   
set mapreduce.reduce.memory.mb=4096;   
CREATE TABLE if not exists t_all_game_app_online_grade(device_id string, category string, nt int)PARTITIONED BY (run_date string) row format delimited fields terminated by '   ' lines terminated by '
' stored as orc;   
insert overwrite table t_all_game_app_online_grade partition(run_date = '2016-08-07')   
select device_id,category,ntile(3) over(partition by category order by value_nt asc) as nt from (   
      select tin_a.device_id,tin_a.category,(0.6*num_nt+0.4*today_days_nt) as value_nt from  (   
            select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt from t_all_game_app_dis_online_month    
            where  run_date='2016-08-07'
        )tin_a
        join (   
            select device_id,category,ntile(10) over(partition by category order by datediff('2016-08-07',max_date) desc) as today_days_nt   
            from  t_all_game_app_dis_online_month where run_date='2016-08-07'   
        )tin_b   
        on tin_a.device_id=tin_b.device_id and tin_a.category=tin_b.category   
 )tb;   
```
  **执行报错OOM**
![image](/uploads/b2f076399d49acde22ded2686603da70/image.png)




**2 改造如下**
```sql
set hive.enforce.bucketing = true;--开启分桶
set mapreduce.reduce.java.opts=-Xmx14336M -XX:+UseG1GC -XX:MaxGCPauseMillis=2000 -XX:InitiatingHeapOccupancyPercent=40   -XX:ConcGCThreads=4 -XX:PermSize=256M -XX:MaxPermSize=512M  -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=128K -XX:HeapDumpPath=/tmp/mapreduce.task.@taskid@.hprof -Xloggc:/tmp/reduce/mapreduce.task.@taskid@.gc.log -XX:ErrorFile=/tmp/reduce/hs_err_pid%p.log;--设置jvm启动参数，堆内存增加到14g
set mapreduce.reduce.memory.mb=15360;--增加reduce任务申请内存为15g（现网集群设置的container最大内存为20g）
set mapreduce.reduce.input.buffer.percent=0.6;--设置reduce缓冲区刷磁盘的比率为0.6，默认值为0.8，减少比率可以减少oom的概率（此处需要进一步验证）
set mapreduce.job.reduces=60;--直接设置reduce的数目，将数据进一步分散
set hive.optimize.skewjoin = true;--优化join关联
set hive.skewjoin.key=1000000000;--优化join关联，关联键超过该设置值时，hive会单独启动一个job进行处理
set mapred.map.tasks.speculative.execution=false;--关闭map推测执行，hivejob通常不需要开启，对于集群节点配置相同的也无须开启
set mapred.reduce.tasks.speculative.execution=false;--关闭reduce推测执行
set hive.mapred.reduce.tasks.speculative.execution=false;--关闭推测执行，该参数会覆盖mapred-site.xml配置
use dmp;
set mapred.job.queue.name=warehouse;
set mapred.job.name=fonder;

--通过对category分桶的方式加快子句ntile(10) over(partition by category order by num asc)查询速度，同时使用orc存储格式
drop table orc_bucket_tmp;
CREATE TABLE `orc_bucket_tmp`(
  `device_id` string,
  `category` string,
  `durations` int,
  `num` int,
  `days` int,
  `max_date` string,
  `flag` int)
PARTITIONED BY (`run_date` string)
clustered by(category) sorted by(num) into 128 buckets
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
stored as orc;

insert into table orc_bucket_tmp partition(run_date) select device_id, category, durations, num, days, max_date, flag, run_date from t_all_game_app_dis_online_year;
CREATE TABLE if not exists t_all_game_app_online_grade(device_id string, category string, nt int)PARTITIONED BY (run_date string) row format delimited fields terminated by '   ' lines terminated by '
' stored as orc;
insert overwrite table t_all_game_app_online_grade partition(run_date = '2016-08-07')
select device_id,category,ntile(3) over(partition by category order by value_nt asc) as nt from
(
    select tin_a.device_id,tin_a.category,(0.6*num_nt+0.4*today_days_nt) as value_nt from
    (
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt from orc_bucket_tmp
    )tin_a
    join
    (
        select device_id,category,ntile(10) over(partition by category order by datediff('2016-08-07',max_date) desc) as today_days_nt from orc_bucket_tmp
    )tin_b
    on tin_a.device_id=tin_b.device_id and tin_a.category=tin_b.category
)tb;

```
***上述sql会报数据格式错误，具体错误log不再重现***  
  
**3. 继续改造，恢复textfile存储格式，并随机拆分多个文件**
```shell
for i in {00..19};do hive -e "
use dmp;
set mapred.reduce.tasks=20;
create table random_tmp as select device_id,category,cast(durations as int),cast(num as int),cast(days as int),max_date,flag,run_date from t_all_game_app_dis_online_year distribute by rand(1234);
drop table if exists random_$i;
CREATE TABLE `random_$i`(
  `device_id` string,
  `category` string,
  `durations` int,
  `num` int,
  `days` int,
  `max_date` string,
  `flag` int,
  `run_date` string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
location 'hdfs://de-hdfs/user/hive/warehouse/dmp.db/random_tmp/0000$i';
";done
for i in {00..19};do hive -e "use dmp; drop table if exists random_$i; CREATE TABLE random_$i( device_id string, category string, durations int, num int, days int, max_date string, flag int, run_date string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n';";done
for i in {00..19};do hdfs dfs -mv hdfs://de-hdfs/user/hive/warehouse/dmp.db/random_tmp/0000${i}_0 hdfs://de-hdfs/user/hive/warehouse/dmp.db/random_${i}/;done
```
```sql

set hive.exec.parallel=false;
CREATE TABLE if not exists t_all_game_app_online_grade_random02(device_id string, category string, nt int,flag int)PARTITIONED BY (run_date string) row format delimited fields terminated by '\t' lines terminated by '\n' stored as orc;
insert overwrite table t_all_game_app_online_grade_random02 partition(run_date='2016-08-07')
select device_id,category,ntile(3) over(partition by category order by value_nt asc) as nt,flag from
(
    select device_id,category,(0.3*num_nt + 0.3*days_nt + 0.4*today_days_nt) as value_nt,flag from
    (
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_00
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_01
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_02
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_03
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_04
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_05
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_06
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_07
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_08
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_09
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_10
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_11
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_12
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_13
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_14
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_15
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_16
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_17
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_18
        union all
        select device_id,category,ntile(10) over(partition by category order by num asc) as num_nt,
                ntile(10) over(partition by category order by days asc) as days_nt,
                ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,flag
        from random_19
    ) ta
)tb;
```
**任务运行失败，报错信息timeout，原因是单个reduce处理任务时长超过了设置的超时时长**  
![image](/uploads/84c8a7fcf078e3c47493ca2572e02b74/image.png)
  

**4. 继续改造**  
***后面和任虎沟通仔细了解了业务背景，发现该函数ntile(10) over(partition by category order by num asc)并不会减少数据量，同时尝试跑了下面的sql已观察数据质量（原本预测该sql同样会由于数据倾斜无法运行成功的）***   
```sql
select category,count(1) from t_all_game_app_dis_online_year group by category;
```
***上述sql运行结果发现无效值非常多，是导致倾斜的主要原因，调大reduce内存为20G后，下面语句运行成功***
```shell
for i in {00..19};do hive -e "set hive.enforce.bucketing = true;
set mapreduce.reduce.java.opts=-Xmx19456M -XX:+UseG1GC -XX:MaxGCPauseMillis=2000 -XX:InitiatingHeapOccupancyPercent=40 -XX:ConcGCThreads=4 -XX:PermSize=256M -XX:MaxPermSize=512M  -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=128K -XX:HeapDumpPath=/tmp/mapreduce.task.@taskid@.hprof -Xloggc:/tmp/reduce/mapreduce.task.@taskid@.gc.log -XX:ErrorFile=/tmp/reduce/hs_err_pid%p.log;
set mapreduce.reduce.memory.mb=20480;
set mapreduce.reduce.input.buffer.percent=0.6;
set mapreduce.job.reduces=100;
set yarn.nodemanager.vmem-pmem-ratio=4;
set mapreduce.task.timeout=1200000;
set hive.optimize.skewjoin = true;
set hive.skewjoin.key=1000000000;
set mapred.map.tasks.speculative.execution=false;
set mapred.reduce.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
set hive.exec.reducers.bytes.per.reducer;
set hive.exec.reducers.max;
use dmp;
set mapred.job.queue.name=warehouse;
set mapred.job.name=fonder;
drop table if exists tmp_random_${i};
create table tmp_random_${i} stored as textfile as select device_id,
category,ntile(10) over(partition by category order by num asc) as num_nt,
ntile(10) over(partition by category order by days asc) as days_nt,
ntile(10) over(partition by category order by datediff('%s',max_date) desc) as today_days_nt,
flag 
from random_${i};
";done &>>/tmp/fonder_hive.log
```
```sql
set hive.enforce.bucketing = true;
set mapreduce.reduce.java.opts=-Xmx19456M -XX:+UseG1GC -XX:MaxGCPauseMillis=2000 -XX:InitiatingHeapOccupancyPercent=40 -XX:ConcGCThreads=4 -XX:PermSize=256M -XX:MaxPermSize=512M  -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=128K -XX:HeapDumpPath=/tmp/mapreduce.task.@taskid@.hprof -Xloggc:/tmp/reduce/mapreduce.task.@taskid@.gc.log -XX:ErrorFile=/tmp/reduce/hs_err_pid%p.log;
set mapreduce.reduce.memory.mb=20480;
set mapreduce.reduce.input.buffer.percent=0.6;
set mapreduce.job.reduces=100;
set yarn.nodemanager.vmem-pmem-ratio=4;--虚拟内存比率由默认值2.1增大为4
set mapreduce.task.timeout=1200000;
set hive.optimize.skewjoin = true;
set hive.skewjoin.key=1000000000;
set mapred.map.tasks.speculative.execution=false;
set mapred.reduce.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
set hive.exec.reducers.bytes.per.reducer;
set hive.exec.reducers.max;
use dmp;
set mapred.job.queue.name=warehouse;
set mapred.job.name=fonder;
drop table t_all_game_app_online_grade_random02;
CREATE TABLE if not exists t_all_game_app_online_grade_random02(device_id string, category string, nt int,flag int)PARTITIONED BY (run_date string) row format delimited fields terminated by '\t' lines terminated by '\n' stored as textfile;
create table tmp_random_a as 
select device_id,category,(0.3*num_nt + 0.3*days_nt + 0.4*today_days_nt) as value_nt,flag from
(
    select * from tmp_random_00
    union all
    select * from tmp_random_01
    union all
    select * from tmp_random_02
    union all
    select * from tmp_random_03
    union all
    select * from tmp_random_04
    union all
    select * from tmp_random_05
    union all
    select * from tmp_random_06
    union all
    select * from tmp_random_07
    union all
    select * from tmp_random_08
    union all
    select * from tmp_random_09 
    union all
    select * from tmp_random_10
) ta;
create table tmp_random_b as 
select device_id,category,(0.3*num_nt + 0.3*days_nt + 0.4*today_days_nt) as value_nt,flag from
(   
    select * from tmp_random_11
    union all
    select * from tmp_random_12
    union all
    select * from tmp_random_13
    union all
    select * from tmp_random_14
    union all
    select * from tmp_random_15
    union all
    select * from tmp_random_16
    union all
    select * from tmp_random_17
    union all
    select * from tmp_random_18
    union all
    select * from tmp_random_19     
) ta;
insert overwrite table t_all_game_app_online_grade_random02 partition(run_date='2016-08-07')
select device_id,category,ntile(3) over(partition by category order by value_nt asc) as nt,flag from
(
    select * from tmp_random_a where category is not NULL and category<>'-'
    union all
    select * from tmp_random_b where category is not NULL and category<>'-'
)tb;
```
**总结：**

**1) 表的拆分主要是减少子句ntile(10) over(partition by category order by xxx)倾斜**  
**2) 在执行之前尽量了解数据的分布情况**  
**3) 数据量大或者数据分布过于集中，可以将reduce虚拟内存调大**  

---
