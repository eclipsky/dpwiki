HBase日常运维   
----------------------------------------
组件部署情况      

| 机器        | IP   |  组件  |
| --------   | :-----:  | :----:  |
| dcnamenode1|192.168.1.164 |   HMaster|
| dckafka1   |192.168.1.150 |   HRegionServer  |
| dckafka2   |192.168.1.151 |   HRegionServer|
| dckafka3   |192.168.1.152 |   HRegionServer|
| dchbase1  |192.168.1.173 |   HRegionServer|
| dchbase2  |192.168.1.172 |   HRegionServer|
| dchbase3  |192.168.1.181 |   HRegionServer|


HBase0.94 实时任务切换过程   
----------------------------------------
背景：

1、HBase0.94已经关闭major compact功能，手动也触发不了   
2、HBase0.94集群的Region不能超过2000个   
3、HBase的 dc_debug_log 表和 dc_rt_coin_plugin 表的region数量增长速度太快   
为了减少region的数量，需要对以上两种表定时做切换，切换结果是   
>dc_debug_log   <==>  dc_debug_log_new   
dc_rt_coin_plugin   <==> dc_rt_coin_plugin _new   

### 实时日志任务切换   
对应实时任务：dcDebugLogStorm   
对应HBase表：dc_debug_log    

1、目标   
把实时任务dcDebugLogStorm切换为dcDebugLogStormNew    
把HBase表dc_debug_log切换为dc_debug_log_new    

2、启动jstorm任务
```
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.ActiveLogTopologyNew
```
我们的kafka消息系统中，会把数据保留一星期，所以使用以下命令观察kafka中的数据是否消费完成   
```
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar debugLogTopic debugLogGroupNew 6 dckafka1,dckafka2,dckafka3
```
如果完成了，才执行以下步骤   

3、切换前端对应的hbase表   
通知蔡昌梁，把前端读取dc_debug_log表的代码，改为dc_debug_log_new   

4、检查界面中的实时日志是否正常    
找到一个有实时日志的用户，登录后打开“实时日志”标签，查看数据是否正常   

5、关闭老的实时任务   
```
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcDebugLogStorm
```

### 虚拟币任务切换   
对应实时任务：dcDebugLogStorm   
对应HBase表：dc_debug_log    

1、目标   
把实时任务dcCoinPluginStorm切换为dcCoinPluginStormNew   
把HBase表dc_rt_coin_plugin切换为dc_rt_coin_plugin_new   

2、启动jstorm任务
```
/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.CoinPluginTopologyNew
```
我们的kafka消息系统中，会把数据保留一星期，所以使用以下命令观察kafka中的数据是否消费完成   
```
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcCoinPluginTopic dcCoinPluginGroupNew 6 dckafka1,dckafka2,dckafka3
```
如果完成了，才执行以下步骤   

3、切换前端对应的hbase表   
通知蔡昌梁，把前端读取dc_rt_coin_plugin表的代码，改为dc_rt_coin_plugin_new   

4、检查界面中的实时日志是否正常    
找到一个有实时日志的用户，登录后打开“虚拟币”标签，查看数据是否正常   

5、关闭老的实时任务   
```
/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcCoinPluginStorm
```

>  注意：以上过程是从 xxxx 修改为 xxx_new ，那下一次切换的时候就是 xxx_new 修改为 xxx 了