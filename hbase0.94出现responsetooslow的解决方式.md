Hbase0.94出现responsetooslow的解决方式   
----------------------------------------
### 一、概述   
实时模块的任务出现延迟，并且重启topoloygy也无法拉起tps的时候，查看hbase的6台机器的log日志：
```
1、dckafka1：目录 -- /data0/hbase/logs，文件 -- hbase-hadoop-regionserver-dckafka1.log
2、dckafka2：目录 -- /data0/hbase/logs，文件 -- hbase-hadoop-regionserver-dckafka2.log
3、dckafka3：目录 -- /data0/hbase/logs，文件 -- hbase-hadoop-regionserver-dckafka3.log
4、dchbase1：目录 -- /data0/hbase/logs，文件 -- hbase-hadoop-regionserver-dchbase1.log
5、dchbase2：目录 -- /data0/hbase/logs，文件 -- hbase-hadoop-regionserver-dchbase2.log
6、dchbase3：目录 -- /data0/hbase/logs，文件 -- hbase-hadoop-regionserver-dchbase3.log
```
如果大量出现responsetooslow的日志，类似于以下这种：  

2017-04-19 10:23:50,985 WARN org.apache.hadoop.ipc.HBaseServer: (responseTooSlow): {"processingtimems":33073,"call":"multi(org.apache.hadoop.hbase.client.MultiAction@47443753), rpc version=1, client version=29, methodsFingerPrint=-540141542","client":"192.168.1.151:49475","starttimems":1492568597912,"queuetimems":0,"class":"HRegionServer","responsesize":0,"method":"multi"}    

则是说明该机器的regionServer请求出现堆积，处理方式是使用hbase自带的 graceful_stop.sh 命令进行region的安全迁移   

### 二、处理步骤   
1、从每台机器的日志上分析定位是哪台机器大量出现了responseTooSlow日志   

2、进入该机器的$HBASE_HOME目录，在bin目录下找到 graceful_stop.sh 命令   

3、执行命令： ./graceful_stop.sh --restart hostname，比如如果是dckafka3机器，则执行   
```
./graceful_stop.sh --restart dckafka3
```
4、命令在执行迁移region，过程比较长，需要大概半个小时的时间   

5、命令执行到最后，需要输入两次该机器的密码，6台hbase机器的hadoop用户的密码都是 dchd@2013   

6、最后重启jstorm任务即可   

### 三、其它   

1、一般出现问题的都是 dcRTTopology 这个任务   

2、dcRTTopology 重启过程的命令如下：
```
1、打开跳板机，进入dctest1机器
   ssh jstorm2@dctest1 
   密码：jstorm2@2016
2、冻结任务命令：/home/jstorm2/jstorm-2.1.0/bin/jstorm deactivate dcRTTopology
3、杀死任务命令：/home/jstorm2/jstorm-2.1.0/bin/jstorm kill dcRTTopology
4、重启任务命令：/home/jstorm2/jstorm-2.1.0/bin/jstorm jar /home/jstorm2/jstorm-2.1.0/dc_topology/ga1/dc-storm2-ga1-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.dataeye.storm.rt.online.RTTopology
```
 
3、查看 dcRTTopology 任务还有多少延迟
```
进入 dckafka1 机器，执行一下命令：
java -jar /home/hadoop/kafka_2.9.2-0.8.2.2/libs/kafka.jar dcRTTopic dcRTGroup 6 dckafka1,dckafka2,dckafka3
```


