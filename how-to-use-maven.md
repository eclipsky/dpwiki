### maven repository 和 mirror的整体架构
![image](/uploads/b8521b8062d62a24604054713b822ac8/image.png)

### repository 和 mirror的关系和区别

1.  repository是仓库的意思，maven的jar都是从repository中下载过来，仓库分为local repository（本地仓库，默认.m2）和remote repository（远程仓库），下载jar时先从local repository中获取，没有的话尝试从remote repository获取
2.  maven有自带的默认remote repository，默认情况下所有的jar最初都是从这个默认的remote repository下载到local repository，下面是默认的仓库
`
<repository>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <id>central</id>
      <name>Central Repository</name>
      <url>http://repo.maven.apache.org/maven2</url>
 </repository>
`
3.  我们自己的jar包一般放在公司自己的maven私服上面，也就是公司自己的remote repository，如果我们的工程需要引用自己的jar包，那么需要在settings.xml(maven的安装目录下的conf目录中)或者工程的pom.xml中添加公司自己对应的remote repository，这样maven才知道从这里下载公司的jar包，例如这样：   
`  <repositories>
    ...
    <repository>
      <id>dc-central</id>
      <name>dc-central</name>
      <url>http://maven.dev.dc:7000/nexus/content/repositories/dc-central</url>
    </repository>
    ...
  </repositories>
`
添加到settings.xml中会对所有工程起效，放在pom.xml只会影响pom所在的工程本身

4.所以***repository解决的是去哪下载jar的问题***

### 那mirror又是什么东东？！

1.  ***mirror是为了解决下载jar的速度问题***，由于直接通过remote repository远程下载jar速度可能比较慢（更何况国内这操蛋的网络环境），所以我们可以通过mirror对remote repository做一个镜像（也就是备份），这样maven去下载远程repository中jar包时，会直接从我们的mirror中去下载，这样就会快许多。我们自己的maven私服会提供mirror功能。
2.  mirror如何配置和生效。mirror配置需要指定该mirror是针对哪个remote repository的，这样访问该remote repository时，该mirror才能拦截该请求。需要注意mirror只能配置在settings.xml中，也就是mirror的配置是全局性的，会影响所有的maven工程。mirror的配置类似:
`
  <mirrors>
      <mirror>
       <!-- mirror 的id -->
        <id>Central-Mirror</id>
       <!-- 指明对哪个remote repository进行mirror，记得maven默认remote repository的id就是center吗？-->
        <mirrorOf>center</mirrorOf>
       <!-- 该mirror的描述 -->
        <name>dataeye center mirror</name>
       <!-- 该mirror的地址，这个地址需要填对，因为我们的maven私服可能mirror了多个不同的remote repository-->
        <url>http://maven.dev.dc:7000/nexus/content/groups/public/</url>
      </mirror>
  </mirrors>
`  
这样当我们去请求maven默认的remote repository时，会直接访问我们的mirror对应的地址，这样速度就更快了。

###  如果对应的mirror挂掉了或者没有我们需要的jar包，情况会怎样？

1.  如果对应的mirror挂了，maven会提示下载失败，并且***不会尝试从原始的remote repository中去下载jar包***

2. 如果对应的mirror中没有需要的jar包，如果还有其他的remote repository话，maven会尝试从其他的remote repository中下载jar包；否则，会提示下载失败， 并且***不会尝试从原始的remote repository中去下载jar包***

所以mirror的这种特性，决定了必须保证mirror的稳定性和同步性（能及时同步remote repository的内容），否则的话，有时候设置mirror反而会适得其反。***这时可以选择注释mirror配置，从原始的remote repository中下载jar包。***

###  如果通过remote repository下载很慢，mirror又不可用，怎么办？

1.  购买一个vpn代理

2. 在settings.xml配置通过代理访问，类似这样:
`
  <proxies>
      <proxy>
        <id>LightSS</id>
        <active>true</active>
        <protocol>http</protocol>
        <username>proxyuser</username>
        <password>proxypass</password>
        <host>127.0.0.1</host>
        <port>1080</port>
        <!-- 指定那些host或域名不通过代理访问-->
        <nonProxyHosts>local.net|some.host.com</nonProxyHosts>
     </proxy>
  </proxies>  
`