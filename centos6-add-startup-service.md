该文档描述如何在centos6下面添加开机服务，例如开机启动supervisor服务，然后supervisor会负责拉起配置的进程，从而实现各种服务的开机重启

### 原理概述

在centos6下面主要通过在/etc/init.d/下面添加服务脚本和chkconfig命令来完成添加启动服务    

*   **添加服务的启停脚本**   
如果需要开机启动服务，或者通过service命令控制服务，必须在/etc/init.d/下面有对应服务的启停脚本，如果我们的服务为supervisor，那么在/etc/init.d/下面对应的启停脚本就是supervisor，且具有执行权限。启停脚本个格式一般都是固定的，比如包括start、stop等函数，来用来对服务进行启动/关闭

一个例子（取自hadoop-robot中的supervisor的开机服务做了简化，完整代码见这里 http://10.1.2.222/dedp/data-robot/raw/master/supervisor/conf/supervisor_init.sh  ）    
```bash
#!/bin/bash
#
# supervisord   This scripts turns supervisord on
#
# chkconfig:    345 83 04
#
# description:  supervisor is a process control utility.  It has a web based
#               xmlrpc interface as well as a few other nifty features.
#               Script was originally written by Jason Koppe <jkoppe@indeed.com>.
#
# usage：rename this script to 'supervisor' and put it to directory /etc/init.d/

# source function library
. /etc/rc.d/init.d/functions

# service strips all environment variables but TERM, PATH and LANG,
# so we need export those variables explicitly.
# see http://unix.stackexchange.com/questions/44370/how-to-make-unix-service-see-environment-variables
export HOSTNAME
export USER=hadoop2

# hadoop's environment variables
. /home/hadoop2/.bashrc

SUPERVISORD=/usr/bin/supervisord
SUPERVISORCTL=/usr/bin/supervisorctl

PIDFILE=/tmp/supervisord.pid
LOCKFILE=/tmp/supervisord

OPTIONS="-c /usr/local/supervisor/supervisord.conf"

# unset this variable if you don't care to wait for child processes to shutdown before removing the $LOCKFILE-lock
WAIT_FOR_SUBPROCESSES=yes

# remove this if you manage number of open files in some other fashion
ulimit -n 96000

RETVAL=0

running_pid()
{
...
}

running()
{
...
}

is_root()
{
...
}

start() 
{
...
}

stop()
 {
...
}

restart() {
        stop
        start
}

case "$1" in
    start)
        start
        RETVAL=$?
        ;;
    stop)
        stop
        RETVAL=$?
        ;;
    restart|force-reload)
        restart
        RETVAL=$?
        ;;
    reload)
        $SUPERVISORCTL $OPTIONS reload
        RETVAL=$?
        ;;
    condrestart)
        [ -f $LOCKFILE ] && restart
        RETVAL=$?
        ;;
    status)
        $SUPERVISORCTL $OPTIONS status
        if running ; then
            RETVAL=0
        else
            RETVAL=1
        fi
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|reload|force-reload|condrestart}"
        exit 1
esac

exit $RETVAL
```
所有的service服务的启停脚本都是类似的格式，可根据需要填充具体内容
* **使用chkconfig添加service**

```bash  
# 添加服务   
chkconfig --add supervisor    

# 删除服务   
chkconfig --del supervisor   

#列出所有服务    
chkconfig --list   
```
* **使用**   

```bash   
service supervisor start

service supervisor stop
```

### 一些坑

* **开机启动的用户身份是root，可我们需要是其他用户身份，比如hadoop启动服务**    
使用su user -c "cmd_here" 来执行服务启动，可以用你希望的用户身份启动服务
* **环境变量读取不到，例如JAVA_HOME等,造成服务启动不成功**  
这主要是service系统在执行时，会strip掉除TERM、PATH等以外所以环境变量。解决的办法是在启停服务脚本中export需要的环境变量    
* **添加或启动service时，提示No such file or directory**  
可能是你在windows下面编辑文件，然后上传到linux，因为windows的许多编辑器会在末尾多加换行。详细解决方案，看这里     
https://confluence.atlassian.com/kb/starting-service-on-linux-throws-a-no-such-file-or-directory-error-794203722.html

### 进一步了解   
查看http://10.1.2.222/dedp/data-robot/raw/master/supervisor/conf/supervisor_init.sh  了解所以细节
