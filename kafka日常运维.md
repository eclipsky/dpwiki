kafka日常运维   
----------------------------------------
### 1、组件部署情况      

| 机器        | IP   |  组件  |
| --------   | :-----:  | :----:  |
| dckafka1  |192.168.1.150 |   Kafka  |
| dckafka2  |192.168.1.151 |   Kafka  |
| dckafka3  |192.168.1.152 |   Kafka  |

### 2、监控脚本   
每台机器的用户home目录下都有对应的kafka进程监控文件 ：
```
 /home/hadoop/monitoring/monitoring_kafka.sh   
```
用于监控kafka进程是否存活，如果不存活就拉起，并发送告警
### 3、日常运维脚本   
进入$KAFKA_HOME目录，即可执行以下这些命令
```
// 创建topic
./bin/kafka-topics.sh --zookeeper dckafka1:12181,dckafka2:12181,dckafka3:12181/kafka --create  --topic topicName  --replication-factor 1 --partitions 1

//删除topic
kafka-run-class.sh kafka.admin.DeleteTopicCommand --topic topicName --zookeeper dckafka1:12181,dckafka2:12181,dckafka3:12181/kafka

//查看所有topic
./bin/kafka-topics.sh --zookeeper dckafka1:12181,dckafka2:12181,dckafka3:12181/kafka --list

//查看具体topic的信息
./bin/kafka-topics.sh -zookeeper dckafka1:12181,dckafka2:12181,dckafka3:12181/kafka -describe -topic topicName

//生产者
./bin/kafka-console-producer.sh --broker-list dckafka1:9092,dckafka2:9092,dckafka3:9092 --topic topicName

//消费者
./bin/kafka-console-consumer.sh --zookeeper dckafka1:12181,dckafka2:12181,dckafka3:12181/kafka --topic topicName
```
### 4、关键文件目录   
**日志文件：/home/hadoop/kafka/logs**   
**数据文件：/data2/kafka/data, /data3/kafka/data**   
### 5、kafka在zookeeper中的存储结构   
![image](/uploads/324ba949d8cab24cbe221914e384ee71/image.png)   
### 6、kafka集群重启步骤   
问题提出：   **kafka主进程下的controller进程(负责选择leader)由于网络波动会出现异常**   
当出现以下情况时，需要重新启动kafka集群：   
```
(1)kafka所产生的日志出现暴涨   
(2)kafka新创建的topic没有leader(使用describe命令可以观察到)   
(3)kafka老的topic中，有多个副本的topic，只有一个可用   
```
重启步骤   
```
(1)把所有jstorm上的实时任务都冻结掉(执行jstorm2@dctest1:/home/jstorm2下的deactiveAllTopology.sh脚本)   
(2)进入每台kafka机器的$KAFKA_HOME目录，执行关闭命令：./bin/kafka-server-stop.sh   
(3)使用jps命令查看kafka进程是否已经被关闭(大概10秒左右)
(4)重新启动kafka：./startKafkaServer.sh
(5)把jstorm上的任务重新激活(执行jstorm2@dctest1:/home/jstorm2下的activeAllTopology.sh脚本)   
```
### 7、kafka生产者消费者的客户端实例代码
> https://github.com/huangyanxiong/de-kafka


