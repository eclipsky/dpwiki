# Hive元数据库升级  
#### 背景说明
新版本的hive提供了更多的新特性，为了使用hive on spark，需要对旧版本进行升级   
旧版本 hive-1.1.0-cdh5.5.0  
新版本 apache-hive-2.2.0-SNAPSHOT-bin  
*注意：cdh版本的hive和Apache社区版hive的区别，目前不确定cdh是否对Apache版的hive元数据库做了修改，所以即使同一个版本号它们的metastore可能也是不同的，这里的升级可以假设他们是相同的，因为已经在de环境上面测试过是没有问题的*   

#### 升级步骤
1. 确定spark环境可用（spark安装参考wiki文档，版本为spark-2.0.0-bin-hadoop2.6）
2. 关闭调度器，此处为了防止hive任务运行导致元数据库发生变化，进而导致备份数据前后不一致无法完整恢复
3. 停掉旧版本的hiveserver2和metastore进程
4. 链接到mysql备份旧版本元数据库，包括表结构和表数据
5. unlink hive解除软链hive-1.1.0-cdh5.5.0
6. 建立软链到新版本ln -s apache-hive-2.2.0-SNAPSHOT-bin hive
7. 将jdbc驱动程序包$HIVE_HOME/jdbc/mysql-connector-java-5.1.36.jar包拷贝到$HIVE_HIVE/lib目录下
8. 修改$HIVE_HOME/conf/hive-site.xml文件，设置hive.metastore.schema.verification为false，开启强制验证metastore的schema版本一致性，默认为true
9. 初始化metastore schema并查看是否升级到对应的版本  
   schematool -dbType mysql –initSchema  
   schematool -dbType mysql –info   
如下截图参考
![image](/uploads/1ee7a5158cb9782f107baadfbf669a50/image.png)



#### 验证数据是否完整
1. 启动hiveserver2和metastore
2. 执行hivesql进行验证

