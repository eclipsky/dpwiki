在一些情况下我们希望能本地运行map-reduce，比如快速测试程序功能，debug程序等。该wiki阐述如何本地运行map-reduce任务，注意目前仅针对linux/mac系统，windows上面有一些坑，看后续踩完了，再更新。

#### 前提条件

*  本机已经安装了hadoop，如何本地安装hadoop请自行google

#### 本地运行的两种模式

1.  idea/eclipse中通过main函数

1.  通过hadoop jar命令的方式

#### 如何运行
两种方式的解决方法基本都一样，都是通过hadoop的-D参数，把本地运行的控制参数设置到hadoop的Configuration中，只有细微不同。
<br>
<br/>
先说hadoop jar的方式
```
hadoop jar your-jar-path your-main-class -Dmapreduce.framework.name=local -Dfs.default.name=file:/// -Dother.config.key=value input-arg output-arg
```
可以看到就是通过这两参数控制即可，-Dmapreduce.framework.name=local -Dfs.default.name=file:///  ，参数的意义也很明确
<br/>
<br/>
那如何在idea/eclipse中通过main函数debug本地map-reduce任务，这里以idea为例子（eclipse类似），简单说就是设置运行时的传入参数，
![image](/uploads/b06e3338b221bec571d009575b841575/image.png)
<br/>
<br/>
可以看到只需要设置程序的传入参数即可，我们甚至都不用设置-Dmapreduce.framework.name=local -Dfs.default.name=file:///  这个两个参数。为什么内，因为通过ide的main函数run/debug任务时，只会读取hadoop自身jar中的默认配置文件，不会读取hadoop安装路径下面的配置文件信息，而默认的配置信息中这两个参数值已经是本地运行模式的值了

#### 还存在的问题

1.  windows下面还存在一些坑，主要是hadoop用到了许多native的功能，在windows下面这需要针对每个hadoop版本单独编译，比较麻烦。