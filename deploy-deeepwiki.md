本文档主要详细说明如何部署deepwiki系统。deepwiki是一个基于markdown的wiki系统。

#### 前提条件
安装php和php-fpm
```
sudo yum install php php-fpm
```
安装nginx
```
sudo yum install nginx
```
#### 安装步骤


*   从下面地址下载deepwiki，并解压到指定目录，这里解压目录选择/usr/share/nginx/wiki
```
https://github.com/ychongsaytc/deepwiki/releases
```
解压之后目录如下图所示：
![image](/uploads/eb0258c461902a106f49d0b7320eb05c/image.png)

*  配置nginx
```
vim /etc/nginx/conf.d/default.conf
```
![image](/uploads/b1517dff3feb2fcd287399dd23ad8800/image.png)

*  配置php和php-fpm
配置php和php-fpm使其输出错误日志，便于调试

```
vi /etc/php.ini 
[PHP]
log_errors = On
error_log = "/var/log/php-fpm/fpm-error.log"
error_reporting=E_ALL&~E_NOTICE
```

```
vim /etc/php-fpm.conf

[global]
error_log = /var/log/php-fpm/fpm-error.log
[www]
catch_workers_output = yes
```
*  修改deepwiki的log设置
deepwiki默认是关闭错误输出的，需要将其打开
```
vim /usr/share/nginx/wiki/index.php
```
![image](/uploads/841c049e2c1755284475fd57516bd6c2/image.png)

*  启动nginx和php-pfm
```
sudo nginx
sudo php-pfm
```

#### 可能的问题

1.  PHP Fatal error:  Call to undefined function mb_strlen()

这是由于php-mbstring模块默认没有安装，安装即可。
```
yum install php-mbstring
```

#### 有用的链接
[关于nginx配置 获取静态资源](https://segmentfault.com/q/1010000005687627)   
[关于Nginx下开启php-fpm 输出php错误日志的设置](http://www.pooy.net/nginx-open-php-fpm-error-log.html)   
https://www.sitepoint.com/setting-up-php-behind-nginx-with-fastcgi/    
https://www.nginx.com/resources/wiki/start/topics/examples/phpfcgi/    
