kylin slow query sql 的监控改造方案
======================





### 背景

在kylin的cube的 segment 被 build、refresh、merge后，将通过ODBC、JDBC、RESTful API提供亚秒级查询服务；尽管如此，在生产环境中，需要记录执行结果很慢的sql，并发出告警，以便分析、优化。


1.slow query sql 的定性
-------------------

在 kylin代码中，类BadQueryDetector作为一个后台线程，不断去发现 slow query sql:


```
  private void detectBadQuery() {
        long now = System.currentTimeMillis();
        ArrayList<Entry> entries = new ArrayList<Entry>(runningQueries.values());
        Collections.sort(entries);
        // report if query running long
        for (Entry e : entries) {
            float runningSec = (float) (now - e.startTime) / 1000;
            if (runningSec >= alertRunningSec) {
                notify("Slow", runningSec, e.startTime, e.sqlRequest.getProject(), e.sqlRequest.getSql(), e.thread);
                dumpStackTrace(e.thread);
            } else {
                break; // entries are sorted by startTime
            }
        }

        // report if low memory
        if (getSystemAvailMB() < alertMB) {
            logger.info("System free memory less than " + alertMB + " MB. " + entries.size() + " queries running.");
        }
    }
```
当执行时间超过 **alertRunningSec**会被定性为慢查询，**alertRunningSec**的定义如下：


```
  public int getBadQueryDefaultAlertingSeconds() {
        return Integer.parseInt(getOptional("kylin.query.badquery.alerting.seconds", "90"));
    }
```

默认为90秒，因此，假如要自定义慢查询的时间，可在kylin的配置文件: **kylin.properties** 中修改该参数：
```
kylin.query.badquery.alerting.seconds=5
```

2.BadQueryDetector 的观察者模式
-------------------
类**BadQueryDetector**中 ,定义了一个接口       
```
public interface Notifier {
        void badQueryFound(String adj, float runningSec, long startTime, String project, String sql, Thread t);
    }

```
只要实现该接口，并注册给**BadQueryDetector**的监听器 ,当其检测到慢查询时，被实现的接口就会被通知，如：可以利用该接口实现检测到 slow query sql 后，发告警邮件。

```
public class  EmailNotifier  implements Notifier  {
        void badQueryFound(String adj, float runningSec, long startTime, String project, String sql, Thread t){

                  //send a email;
         }
    }
```

```
public void registerNotifier(Notifier notifier) {
        notifiers.add(notifier);
    }
```

类**BadQueryDetector**注册了两个自定义的监听器如下：

```
private void initNotifiers() {
        this.notifiers.add(new LoggerNotifier());
        if (kylinConfig.getBadQueryPersistentEnabled()) {
            this.notifiers.add(new PersistenceNotifier());
        }
    }
```

可把slow query  sql 记录到日志并存储到hbase。


3 .slow query sql 在hbase中的位置：
-------------------
```
     1)表 : kylin_metadata 

     2)rowKey : /bad_query/${project_name}.json // 用UTF-8 编码
 
     3) column: f:c  // content: slow sql的具体内容,需先用UTF-8解码，然后用 JsonSerializer 反序列化
     
        column: f:t  //timestamp  : 更新该条记录的 时间戳

  ```






