### hadoop2.0集群的日常运维

1 . 清理app统计分析和ga2.0的数据，释放空间   

```bash
// 清理app统计的中间结果数据，脚本部署在dchadoop206，/home/hadoop2/monitor-shell   
python  hadoop-clean-tool.py hadoop-clean-tool.conf > hadoop-clean-tool.log   

// 清理ga2.0的中间结果数据，，脚本部署在dchadoop206，/home/hadoop2/monitor-shell    
python  hadoop-clean-tool.py ga2-clean-tool.conf '%Y%m%d' > ga2-clean-tool.log   

上述脚本会把清理的数据放到trash中，确认无误后，可以清理trash。conf文件的每行格式是：清理的目录   保存最近多少天

具体代码见 http://10.1.2.222/dedp/data-robot/blob/master/hadoop/hadoop-clean-tool.py

// hadoop2.0目录/de-hdfs/data/logserver/chlog2的日志会被定时压缩，压缩之后原始数据可以删除，注意只能删除
// 已近成功压缩的原始数据！！！默认是压缩3个月前的数据，所以删除只能删除3个月前的数据。比如现在是2017-03-23
// 那你只能删除2016-12-23之前的数据，删之前确认一下。
```

2 . hadoop2.0定时的日志清理任务     
```
//两台主节点上部署了，如下的清理本地log日志的脚本
*/5 * * * * python /home/hadoop2/monitor-shell/clean-robot.py -d /data0 -r /data0/hadoop2/logs -u 60\% -f 2g -p log -t 7d >> /data0/hadoop2/logs/cleanup_logs.log 2>&1

具体代码见 http://10.1.2.222/dedp/data-robot/blob/master/robot/clean_robot.py ，有详细说明
```

3 . 主要节点的磁盘监控程序
```
// 在hadoop2.0和hadoop1.0的主节点部署了如下磁盘监控程序
*/5 * * * * python /home/hadoop2/monitor-shell/disk_monitor.py /home/hadoop2/monitor-shell/disk_monitor.conf  >> /data0/hadoop2/logs/disk_monitor.log 2>&1

具体代码见 http://10.1.2.222/dedp/data-robot/blob/master/robot/disk_monitor.py ，代码中有详细的说明
```

4 . 新的进程监控程序
```
//之前的进程监控脚本，太冗余，写了一条新的监控监本，使用方法如下

*/2 * * * * /home/hadoop/monitoring/monitoring_hbase_regionserver.sh >> /tmp/restart-hbase-regionserver.log 2>&1

代码详见  http://10.1.2.222/dedp/data-robot/tree/master/hadoop/monitoring ，代码中有详细说明
```

5 . hbase1.0的ganglia数据采集
```
// hbase0.94的ganglia的数据采集采用logstash的方法进行，原理就是hbase将metric数据写到本地文件，logstash tail本地文件并push到ganglia中
// 启动命令如下

/home/hadoop/logstash-2.1.1/bin/logstash agent -f conf/hbase-metric1-ganglia.conf

具体代码见 http://10.1.2.222/dedp/data-robot/blob/master/elk/logstash/hbase-metric1-ganglia.conf，逻辑很简单，需要一些ruby语法

//本地文件的目录是
/data0/hbase/metric/，有三个不同文件metrics_hbase.log  metrics_jvm.log  metrics_rpc.log，针对不同的指标，同时又定时任务清理日志

0 23 * * * python /home/hadoop/dctools/clean-robot.py -d /data0 -r /data0/hbase/metric/ -u 1\% -f 2m -p log -t 7d >> /data0/hbase/logs/cleanup_metric.log 2>&1
```

6 . 对warehouse任务进行监控的脚本
```
//该脚本部署在dchadoop206 ，对warehouse的任务进行监控，运行超过5小时的任务会被kill。防止异常任务影响集群。

*/10 * * * *  python /home/hadoop2/monitor-shell/monitoring_warehouse_application.py >> /tmp/monitoring_warehouse_application.log 2>&1

具体代码见 http://10.1.2.222/dedp/data-robot/blob/master/hadoop/monitoring_warehouse_application.py ，代码中有详细说明
```