### 背景
hadoop2系统需要将一台journal node迁移到另一台负载较低的机器上

### 方案
先添加新的journal node，然后再移除旧的journal node节点

### 操作步骤
1.  新机器准备hadoop环境，如果已经有hadoop环境，跳到2。
2. 创建journal node的edits log目录，参见hdfs-site.xml的dfs.journalnode.edits.dir设置：
```
mkdir -p target_edits_log_path
```
3. 将线上其他的journal node的dfs.journalnode.edits.dir目录内容同步一份到新的节点（这一步是必须的，相当于初始化edits log目录）
```
 scp -r journal/* new_host:/data0/hadoop2/hdfs/journal
```
3. 修改active namenode 和 standby namenode的dfs.namenode.shared.edits.dir配置，增加新的journal node的地址
4. 启动新机器上的journal node
```
sbin/hadoop-daemon.sh start journalnode
```
5. 重启standby namenode节点
6. 重启active namenode节点
7.  观察系统是否正常（name node是否正常，journal nodes的edits log同步是否正常）。观察一段时间，如果需要摘除就节点，继续8
8. 修改active namenode 和 standby namenode的dfs.namenode.shared.edits.dir配置，去掉要摘除的journal node的地址
9. 重启standby namenode节点
10. 重启active namenode节点


### 操作影响
无，namenode ha会自动完成切换

### 注意事项
新的journode节点上要部署监控脚本  

### 参考资源
http://zdatainc.com/2015/09/add-journalnode-to-ambari-managed-hadoop-cluster/