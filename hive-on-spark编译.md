hive源码编译与hive on spark环境搭建
======
**背景**   
hive2已经将执行引擎建议设置为spark，并在后续的版本中可能会将spark设置为默认执行引擎。相比于MR，spark拥有更好的性能表现。由于官网提供的spark版本支持的hive最新版本为hive1.2，为了使用hive2这里需要重新编译。

**一、编译环境准备**   
从官网下载最新版本并按照官网介绍进行安装
1. apache-maven-3.3.9
2. jdk1.7
3. spark-2.0.0-bin-hadoop2.6
3. hive源码下载```git clone https://git-wip-us.apache.org/repos/asf/hive.git```   

**二、修改配置文件**
1. 解压下载的hive源码包hive-master，并进入目录hive-master
2. 编辑pom.xml文件，找到```<spark.version>1.6.0</spark.version>```标签对，将```1.6.0```修改为```2.0.0```，注意此处必须和安装的spark版本保持一致

**三、hive源码编译**   
直接执行```mvn clean package -Dmaven.test.skip=true -Pdist```进行编译，
中间可能会出现错误，比如网络问题导致的某些依赖包下载失败，这种情况只需要重复执行编译命令即可。如果本来已经下载了依赖包，可以执行离线编译```mvn -o clean package -Dmaven.test.skip=true -Pdist```

**四、搭建hive on spark环境**
1. 将编译生成的hive安装包```apache-hive-2.2.0-SNAPSHOT-bin.tar.gz```解压后，配置```$HIVE_HOME```，并进入```$HIVE_HOME/conf```目录，创建hive-site.xml文件，添加配置项指定执行引擎为spark
```
<property>
    <name>hive.execution.engine</name>
    <value>spark</value>
</property>
```
并其他添加必要的配置（具体配置内容请参考已有hive环境即可）
2. 拷贝conf/hive-env.sh.template并重命名conf/hive-env.sh，编辑该文件添加如下内容：```export SPARK_HOME=/usr/local/spark```
3. 编辑bin/hive文件，添加如下内容![image](/uploads/9f6897236bbe2aeb81d615b17a2aee5c/image.png)

4. 进入```$SPARK_HOME/jars```目录，将所有hive开头的jar包删除![image](/uploads/e2f5532e98dc1f8283e04105c2969c54/image.png)


注意：此处是spark编译的时候指定了-Phive参数，这样编译后的spark即支持hive sql，所以上述hive相关的jar包用来支持hive1.2。这里不再重新编译spark，直接删除这些hive包即可，如果不删除，在执行hive2的时候将会报错。

**五、运行hive2 on spark**
1. 进入hive cli并执行set yarn.timeline-service.enabled=false;否则将会报错，如下截屏![image](/uploads/1dc037a9e4a6b881dbddcdec65434439/image.png)

2. 执行hive sql查看测试结果![image](/uploads/f010d5beb11124b25cf26f4e06cd3d80/image.png)

3. 查看hiveserver2 WEB UI, http://demain1:10002/![image](/uploads/20872e2a80adf0a28a4a846aaf3b00f7/image.png)

