一、备注
```

hander：gaungsheng      Summary document：qingyun
```

一、知识点补充：
```
在客户端来看，SSH提供两种级别的安全验证
第一种级别（基于密码的安全验证），知道帐号和密码，就可以登录到远程主机，并且所有传输的数据都会被加密。
第二种级别（基于密钥的安全验证），需要依靠密钥，也就是你必须为自己创建一对密钥，并把公有密钥放在需要访问的服务器上。
```

二、问题描述
`集群一台主节点master,用到第二种级别，运行的用户是hadoop2，现在配置了公钥登录后，ssh连接还是显示输入密码。`

三、问题呈现
1、生成免密钥对
```
[hadoop2@master ~]$ ssh-keygen -t rsa -P "" -f ~/.ssh/id_rsa
Generating public/private rsa key pair.
Created directory '/home/hadoop2/.ssh'.
Your identification has been saved in /home/hadoop2/.ssh/id_rsa.
Your public key has been saved in /home/hadoop2/.ssh/id_rsa.pub.
The key fingerprint is:
79:f8:aa:87:bd:de:22:bd:30:89:69:fa:5b:73:5b:bf hadoop2@master
The key's randomart image is:
+--[ RSA 2048]----+
|                 |
|                 |
|                 |
|         o       |
|        S .      |
|     o . o       |
|    + *+. o      |
|   o .o=== .     |
|  ..o..=*+. E.   |
+-----------------+
```

2、复制公钥，10.1.9.214也就是本机master
```
[hadoop2@master root]$ ssh-copy-id -i ~/.ssh/id_rsa.pub 10.1.9.214
hadoop2@10.1.9.214's password: 
Now try logging into the machine, with "ssh '10.1.9.214'", and check in:
  .ssh/authorized_keys
to make sure we haven't added extra keys that you weren't expecting.
```

3、验证，还是提示密码输入
```
[hadoop2@master root]$ ssh master
The authenticity of host 'master (10.1.9.214)' can't be established.
RSA key fingerprint is f3:80:84:4f:7d:7c:97:74:57:09:35:8e:aa:b9:f2:06.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'master' (RSA) to the list of known hosts.
hadoop2@master's password:
```


四、排查
1、调出debug模式
```
[hadoop2@master ~]$ ssh -v master
OpenSSH_5.3p1, OpenSSL 1.0.1e-fips 11 Feb 2013
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: Applying options for *
debug1: Connecting to master [10.1.9.214] port 22.
debug1: Connection established.
debug1: identity file /home/hadoop2/.ssh/identity type -1
debug1: identity file /home/hadoop2/.ssh/identity-cert type -1
debug1: identity file /home/hadoop2/.ssh/id_rsa type 1
debug1: identity file /home/hadoop2/.ssh/id_rsa-cert type -1
debug1: identity file /home/hadoop2/.ssh/id_dsa type -1
debug1: identity file /home/hadoop2/.ssh/id_dsa-cert type -1
debug1: identity file /home/hadoop2/.ssh/id_ecdsa type -1
debug1: identity file /home/hadoop2/.ssh/id_ecdsa-cert type -1
debug1: Remote protocol version 2.0, remote software version OpenSSH_5.3
debug1: match: OpenSSH_5.3 pat OpenSSH*
debug1: Enabling compatibility mode for protocol 2.0
debug1: Local version string SSH-2.0-OpenSSH_5.3
debug1: SSH2_MSG_KEXINIT sent
debug1: SSH2_MSG_KEXINIT received
debug1: kex: server->client aes128-ctr hmac-md5 none
debug1: kex: client->server aes128-ctr hmac-md5 none
debug1: SSH2_MSG_KEX_DH_GEX_REQUEST(1024<1024<8192) sent
debug1: expecting SSH2_MSG_KEX_DH_GEX_GROUP
debug1: SSH2_MSG_KEX_DH_GEX_INIT sent
debug1: expecting SSH2_MSG_KEX_DH_GEX_REPLY
debug1: Host 'master' is known and matches the RSA host key.
debug1: Found key in /home/hadoop2/.ssh/known_hosts:2
debug1: ssh_rsa_verify: signature correct
debug1: SSH2_MSG_NEWKEYS sent
debug1: expecting SSH2_MSG_NEWKEYS
debug1: SSH2_MSG_NEWKEYS received
debug1: SSH2_MSG_SERVICE_REQUEST sent
debug1: SSH2_MSG_SERVICE_ACCEPT received
debug1: Authentications that can continue: publickey,password
debug1: Next authentication method: publickey
debug1: Trying private key: /home/hadoop2/.ssh/identity
debug1: Offering public key: /home/hadoop2/.ssh/id_rsa
debug1: Authentications that can continue: publickey,password
debug1: Trying private key: /home/hadoop2/.ssh/id_dsa
debug1: Trying private key: /home/hadoop2/.ssh/id_ecdsa
debug1: Next authentication method: password
hadoop2@master's password: 
debug1: Authentication succeeded (password).
debug1: channel 0: new [client-session]
debug1: Requesting no-more-sessions@openssh.com
debug1: Entering interactive session.
debug1: Sending environment.
debug1: Sending env LANG = zh_CN.UTF-8
Last login: Fri Nov 18 16:14:57 2016 from 10.1.9.214
```
2、切换root，查看sshd_config配置文件，没发现异常
```

#       $OpenBSD: sshd_config,v 1.80 2008/07/02 02:24:18 djm Exp $
 
# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.
 
# This sshd was compiled with PATH=/usr/local/bin:/bin:/usr/bin
 
# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options change a
# default value.
 
#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
 
# Disable legacy (protocol version 1) support in the server for new
# installations. In future the default will change to require explicit
# activation of protocol 1
Protocol 2
 
# HostKey for protocol version 1
#HostKey /etc/ssh/ssh_host_key
# HostKeys for protocol version 2
#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_dsa_key
 
# Lifetime and size of ephemeral version 1 server key
#KeyRegenerationInterval 1h
#ServerKeyBits 1024
 
# Logging
# obsoletes QuietMode and FascistLogging
# This sshd was compiled with PATH=/usr/local/bin:/bin:/usr/bin
 
# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options change a
# default value.
 
#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
 
# Disable legacy (protocol version 1) support in the server for new
# installations. In future the default will change to require explicit
# activation of protocol 1
Protocol 2
 
# HostKey for protocol version 1
#HostKey /etc/ssh/ssh_host_key
# HostKeys for protocol version 2
#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_dsa_key
 
# Lifetime and size of ephemeral version 1 server key
#KeyRegenerationInterval 1h
#ServerKeyBits 1024
 
# Logging
# obsoletes QuietMode and FascistLogging
#SyslogFacility AUTH
SyslogFacility AUTHPRIV
#LogLevel INFO
 
# Authentication:
 
#LoginGraceTime 2m
#PermitRootLogin yes
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10
 
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile      .ssh/authorized_keys
AuthorizedKeysCommand none
AuthorizedKeysCommandRunAs nobody
 
# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#RhostsRSAAuthentication no
# similar for protocol version 2
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# RhostsRSAAuthentication and HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes
 
# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no
PasswordAuthentication yes
 
# Change to no to disable s/key passwords
#ChallengeResponseAuthentication yes
ChallengeResponseAuthentication no
 
# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes
 
# GSSAPI options
#GSSAPIAuthentication no
GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
 
# Set this to 'yes' to enable PAM authentication, account processing, 
# and session processing. If this is enabled, PAM authentication will 
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
#UsePAM no
UsePAM yes
 
# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS
 
#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
#X11Forwarding no
X11Forwarding yes
#X11DisplayOffset 10
#X11UseLocalhost yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#UseLogin no
#UsePrivilegeSeparation yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#ShowPatchLevel no
UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
 
# no default banner path
#Banner none
 
# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server
 
# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       ForceCommand cvs server
```

5、查看权限，也正常
```
[hadoop2@master ~]$ ls ~/.ssh/ -ld
drwx------ 2 hadoop2 hadoop2 4096 11月 18 16:13 /home/hadoop2/.ssh/
[hadoop2@master ~]$ ls ~/.ssh/ -l
总用量 16
-rw------- 1 hadoop2 hadoop2  396 11月 18 16:13 authorized_keys
-rw------- 1 hadoop2 hadoop2 1675 11月 18 16:12 id_rsa
-rw-r--r-- 1 hadoop2 hadoop2  396 11月 18 16:12 id_rsa.pub
-rw-r--r-- 1 hadoop2 hadoop2  780 11月 18 16:14 known_hosts
```

6、man一下sshd 帮助信息
   6.1 可以看到-d选项是调试模式
          ```
 -d      Debug mode.  The server sends verbose debug output to the system log, and does
             not put itself in the background.  The server also will not fork and will only
             process one connection.  This option is only intended for debugging for the
             server.  Multiple -d options increase the debugging level.  Maximum is 3.
```
   6.2  默认指定22端口，可以指定其他端口
```
 -p port
             Specifies the port on which the server listens for connections (default 22).
             Multiple port options are permitted.  Ports specified in the configuration file
             with the Port option are ignored when a command-line port is specified.  Ports
             specified using the ListenAddress option override command-line ports.
```

五、解决
1、sshd指定一个端口1234启动
```
[hadoop2@master root]$ sudo /usr/sbin/sshd  -d -p 1234
[sudo] password for hadoop2: 
```
hadoop2 不在 sudoers 文件中。此事将被报告。
visudo添加/usr/sbin/sshd权限，即可解决

```
hadoop2 ALL=(ALL) NOPASSWD:/usr/sbin/sshd

```
  重新启动
```
[hadoop2@master root]$ sudo /usr/sbin/sshd  -d -p 1234
debug1: sshd version OpenSSH_5.3p1
debug1: read PEM private key done: type RSA
debug1: private host key: #0 type 1 RSA
debug1: read PEM private key done: type DSA
debug1: private host key: #1 type 2 DSA
debug1: rexec_argv[0]='/usr/sbin/sshd'
debug1: rexec_argv[1]='-d'
debug1: rexec_argv[2]='-p'
debug1: rexec_argv[3]='1234'
Set /proc/self/oom_score_adj from 0 to -1000
debug1: Bind to port 1234 on 0.0.0.0.
Server listening on 0.0.0.0 port 1234.
debug1: Bind to port 1234 on ::.
Server listening on :: port 1234.
```

2、然后打开另一个窗口，连接
```
[hadoop2@master ~]$ ssh -p 1234 master
hadoop2@master's password:
```

3、返回启动服务1234窗口，这时出现debug信息
```
[hadoop2@master root]$ sudo /usr/sbin/sshd  -d -p 1234
debug1: sshd version OpenSSH_5.3p1
debug1: read PEM private key done: type RSA
debug1: private host key: #0 type 1 RSA
debug1: read PEM private key done: type DSA
debug1: private host key: #1 type 2 DSA
debug1: rexec_argv[0]='/usr/sbin/sshd'
debug1: rexec_argv[1]='-d'
debug1: rexec_argv[2]='-p'
debug1: rexec_argv[3]='1234'
Set /proc/self/oom_score_adj from 0 to -1000
debug1: Bind to port 1234 on 0.0.0.0.
Server listening on 0.0.0.0 port 1234.
debug1: Bind to port 1234 on ::.
Server listening on :: port 1234.
debug1: Server will not fork when running in debugging mode.
debug1: rexec start in 5 out 5 newsock 5 pipe -1 sock 8
debug1: inetd sockets after dupping: 3, 3
Connection from 10.1.9.214 port 54822
debug1: Client protocol version 2.0; client software version OpenSSH_5.3
debug1: match: OpenSSH_5.3 pat OpenSSH*
debug1: Enabling compatibility mode for protocol 2.0
debug1: Local version string SSH-2.0-OpenSSH_5.3
debug1: permanently_set_uid: 74/74
debug1: list_hostkey_types: ssh-rsa,ssh-dss
debug1: SSH2_MSG_KEXINIT sent
debug1: SSH2_MSG_KEXINIT received
debug1: kex: client->server aes128-ctr hmac-md5 none
debug1: kex: server->client aes128-ctr hmac-md5 none
debug1: SSH2_MSG_KEX_DH_GEX_REQUEST received
debug1: SSH2_MSG_KEX_DH_GEX_GROUP sent
debug1: expecting SSH2_MSG_KEX_DH_GEX_INIT
debug1: SSH2_MSG_KEX_DH_GEX_REPLY sent
debug1: SSH2_MSG_NEWKEYS sent
debug1: expecting SSH2_MSG_NEWKEYS
debug1: SSH2_MSG_NEWKEYS received
debug1: KEX done
debug1: userauth-request for user hadoop2 service ssh-connection method none
debug1: attempt 0 failures 0
debug1: PAM: initializing for "hadoop2"
debug1: userauth-request for user hadoop2 service ssh-connection method publickey
debug1: attempt 1 failures 0
debug1: test whether pkalg/pkblob are acceptable
debug1: PAM: setting PAM_RHOST to "10.1.9.214"
debug1: PAM: setting PAM_TTY to "ssh"
debug1: temporarily_use_uid: 2532/2533 (e=0/0)
debug1: trying public key file /home/hadoop2/.ssh/authorized_keys
debug1: fd 4 clearing O_NONBLOCK
Authentication refused: bad ownership or modes for directory /home/hadoop2
debug1: restore_uid: 0/0
debug1: temporarily_use_uid: 2532/2533 (e=0/0)
debug1: trying public key file /home/hadoop2/.ssh/authorized_keys
debug1: fd 4 clearing O_NONBLOCK
Authentication refused: bad ownership or modes for directory /home/hadoop2
debug1: restore_uid: 0/0
Failed publickey for hadoop2 from 10.1.9.214 port 54822 ssh2
```
  从debug信息，可以看出Authentication refused: bad ownership or modes for directory /home/hadoop2，显然这个目录权限有问题！    

4、现在去这个目录下验证
```
[hadoop2@master root]$ ll /home/hadoop2 -ld
drwxrwxr-x 4 hadoop2 hadoop 4096 11月 18 16:51 /home/hadoop2
```

```
[hadoop@master root]$ umask
0002

```
额，775目录权限，系统默认umask是0022，即755的权限。   

5、修改回来755

```
[hadoop@master root]$ umask
0002
```
```

[root@master ~]# chmod 755 /home/hadoop2
```

六、最后验证，正常 
```
[hadoop2@master root]$ ssh master
Last login: Fri Nov 18 17:23:20 2016 from 10.1.9.214
[hadoop2@master ~]$
```