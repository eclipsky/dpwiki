### Hive权限与通用优化配置文件说明  
##### 背景   
现网环境任何用户都可以通过hive cli对hdfs上的数据执行几乎不受限的操作，hdfs权限模型又无法满足hive用户的操作需求，在考虑到hive用户操作的潜在风险，研究了hive-1.1.0-cdh5.5.0权限控制相关内容   

##### 配置介绍   

设置为true将以提交hive sql的用户身份执行该sql，否则以启动hiveserver2进程的用户身份执行hive sql，此处请务必设置为true   
```
<property>
        <name>hive.server2.enable.doAs</name>
        <value>true</value>
        <description>
               Setting this property to true will have HiveServer2 execute Hive operations as the user making the calls to it.
        </description>
</property>
```

---
开启storage check，该配置主要是对应于hdfs的粘着位sticky bit，只允许用户删除自己的数据而不允许删除其他用户的数据   
```
<property>
        <name>hive.metastore.authorization.storage.checks</name>
        <value>true</value>
        <description>
            Should the metastore do authorization checks against the underlying storage (usually hdfs)
            for operations like drop-partition (disallow the drop-partition if the user in
            question doesn't have permissions to delete the corresponding directory
            on the storage).
        </description>
</property>
```

---
设置管理员角色为admin_role   
```
<property>
        <name>hive.users.in.admin.role</name>
        <value>admin_role</value>
        <description>
          Comma separated list of users who are in admin role for bootstrapping.
          More users can be added in ADMIN role later.
        </description>
 </property>
```
---
为各个角色分配默认的权限，这些角色可以自定义增加，默认权限也可以自定义，格式参考下面的description说明   
admin_role:ALL    管理员角色默认拥有所有权限   
common_role:select   自定义角色common_role，并分配默认权限select   
super_role:alter,create,update,select,drop   自定义角色super_role，并设置默认权限为alter,create,update,select,drop   
```
<property>
        <name>hive.security.authorization.createtable.role.grants</name>
        <value>admin_role:ALL;common_role:select;super_role:alter,create,update,select,drop</value>
        <description>the privileges automatically granted to some roles whenever a table gets created.
            An example like "roleX,roleY:select;roleZ:create" will grant select privilege to roleX and roleY,
            and grant create privilege to roleZ whenever a new table created.
        </description>
</property>
```

---
设置hive指定用户的默认权限，hadoop2和root用户默认权限为ALL，即所有权限，具体格式参考description标签说明，***注意这里的hadoop2或者root是超级管理员，需要在配置项```hive.semantic.analyzer.hook```中指定的类中明确设置，否则任何普通用户都可以给自己赋予权限进而执行受限的操作***
```
<property>
        <name>hive.security.authorization.createtable.user.grants</name>
        <value>hadoop2,root:ALL</value>
        <description>
        the privileges automatically granted to some users whenever a table gets created.
        An example like "userX,userY:select;userZ:create" will grant select privilege to userX and userY,
        and grant create privilege to userZ whenever a new table created.
        </description>
</property>
```
为指定的组设置默认的权限，这里不再设置
```
<property>
        <name>hive.security.authorization.createtable.group.grants</name>
        <value/>
        <description>
          the privileges automatically granted to some groups whenever a table gets created.
          An example like "groupX,groupY:select;groupZ:create" will grant select privilege to groupX and groupY,
          and grant create privilege to groupZ whenever a new table created.
        </description>
</property>
```
设置对象（表，分区等）的拥有者默认具有所有权限，如：一个用户对其自己创建的表拥有所有权限
```
<property>
        <name>hive.security.authorization.createtable.owner.grants</name>
        <value>ALL</value>
        <description>Enable or disable the hive client authorization</description>
</property>
```

设置如下配置，否则出现错误```Error while compiling statement: FAILED: SemanticException The current builtin authorization in Hive is incomplete and disabled.```
```
<property>
        <name>hive.security.authorization.task.factory</name>
        <value>org.apache.hadoop.hive.ql.parse.authorization.HiveAuthorizationTaskFactoryImpl</value>
        <description>Authorization DDL task factory implementation</description>
</property>
```

设置超级管理员用户实现类，这是hive本身不够完善的地方，需要用户自己实现类并将超级管理员用户hadoop2设置到代码中。   
下面的AuthHook就是自定义类，其jar包部署在$HIVE_HOME/conf/.hiverc指定的路径下面，也可以部署在其他位置只需要被CLASSPATH加载到即可，部署完毕后重启即可。   
***注意 编译的时候需要导入依赖antlr-runtime-3.4.jar，hive-exec-1.1.0-cdh5.5.0.jar***
```
<property>
        <name>hive.semantic.analyzer.hook</name>
        <value>com.dataeye.hive.tools.authorization.AuthHook</value>
</property>
```
![image](/uploads/40bf198c24772522f9ce0fac1c751ae3/image.png)   


指定hive中的配置项不能更改，否则用户可以在自己的session范围内修改某些配置进而可以破坏权限控制，如：设置hive整个权限控制失效
```
<property>
        <name>hive.conf.restricted.list</name>
        <value>hive.security.authenticator.manager,hive.security.authorization.manager,hive.users.in.admin.role,hive.security.authorization.enabled</value>
        <description>Comma separated list of configuration options which are immutable at runtime</description>
</property>
```

完成上述配置后，就可以在hive cli或者beeline下面执行grant, revoke等操作。   
针对权限方面其实并不会很复杂，总结起来可以简单一句话概况：某个用户（具体的hive用户）对某个对象（数据库，表，分区，视图等数据库实体）拥有某种角色或权限（定义上角色就是一组权限的集合）

参考```http://www.zhangrenhua.com/2016/01/31/hadoop-Hive%E8%BF%9B%E9%98%B6-%E8%87%AA%E5%AE%9A%E4%B9%89%E7%94%A8%E6%88%B7%E6%9D%83%E9%99%90%E6%8E%A7%E5%88%B6/```   
 
      
    
### 完整的权限控制和通用的优化配置参考hive-site.xml，这里不再一一说明   
此文件是开发环境的配置，除了host其他配置和现网环境完全一致
[hive-site.xml](/uploads/d5154fbe0d91dfa9237fda8421269c6a/hive-site.xml)
