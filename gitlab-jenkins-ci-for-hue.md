### 背景

现在开发模式很多时候是，本地修改代码，copy到线上，重新编译，然后重启服务，测试。过程很简单，但人工重复工作比较多。考虑使用持续集成优化这一块

### 方案
Gitlab+Jenkins

### 原理
![image](/uploads/def0b152107732aafb5ca194fa7b24c3/image.png)

### 流程

1.  用户提交代码到gitlab

2. gitlab通过web hook来通知Jenkins执行自动构建  

3.  Jenkins的自动构建会调用我们的构建脚本进行构建

4. 构建成功，deploy测试环境  

### 优点

1.  线上的运行环境，始终基于当前代码库，避免线上修改的僵尸代码

2.  整个流程完全自动化，只需要提供构建脚本

### 部署

1.  下载Jenkins，https://jenkins.io/index.html， war包安装即可

2.  运行Jenkins，访问http://10.1.2.217:8080/ （de环境），第一访问会提示安装plugins，可根据需要选择。我们需要git和gitlab相关插件。如果在线安装有问题，可离线安装plugin，请自行google
```bash
 java  -Djava.awt.headless=true -Dhudson.util.ProcessTree.disable=true  -Xmx1024m -XX:MaxPermSize=512m -jar jenkins.war 
```

3.  Jenkins上创建构建job
![image](/uploads/c9e78535399882c8c0ec270e37becc0a/image.png)
选择构建触发器
![image](/uploads/f6017b4ef40c1dacad30d275f0e4775d/image.png)
选择构建过程，这里是执行shell
![image](/uploads/0014e0992234e2ed44d94c1de12c0b86/image.png)
填写具体执行的脚本
![image](/uploads/be7ba6fd5b1c90f40126e6b1e1f6f440/image.png)

4.  在Gitlab上对应project上添加deploy keys，允许Jenkins所在机器从gitlab上拉取代码
![image](/uploads/a7791102bcc33f541b8d48c475cbcaec/image.png)
添加deploy key
![image](/uploads/8da0fe45bdddbd11f2c792d9f0b5f73a/image.png)

5.   Gitlab project设置web hook，在代码变更的时候，触发构建
![image](/uploads/9bf2dcb6163ef63f8d04b4039824c848/image.png)

这样hue中提交新的代码后，将自动触发Jenkins构建。

### 具体的构建脚本例子(build_hue.sh)

```bash
#!/bin/sh

echo "build hue start"

cd ~/git-repo/de-hue

git pull

make apps

hue_exist=`ps -ef | grep python | grep hue | awk '{print $2}'`

if [ "$hue_exist"x != "x" ];then
  echo kill exist hue first
  ps -ef | grep python | grep hue | awk '{print $2}' | xargs kill -9
fi

echo "start hue"
nohup build/env/bin/hue runserver 0.0.0.0:8000 >> logs/runserver.log 2>&1 &

echo "build success!"
```
这个脚本会执行从gitlab pull代码，执行构建，启动新的测试环境

### 最后
更高级的用法，继续补充






