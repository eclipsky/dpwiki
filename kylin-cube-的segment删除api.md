背景
===================

cube每天增量build时，会生产一个segment 和一个hbase表，日积月累后，数据会越积越大，所以需要定时清除过期的segment数据。



解决方案
-------------

官网并没有提供segment删除的rest API,通过 jira issue 跟踪网站查看，kylin1.5.2已经提供了segment删除的API。如下图：


![image](/uploads/0c6c27e1238065cc69460d493123f9d1/image.png)


```
```


手动测试后，发现失败。于是拉取 kylin1.5.2的源代码，查看相关的API，在类**CubeController** 中发现 删除segment 的代码如下：

```

/**
     * Delete a cube segment
     *
     * @throws IOException
     */
    @RequestMapping(value = "/{cubeName}/segs/{segmentName}", method = { RequestMethod.DELETE })
    @ResponseBody
    public CubeInstance deleteSegment(@PathVariable String cubeName, @PathVariable String segmentName) {
        CubeInstance cube = cubeService.getCubeManager().getCube(cubeName);

        if (cube == null) {
            throw new InternalErrorException("Cannot find cube " + cubeName);
        }

        CubeSegment segment = cube.getSegment(segmentName, null);
        if (segment == null) {
            throw new InternalErrorException("Cannot find segment '" + segmentName + "'");
        }

        try {
            return cubeService.deleteSegment(cube, segmentName);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
            throw new InternalErrorException(e.getLocalizedMessage());
        }
    }

```

API如下：

```
/**Delete a cube segment**/
Delete:    /cubes/{cubeName}/segs/{segmentName}

Path Variable

cubeName- required string cube name

segmentName  segment name of the cube 

```