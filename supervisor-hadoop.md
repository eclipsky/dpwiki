#### 背景和要解决的问题    
每次停电后，都要人工的重启dev/test上的各种环境，很操蛋。这个是最好的使用自动化工具的场景。
目前也在crontab中使用使用脚本自动拉起进程，但是crontab配置脚本还是有些麻烦，也没办法做可视化监控。

#### 目标
完成各种dev/test环境的监控和自动重启（包括开机）。

#### 方案
supervisor

#### 介绍
supervisor是python的一个进程守护工具，它可以通过配置文件，对配置文件中的程序或进程进行监控，并在程序或进程异常退出时，对其进行自动拉取。

#### 实现
因为supervisor只能对前台进程进行监控（**为什么？因为deamon化以后的进程，parent和child的关系就没了，所以parent进程不能通过发送signal去操作child进程**）。而hadoop进程大多数是通过deamonlize的方式进程启动的，不能直接通过supervisor进行监控。所以对hadoop和yarn的启动脚本进行了如下封装：     
**basic-foreground.sh**   

```bash
#! /usr/bin/env bash

function component_alive(){
  if [ -f $pidfile ]; then
    #等待10s，确认pidfile是否稍后会被删除。
    sleep 10
    if kill -0 `cat $pidfile` > /dev/null 2>&1; then
      echo $component running as process `cat $pidfile`.  Stop it first.
      return 0
    fi
  fi
  return 1
}

# Proxy signals
function kill_component(){
  if component_alive ; then
    echo killing $component
    kill $(cat $pidfile)
    sleep 5
    if kill -0 $(cat $pidfile) > /dev/null 2>&1; then
      echo "$component did not stop gracefully after 5 seconds: killing with kill -9"
      kill -9 $(cat $pidfile)
    fi
    rm -rf $pidfile 
  fi
  #supervisor can notice this
  exit 1000
}
trap "kill_component" SIGINT SIGTERM

# Launch daemon
if ! component_alive ; then
  $command
  sleep 2
fi


# Loop while the pidfile and the process exist
while [ -f $pidfile ] && kill -0 $(cat $pidfile) ; do
   sleep 1
done
echo "exit unexpected"
exit 1000


```
这个脚本内部会调用hadoop的deamon脚本启动相应的hadoop进程，然后一直check进程是否存在并循环。同时该脚本通过trap对TERM等退出信号进行相应，响应的逻辑就是：在脚本推迟之前执行`kill_component`杀死启动的后台进程。

接下来看看如果利用这段脚本来对所有hadoop deamon脚本进行包装，以hadoop-deamon.sh为例，其他类似:    
**hadoop-foreground.sh**
```bash
#! /usr/bin/env bash
usage="Usage: hadoop-foreground.sh [--config <conf-dir>] [--hosts hostlistfile] [--script script] (start|stop) <hadoop-command> <args...>"

# if no args specified, show usage
if [ $# -le 1 ]; then
  echo $usage
  exit 1
fi

set -eu
export HADOOP_PID_DIR=/data0/hadoop2/pid
export HADOOP_IDENT_STRING=$USER
component=$2

pidfile=$HADOOP_PID_DIR/hadoop-$HADOOP_IDENT_STRING-$component.pid
command="$HADOOP_HOME/sbin/hadoop-daemon.sh $@"

script_dir="$(dirname "$0")"
. "$script_dir/basic-foreground.sh"

```
脚本逻辑很简单：设置pidfile和具体的deamon启动命令command。

#### 配置
接下来配置supervisor的配置文件就可以了，看一个例子，supervisord.conf   
```
[program:namenode]
command=/usr/local/hadoop/supervisor/hadoop-foreground.sh start namenode
stdout_logfile=/usr/local/supervisor/logs/namenode-stdout.log
stderr_logfile=/usr/local/supervisor/logs/namenode-stderr.log
stopwaitsecs=120

[program:journalnode]
command=/usr/local/hadoop/supervisor/hadoop-foreground.sh start journalnode
stdout_logfile=/usr/local/supervisor/logs/journalnode-stdout.log
stderr_logfile=/usr/local/supervisor/logs/journalnode-stderr.log
stopwaitsecs=120

[program:zkfc]
command=/usr/local/hadoop/supervisor/hadoop-foreground.sh start zkfc
stdout_logfile=/usr/local/supervisor/logs/zkfc-stdout.log
stderr_logfile=/usr/local/supervisor/logs/zkfc-stderr.log
stopwaitsecs=120

[program:zookeeper]
command=/usr/local/zookeeper/bin/zkServer.sh start-foreground
stdout_logfile=/usr/local/supervisor/logs/zookeeper-stdout.log
stderr_logfile=/usr/local/supervisor/logs/zookeeper-stderr.log
stopwaitsecs=120

[program:resourcemanager]
command=/usr/local/hadoop/supervisor/yarn-foreground.sh start resourcemanager
stdout_logfile=/usr/local/supervisor/logs/resourcemanager-stdout.log
stderr_logfile=/usr/local/supervisor/logs/resourcemanager-stderr.log
stopwaitsecs=120

[program:metastore]
command=/usr/local/hive/bin/hive --service metastore
stdout_logfile=/usr/local/supervisor/logs/metastore-stdout.log
stderr_logfile=/usr/local/supervisor/logs/metastore-stderr.log
stopwaitsecs=120
```
配置都很简单易懂，**唯一需要说一下的是stopwaitsecs=120**，stopwaitsecs设置的supervisor等待kill命令的时间（signal是TERM），超过这个时间supervisor会直接SIGKILL（也就是kill -9），stopwaitsecs默认是10s，但是10s，`component_alive`可能没有执行完毕，所以这里设置为120s。

#### 开机启动supervisor
参考这里 http://10.1.2.222/dp-wiki/dpwiki/wikis/centos6-add-startup-service

下一次停电就不用手动起环境了。所有代码和配置在这里，http://10.1.2.222/dedp/data-robot/tree/master/supervisor
