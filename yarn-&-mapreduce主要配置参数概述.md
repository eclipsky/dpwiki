**Yarn & mapreduce主要配置参数概述**
```
mapreduce.map.sort.spill.percent=0.8  默认值0.8
mapreduce.map.memory.mb=1536      默认1024MB
mapreduce.map.cpu.vcores=2        默认值为1
mapreduce.reduce.memory.mb=1536   默认值1024
mapreduce.reduce.cpu.vcores=2	  默认值1
mapreduce.map.java.opts=-Xmx1024M 
mapreduce.reduce.java.opts=-Xmx2560M	
       该参数已经被上面的取代deprecated mapred.child.java.opts=-Xmx1024m    默认值-Xmx200m
yarn.scheduler.increment-allocation-mb=1024 默认值1024  规整化最小单位，规整化因子等于最小可申请资源量，不可单独配置
       公式为ceil(a/b)*b，其中a是应用程序申请的资源，b为规整化因子
       如果一个Container请求资源是1.5GB，则将被调度器规整化为ceiling(1.5 GB / 1GB) * 1G=2GB
yarn.scheduler.increment-allocation-vcores=1  默认是1，虚拟CPU规整化单位

mapreduce.task.io.sort.factor=20  默认是10，触发合并的溢写文件个数，对mapper和reducer同时生效
mapreduce.task.io.sort.mb=200     默认是100MB，mapper输出排序使用的buffer大小
io.sort.record.percent   不调整  缓冲区的索引数据占整个buffer的空间比 默认为0.05，即5%
io.sort.spill.percent    不调整  buffer空间使用率，默认值0.8，即80%空间被使用，溢写spill

mapreduce.reduce.shuffle.input.buffer.percent 默认为0.70，shuffle缓存(排序)申请的heap内存百分比，对应用mapper端具体大小MB参数mapreduce.task.io.sort.mb 
mapreduce.reduce.shuffle.memory.limit.percent 默认是0.25  每个fetch取到的输出的大小能够占的内存比的大小。
heapsize*{mapreduce.reduce.shuffle.input.buffer.percent}*{mapreduce.reduce.shuffle.memory.limit.percent}
        heapsize为mapreduce.reduce.java.opts
       该表达式计算结果被reduce用于判断mapper端shuffle过来的每个文件是否放置内存或者写入磁盘
       实际每个fetcher的到mapper输出文件能放在内存的大小是reducer的Java heap size*0.70*0.25
       所以，如果我们想fetch不进磁盘的话，可以适当调大这个值
mapreduce.reduce.shuffle.merge.percent 默认0.66  shuffle时占用总内存的百分比，触发merge写入磁盘，类似于mapper端spill
        计算方式 reducer heapsize*{mapreduce.reduce.shuffle.input.buffer.percent}*0.66
mapreduce.reduce.merge.memtomem.enabled=false：是否开启内存merge，默认是false
mapreduce.reduce.input.buffer.percent  默认值0，在执行reduce函数之前缓冲区读入的数据，默认不缓冲


container
yarn.nodemanager.resource.memory-mb      容器内存
yarn.sheduler.minimum-allocation-mb      最小容器内存
yarn.scheduler.increment-allocation-mb   容器内存增量
yarn.sheduler.maximun-allocation-mb      最大容器内存
yarn.nodemanager.vmem-pmem-ratio		 map/reduce申请虚拟内存比率

```

