所有涉及NodeManager的JVM参数优化均可照此操作
### 问题背景
NodeManager由于```java.lang.OutOfMemoryError: unable to create new native thread```异常退出   
### 解决方案
增加JVM的堆内存和修改Xss为512k  
### 操作影响
可能造成一些任务执行时间变长或者需要重跑
### 操作步骤
1.  登录所有NodeManager机器，修改yarn-env.sh中的 YARN_NODEMANAGER_OPTS变量，修改Xmx和Xss参数     
2. 登录所有NodeManager机器，执行以下操作:   
     1. 禁用NodeManager拉起的脚本（crontab中）   
     2. 重启NodeManger并观察   
     3. 恢复NodeManager的拉起脚本   
3. 完成   